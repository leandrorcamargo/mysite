#!/usr/bin/python
#-*- coding: utf-8 -*-

from django import forms
from django.forms import ModelForm, ModelMultipleChoiceField

from mysite.forms import *
from mysite.models import Estado, Cidade

class IndexForm(forms.Form):
    estado = forms.ModelChoiceField(queryset=Estado.objects.all(), empty_label="Selecione um estado")
    cidade = forms.ModelChoiceField(queryset=Cidade.objects.all(), empty_label="Selecione uma cidade")