#!/usr/bin/python
#-*- coding: utf-8 -*-

from django import forms
from django.forms import ModelForm

from mysite.forms import *
from mysite.models import Plano, Cidade

class PlanoForm(ModelForm):
  
  cidades = forms.ModelMultipleChoiceField(widget= forms.CheckboxSelectMultiple(), queryset = Cidade.objects.all(), required=False)

  def __init__(self, *args, **kwargs):
    super(PlanoForm, self).__init__(*args, **kwargs)

  class Meta:
    model = Plano
    fields = ('nome', 'tipo', 'duracao', 'valor', 'ensaios', 'cidades', 'dupla', 'cortesia', 'ativo')