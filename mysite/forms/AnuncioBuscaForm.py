#!/usr/bin/python
#-*- coding: utf-8 -*-

from django import forms
from django.forms import ModelForm

from mysite.forms import *
from mysite.models import Anuncio, Caracteristica, FormaPagamento, Informacao, Idioma, Biotipo, Seios, Olhos

class AnuncioBuscaForm(ModelForm):


  idiomas = forms.ModelMultipleChoiceField(label = "Idiomas", widget= forms.CheckboxSelectMultiple(), queryset = Idioma.objects.all(), required=False)

  caracteristicas = forms.ModelMultipleChoiceField(widget= forms.CheckboxSelectMultiple(), queryset = Caracteristica.objects.all(), required=False)

  atende = forms.ModelMultipleChoiceField(widget= forms.CheckboxSelectMultiple(), queryset = Atende.objects.all(), required=False)

  informacoes = forms.ModelMultipleChoiceField(label = "Informações", widget= forms.CheckboxSelectMultiple(), queryset = Informacao.objects.all(), required=False)

  formasPagamento = forms.ModelMultipleChoiceField(label = "Formas de Pagamento", widget= forms.CheckboxSelectMultiple(), queryset = FormaPagamento.objects.all(), required=False)

  idade_min = forms.IntegerField(label = "Idade Mínima", initial = 18, min_value = 18, max_value = 65 , widget=forms.NumberInput(), localize=True)
  
  idade = forms.IntegerField(label = "Idade Máxima", min_value = 18, max_value = 65, widget=forms.NumberInput(), localize=True) 

  peso_min = forms.FloatField(label = "Peso Mínimo", widget=forms.NumberInput(), localize=True) 

  peso = forms.FloatField(label = "Peso Máximo", widget=forms.NumberInput(), localize=True)

  altura_min = forms.FloatField(label = "Altura Mínima", widget=forms.NumberInput(), localize=True,  help_text='Exemplo: 1,65 para 1m e 65cm') 

  altura = forms.FloatField(label = "Altura Máxima", widget=forms.NumberInput(), localize=True,  help_text='Exemplo: 1,65 para 1m e 65cm') 
  
  def __init__(self, *args, **kwargs):
    super(AnuncioBuscaForm, self).__init__(*args, **kwargs)
    for key, field in self.fields.items():
      self.fields[key].required = False

  class Meta:
    model = Anuncio
    localized_fields = '__all__'
    fields = ('estado', 'cidade', 'categoria', 'nome', 'idade_min', 'idade', 'atende', 'idiomas', 'olhos', 'seios', 'biotipo', 'peso_min', 'peso', 'altura_min', 'altura', 'local', 'formasPagamento', 'caracteristicas', 'informacoes')    