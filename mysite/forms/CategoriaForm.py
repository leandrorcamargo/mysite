#!/usr/bin/python
#-*- coding: utf-8 -*-

from django import forms
from django.forms import ModelForm

from mysite.forms import *
from mysite.models import Categoria

class CategoriaForm(ModelForm):
  
  
  class Meta:
    model = Categoria
    fields = ('nome',)