#!/usr/bin/python
#-*- coding: utf-8 -*-

from django import forms
from django.forms import ModelForm

from mysite.forms import *
from mysite.models import AnuncioD, Plano, Anunciante, Caracteristica

class AnuncioDForm(ModelForm):

  caracteristicas1 = forms.ModelMultipleChoiceField(label = "Características Físicas", widget= forms.CheckboxSelectMultiple(), queryset = Caracteristica.objects.all(), required=False)
  caracteristicas2 = forms.ModelMultipleChoiceField(label = "Características Físicas da Parceira", widget= forms.CheckboxSelectMultiple(), queryset = Caracteristica.objects.all(), required=False)
  informacoes = forms.ModelMultipleChoiceField(label = "Informações", widget= forms.CheckboxSelectMultiple(), queryset = Informacao.objects.all(), required=False)
  formasPagamento = forms.ModelMultipleChoiceField(label = "Formas de Pagamento", widget= forms.CheckboxSelectMultiple(), queryset = FormaPagamento.objects.all(), required=False)
  idiomas = forms.ModelMultipleChoiceField(label = "Idiomas", widget= forms.CheckboxSelectMultiple(), queryset = Idioma.objects.all(), required=False)
  atende = forms.ModelMultipleChoiceField(label = "Atende", widget= forms.CheckboxSelectMultiple(), queryset = Atende.objects.all(), required=False)

  altura1 = forms.FloatField(label = "Altura", widget= forms.NumberInput(), required=True, localize=True, help_text='Exemplo: 1,65 para 1m e 65cm')
  idade1 = forms.FloatField(label = "Idade", min_value = 18, widget= forms.NumberInput(), required=True, localize=True)
  peso1 = forms.IntegerField(label = "Peso", widget= forms.NumberInput(), required=True, localize=True)
  altura2 = forms.FloatField(label = "Altura da Parceira", widget= forms.NumberInput(), required=True, localize=True, help_text='Exemplo: 1,65 para 1m e 65cm')
  idade2 = forms.IntegerField(label = "Idade da Parceira", min_value = 18, widget= forms.NumberInput(), required=True, localize=True)
  peso2 = forms.FloatField(label = "Peso da Parceira", widget= forms.NumberInput(), required=True, localize=True)
  cache = forms.IntegerField(label = "Cachê", widget= forms.NumberInput(), required=True, localize=True, help_text='Valor cobrado pelo serviço. Se preferir combinar, deixe o valor 0 que aparecerá "A combinar" no anúncio.')
  
  def __init__(self, *args, **kwargs):
    anunciante = kwargs.pop('anunciante')
    user = kwargs.pop('user')
    edit = kwargs.pop('edit')
    super(AnuncioDForm, self).__init__(*args, **kwargs)
    self.fields['capa'].widget.initial_text = "Capa atual"
    self.fields['capa'].widget.input_text = "Alterar capa"
    if not user.is_superuser:
      self.fields['pausado'].widget = forms.HiddenInput()
      self.fields['data_ini'].widget = forms.HiddenInput()
      self.fields['data_exp'].widget = forms.HiddenInput()
      self.fields['data_pause'].widget = forms.HiddenInput()
      self.fields['anunciante'].initial = anunciante.id
      self.data.update({ 'anunciante': anunciante.id })  #Atualiza o field mesmo desabilitado
      self.fields['anunciante'].widget.attrs['disabled'] = True
      if edit:
        self.fields['plano'].widget = forms.HiddenInput()

  class Meta:
    model = AnuncioD
    localized_fields = '__all__'
    fields = ('anunciante', 'categoria1', 'categoria2', 'plano', 'estado', 'cidade', 'nome', 'primeiroNome1', 'primeiroNome2', 'idade1', 'idade2', 'altura1', 'altura2', 'peso1', 'peso2', 'olhos1', 'olhos2', 'seios1', 'seios2',
    'biotipo1', 'biotipo2', 'idiomas', 'telefone', 'instagram1', 'instagram2', 'twitter1', 'twitter2', 'onlyfans1', 'onlyfans2', 'privacy1', 'privacy2', 'local', 'cache', 
    'formasPagamento', 'texto', 'atende', 'caracteristicas1', 'caracteristicas2', 'informacoes', 'capa', 'data_ini', 'data_exp', 'data_pause', 'pausado')
    widgets = {
      'texto': forms.Textarea(attrs={'rows': 8}),
    }