#!/usr/bin/python
#-*- coding: utf-8 -*-

from django import forms
from django.forms import ModelForm

from mysite.forms import *
from mysite.models import Pagamento

class PagamentoForm(ModelForm):
  
  class Meta:
    model = Pagamento
    fields = '__all__'