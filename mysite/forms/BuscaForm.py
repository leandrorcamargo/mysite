#!/usr/bin/python
#-*- coding: utf-8 -*-

from django import forms
from django.forms import ModelForm, ModelMultipleChoiceField

from mysite.forms import *
from mysite.models import Categoria

class BuscaForm(forms.Form):
    categoria = forms.ModelChoiceField(queryset=Categoria.objects.all())