#!/usr/bin/python
#-*- coding: utf-8 -*-

from django import forms
from django.forms import ModelForm

from mysite.forms import *
from mysite.models import Estado

class EstadoForm(ModelForm):
  
  
  class Meta:
    model = Estado
    fields = ('nome', 'sigla')