#!/usr/bin/python
#-*- coding: utf-8 -*-

from django import forms
from django.forms import ModelForm, ModelMultipleChoiceField
from django.forms.extras.widgets import SelectDateWidget
from django.forms.fields import DateField

from mysite.forms import *
from mysite.models import Anunciante

class WriteEmailForm(forms.Form):
    para = forms.ModelMultipleChoiceField(queryset=Anunciante.objects.all())
    subject = forms.CharField(widget=forms.TextInput(), label='Assunto')
    body = forms.CharField(widget=forms.Textarea(), label='Corpo da Mensagem')
    imagem = forms.ImageField(required=False)