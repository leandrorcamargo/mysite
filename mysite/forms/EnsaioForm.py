#!/usr/bin/python
#-*- coding: utf-8 -*-

from django import forms
from django.forms import ModelForm
from mysite.forms import *
from mysite.models import Ensaio
import os
import magic

class EnsaioForm(ModelForm):
  
  def __init__(self, *args, **kwargs):
    super(EnsaioForm, self).__init__(*args, **kwargs)
    self.fields['video'].widget.initial_text = "Video Atual"
    self.fields['video'].widget.input_text = "Alterar Video"
    self.fields['foto1'].widget.initial_text = "Imagem Atual"
    self.fields['foto1'].widget.input_text = "Alterar Imagem"
    self.fields['foto2'].widget.initial_text = "Imagem Atual"
    self.fields['foto2'].widget.input_text = "Alterar Imagem"
    self.fields['foto3'].widget.initial_text = "Imagem Atual"
    self.fields['foto3'].widget.input_text = "Alterar Imagem"
    self.fields['foto4'].widget.initial_text = "Imagem Atual"
    self.fields['foto4'].widget.input_text = "Alterar Imagem"
    self.fields['foto5'].widget.initial_text = "Imagem Atual"
    self.fields['foto5'].widget.input_text = "Alterar Imagem"
    self.fields['foto6'].widget.initial_text = "Imagem Atual"
    self.fields['foto6'].widget.input_text = "Alterar Imagem"
    self.fields['foto7'].widget.initial_text = "Imagem Atual"
    self.fields['foto7'].widget.input_text = "Alterar Imagem"
    self.fields['foto8'].widget.initial_text = "Imagem Atual"
    self.fields['foto8'].widget.input_text = "Alterar Imagem"
    self.fields['foto9'].widget.initial_text = "Imagem Atual"
    self.fields['foto9'].widget.input_text = "Alterar Imagem"
    self.fields['foto10'].widget.initial_text = "Imagem Atual"
    self.fields['foto10'].widget.input_text = "Alterar Imagem"

  def clean(self):
        anuncio = self.cleaned_data.get('anuncio')
        anuncioD = self.cleaned_data.get('anuncioD')
        video = self.cleaned_data.get('video')
        fotos = ['foto1','foto2','foto3','foto4','foto5',
    'foto6','foto7','foto8','foto9','foto10']
        if anuncio and anuncioD:
          msg = "Escolha apenas um anúncio ou um anúncio de dupla para o ensaio"
          self.add_error('anuncio', msg)
          self.add_error('anuncioD', msg)
          raise forms.ValidationError(msg)
        if not anuncio and not anuncioD:
          msg = "É necessário esscolher um anúncio ou um anúncio de dupla parao ensaio."
          self.add_error('anuncio', msg)
          self.add_error('anuncioD', msg)
          raise forms.ValidationError(msg)
        if video:
          msg = 'O arquivo não é MP4. Favor enviar video no formato MP4.'
          video.seek(0)
          file_mime_type = magic.from_buffer(video.read(), mime=True)
          #video.seek(0)
          print(file_mime_type)
          valid_mime_types = ['video/mp4', 'video/x-m4v']
          if file_mime_type not in valid_mime_types:
            self.add_error('video', msg)
            raise forms.ValidationError(msg)
          valid_file_extensions = ['.mp4']
          ext = os.path.splitext(video.name)[1]
          if ext.lower() not in valid_file_extensions:
            msg = 'O arquivo não é MP4. Favor enviar video no formato MP4.'
            self.add_error('video', msg)
            raise forms.ValidationError(msg)
          if video.size > 53000000:
            msg = 'O arquivo é maior que o limite de 50MB para video.'
            self.add_error('video', msg + str(video.size))
            raise forms.ValidationError(msg)
        for foto in fotos:
          foto_clean = self.cleaned_data.get(foto)
          if ((foto_clean != '0') and (foto_clean != None)):
            valid_mime_types = ['image/jpeg', 'image/png', 'image/gif']
            foto_clean.seek(0)
            file_mime_type = magic.from_buffer(foto_clean.read(), mime=True)            
            print(file_mime_type)
            if file_mime_type not in valid_mime_types:
              msg = 'O arquivo não é uma imagem em formato válido. Favor enviar apenas imagens nos formatos JPG, PNG ou GIF.'
              self.add_error(foto, msg)
              raise forms.ValidationError(msg)
            if foto_clean.size > 8400000:
              msg = 'O arquivo é maior que o limite de 8MB para foto.'
              self.add_error(foto, msg)
              raise forms.ValidationError(msg)


  class Meta:
    model = Ensaio
    fields = ('anuncio', 'anuncioD', 'nome', 'video', 'foto1','foto2','foto3','foto4','foto5',
    'foto6','foto7','foto8','foto9','foto10')