#!/usr/bin/python
#-*- coding: utf-8 -*-

from django import forms

from mysite.forms import *

class UserSignInForm(forms.Form):
  """
  Form with email and password fields
  """
  email = forms.EmailField(label = 'Email',
    widget=forms.TextInput(), error_messages={'invalid': 'Digite um endereço de e-mail válido'})
  
  password = forms.CharField(label = 'Password',
    widget=forms.PasswordInput())