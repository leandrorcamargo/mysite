#!/usr/bin/python
#-*- coding: utf-8 -*-

from django import forms
from django.forms import ModelForm

from mysite.forms import *
from mysite.models import Informacao

class InformacaoForm(ModelForm):
  
  
  class Meta:
    model = Informacao
    fields = ('nome',)