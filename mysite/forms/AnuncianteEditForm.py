#!/usr/bin/python
#-*- coding: utf-8 -*-

from django import forms
from django.forms import ModelForm
from django.forms.fields import DateField
from django.contrib.auth.models import User
from django.utils.translation import gettext_lazy as _

from mysite.controllers import invariants
from mysite.controllers import utils, mail
from mysite.forms import *
from mysite.models import Anunciante, UserConfirmation, Category, Cidade, Estado

class AnuncianteEditForm(ModelForm):

  def __init__(self, *args, **kwargs):
    usuario = kwargs.pop('usuario')
    super(AnuncianteEditForm, self).__init__(*args, **kwargs)    
    self.fields['rg'].widget.initial_text = "Imagem Atual"
    self.fields['rg'].widget.input_text = "Alterar Imagem"
    self.fields['segurando_rg'].widget.initial_text = "Imagem Atual"
    self.fields['segurando_rg'].widget.input_text = "Alterar Imagem"
    if not usuario.is_superuser:
      self.fields['aprovado'].widget = forms.HiddenInput()
    
    self.old_email = self.instance.email

  def clean_email(self):
    email = self.cleaned_data.get('email')
    if User.objects.filter(username=email).exists() and self.old_email != email:
      raise forms.ValidationError(_('Email "%s" is already in use.') % email)
    return email

  def save (self, url, commit = True):
    anunciante = super(AnuncianteEditForm, self).save(commit=False)
    if commit:

      user = User.objects.get(email = self.old_email)
      estado, ecreated = Estado.objects.update_or_create(sigla = anunciante.estado)
      cidade, ccreated = Cidade.objects.update_or_create(estado = estado, nome=anunciante.cidade)
         
      name_splitted = anunciante.nome.split(" ") #Splits name to get first, last and middlenames
      name_size = len(name_splitted)
      user.first_name = name_splitted[0]
      
      if name_size > 1:
        user.last_name = name_splitted[name_size -1]
      anunciante.save()

      if self.old_email != self.instance.email:
        user.email = self.instance.email
        user.save()
        userConfirmation = UserConfirmation.objects.create(user=user, token=utils.generateHashKey(user.username),
        category=Category.VERIFICATION, confirmed = False )
        subject = invariants.message_subject_confirmation2
        message = invariants.message_body_confirmation2 % (user.first_name)
        button = invariants.button_account_confirm

        context = {'confirmation_url': url+userConfirmation.token, 
          'email': user.email, 
          'message': message, 
          'button': button}

        mail.sendMailTemplate(subject, context, [user.email])
    return anunciante

  class Meta:
    model = Anunciante
    fields = ('nome', 'idade', 'rg', 'segurando_rg', 'cpf', 'telefone', 'cep', 'estado', 'cidade', 'rua', 'numero', 'email', 'aprovado')