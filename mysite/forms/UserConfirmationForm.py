#!/usr/bin/python
#-*- coding: utf-8 -*-

from django import forms
from django.contrib.auth.models import User
from django.utils.translation import gettext_lazy as _

from mysite.controllers import mail
from mysite.forms import *

class UserConfirmationForm(forms.Form):
  """
  Form with field: email
  """

  email = forms.EmailField(
    widget=forms.TextInput()) 


  def clean_email(self):
    email = self.cleaned_data['email']
    if not User.objects.filter(email=email).exists():
      raise forms.ValidationError(_('Cadastro não encontrado.'))
    else:
      return email


  def save(self, subject, context, email):
    
    mail.sendMailTemplate(subject, context, [email])
