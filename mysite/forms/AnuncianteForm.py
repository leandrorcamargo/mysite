#!/usr/bin/python
#-*- coding: utf-8 -*-

from django import forms
from django.db import transaction
from django.forms import ModelForm
from django.forms.fields import DateField
from django.contrib.auth.models import User
from django.utils.translation import gettext_lazy as _

from mysite.controllers import invariants
from mysite.controllers import utils, mail
from mysite.forms import *
from mysite.models import Anunciante, UserConfirmation, Category, Cidade, Estado

class AnuncianteForm(ModelForm):
  email2 = forms.EmailField(label = _('Confirme seu E-mail'))

  def __init__(self, *args, **kwargs):
    super(AnuncianteForm, self).__init__(*args, **kwargs)
    self.aprovado = False
    self.fields['aprovado'].widget = forms.HiddenInput()


    self.email = ''
    if self.instance.id != None:
      self.email = self.instance.email


  def clean_email2(self):
    email = self.cleaned_data.get('email')
    email2 = self.cleaned_data['email2']
    if email != email2:
      raise forms.ValidationError (_("The e-mails submited are differents from each other."))
    if User.objects.filter(email=email).exists():
        raise forms.ValidationError(_('Email "%s" is already in use.') % email)
    return email

  @transaction.atomic
  def save (self, url, commit = True):
    """
    Receives input and save the data in the database: :model 'auth.User' and 'optisensor.models.UserExtraInfo'
    Sends email confirmation with function :py:func: 'optisensor.controllers.mail.opsendMailTemplate'    
    """

    anunciante = super(AnuncianteForm, self).save(commit=False)
    # name = self.cleaned_data['nome']
    # email = self.cleaned_data['email']
    # idade = self.cleaned_data['idade']
    # rg = self.cleaned_data['rg']
    # telefone = self.cleaned_data['telefone']
    # estado = self.cleaned_data['estado']
    # cidade = self.cleaned_data['cidade']
    # endereco = self.cleaned_data['endereco']

    if commit:
      user, ucreated = User.objects.update_or_create(username = anunciante.email, email=anunciante.email)
      userConfirmation, uccreated = UserConfirmation.objects.update_or_create(user=user, token=utils.generateHashKey(user.username),
        category=Category.PASSWORD, confirmed = False )
      estado, ecreated = Estado.objects.update_or_create(sigla = anunciante.estado)
      cidade, ccreated = Cidade.objects.update_or_create(estado = estado, nome=anunciante.cidade)
      anunciante.user = user
      anunciante.save()
      name_splitted = anunciante.nome.split(" ") #Splits name to get first, last and middlenames
      name_size = len(name_splitted)
      user.first_name = name_splitted[0]
      
      if name_size > 1:
        user.last_name = name_splitted[name_size -1]

      if self.email == '' or self.email != anunciante.email:
        subject = invariants.message_subject_confirmation
        message = invariants.message_body_confirmation % (anunciante.nome)
        button = invariants.button_account_confirm

        context = {'confirmation_url': url+"password/"+userConfirmation.token, 
          'email': user.email, 
          'message': message, 
          'button': button}

        mail.sendMailTemplate(subject, context, [user.email])


      # user.save() #Saves user first_name and last_name
      # anunciante = Anunciante.objects.create(user=user, nome = name, email=email, idade=idade, rg=rg, telefone=telefone, estado=estado, cidade=cidade, endereco=endereco)
      # userConfirmation.save() 
      #   return name
      # else:
      #   return None

    return anunciante

  class Meta:
    model = Anunciante
    fields = ('nome', 'idade', 'rg', 'segurando_rg', 'cpf', 'telefone', 'cep', 'estado', 'cidade', 'rua', 'numero', 'email', 'aprovado')
