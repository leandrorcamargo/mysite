# Generated by Django 3.2.6 on 2021-10-03 20:40

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mysite', '0003_auto_20210818_2304'),
    ]

    operations = [
        migrations.CreateModel(
            name='FormaPagamento',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nome', models.CharField(max_length=50, verbose_name='Nome')),
            ],
            options={
                'verbose_name': 'Formas de Pagamento',
                'ordering': ['-nome'],
            },
        ),
        migrations.AddField(
            model_name='anunciante',
            name='aprovado',
            field=models.BooleanField(blank=True, default=False, verbose_name='Aprovado'),
        ),
        migrations.AddField(
            model_name='anuncio',
            name='formasPagamento',
            field=models.ManyToManyField(to='mysite.FormaPagamento'),
        ),
    ]
