# Generated by Django 3.2.6 on 2021-10-12 08:56

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mysite', '0013_remove_pagamento_mercado_pago_external_reference'),
    ]

    operations = [
        migrations.AddField(
            model_name='plano',
            name='cidades',
            field=models.ManyToManyField(to='mysite.Cidade', verbose_name='Cidades'),
        ),
        migrations.AlterField(
            model_name='plano',
            name='tipo',
            field=models.IntegerField(choices=[(1, 'Normal'), (2, 'Destaque')], default=False, verbose_name='Tipo do plano'),
        ),
    ]
