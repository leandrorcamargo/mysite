#!/usr/bin/python
#-*- coding: utf-8 -*-
"""mysite URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
"""
from django.urls import re_path, include
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static

import mysite.views as views


urlpatterns = [
    re_path(r'^$', views.index, name='index'),
    re_path(r'^admin/', admin.site.urls),

    re_path(r'^anunciante/sign_in/$', views.userSignIn, name='sign_in'),
    re_path(r'^anunciante/sign_out/$', views.userSignOut, name='sign_out'),

    re_path(r'^anunciante/$', views.anuncianteList, name='anuncianteList'),
    re_path(r'^anunciante/preNew/$', views.anunciantePreNew, name='anunciantePreNew'),
    re_path(r'^anunciante/preNew2/$', views.anunciantePreNew2, name='anunciantePreNew2'),
    re_path(r'^anunciante/new/$', views.anuncianteNew, name='anuncianteNew'),
    re_path(r'^anunciante/(?P<anuncianteId>[0-9]+)/$', views.anuncianteShow, name='anuncianteShow'),
    re_path(r'^anunciante/(?P<anuncianteId>[0-9]+)/password/$', views.userChangePassword, name='userChangePassword'),
    re_path(r'^anunciante/(?P<anuncianteId>[0-9]+)/delete/$', views.anuncianteDelete, name='anuncianteDelete'),
    re_path(r'^anunciante/(?P<anuncianteId>[0-9]+)/edit/$', views.anuncianteEdit, name='anuncianteEdit'),
    re_path(r'^anunciante/new/password/$', views.userPasswordNew, name='userPasswordNew'),
    re_path(r'^anunciante/new/password/(?P<token>\w+)/$', views.userPasswordNewConfirm, name='userPasswordNewConfirm'),
    re_path(r'^anunciante/confirmation/new/$', views.userConfirmationNew, name='userConfirmationNew'),
    re_path(r'^anunciante/(?P<anuncianteId>[0-9]+)/edit/(?P<token>\w+)/$', views.userConfirmationNewConfirm, name='userConfirmationNewConfirm'),

    re_path(r'^anuncio$', views.anuncioList,  name='anuncioList'),
    re_path(r'^anuncio/$', views.anuncioList,  name='anuncioList'),
    re_path(r'^anuncio/(?P<categoriaId>[0-9]+)/$', views.anuncioList,  name='anuncioList'),
    re_path(r'^anuncio/new/$', views.anuncioNew, name='anuncioNew'),
    re_path(r'^anuncio/s(?P<anuncioId>[0-9]+)/$', views.anuncioShow, name='anuncioShow'),
    re_path(r'^anuncio/(?P<anuncioId>[0-9]+)/delete/$', views.anuncioDelete, name='anuncioDelete'),
    re_path(r'^anuncio/(?P<anuncioId>[0-9]+)/pause/$', views.anuncioPause, name='anuncioPause'),
    re_path(r'^anuncio/(?P<anuncioId>[0-9]+)/edit/$', views.anuncioEdit, name='anuncioEdit'),
    re_path(r'^anuncio/(?P<anuncioId>[0-9]+)/renew/$', views.anuncioRenew, name='anuncioRenew'),

    re_path(r'^anuncioD$', views.anuncioDList,  name='anuncioDList'),
    re_path(r'^anuncioD/$', views.anuncioDList,  name='anuncioDList'),
    re_path(r'^anuncioD/new/$', views.anuncioDNew, name='anuncioDNew'),
    re_path(r'^anuncioD/s(?P<anuncioDId>[0-9]+)/$', views.anuncioDShow, name='anuncioDShow'),
    re_path(r'^anuncioD/(?P<anuncioDId>[0-9]+)/delete/$', views.anuncioDDelete, name='anuncioDDelete'),
    re_path(r'^anuncioD/(?P<anuncioDId>[0-9]+)/pause/$', views.anuncioPause, name='anuncioDPause'),
    re_path(r'^anuncioD/(?P<anuncioDId>[0-9]+)/edit/$', views.anuncioDEdit, name='anuncioDEdit'),
    re_path(r'^anuncioD/(?P<anuncioDId>[0-9]+)/renew/$', views.anuncioDRenew, name='anuncioDRenew'),

    re_path(r'^contato/show/$', views.contatoShow, name='contatoShow'),

    re_path(r'^failure/(?P<pagamentoId>[-A-Za-z0-9_]+)/$', views.paymentFailure,  name='paymentFailure'),
    re_path(r'^pending/(?P<pagamentoId>[-A-Za-z0-9_]+)/$', views.paymentPending,  name='paymentPending'),
    re_path(r'^success/(?P<pagamentoId>[-A-Za-z0-9_]+)/$', views.paymentSuccess,  name='paymentSuccess'),
    re_path(r'^webhook/$', views.paymentWebhook,  name='paymentWebhook'),
    re_path(r'^process/(?P<preferenceId>[-A-Za-z0-9_]+)/getForm/(?P<anuncioId>[-A-Za-z0-9_]+)/$', views.paymentProcess,  name='paymentProcess'),
    re_path(r'^pagamento/$', views.pagamentoList, name='pagamentoList'),
    re_path(r'^pagamento/(?P<pagamentoId>[0-9]+)/$', views.pagamentoShow, name='pagamentoShow'),
    re_path(r'^pagamento/(?P<pagamentoId>[0-9]+)/edit/$', views.pagamentoEdit, name='pagamentoEdit'),
    re_path(r'^pagamento/(?P<pagamentoId>[0-9]+)/delete/$', views.pagamentoDelete, name='pagamentoDelete'),

    re_path(r'^ensaio/$', views.ensaioList, name='ensaioList'),
    re_path(r'^ensaio/new/$', views.ensaioNew, name='ensaioNew'),
    re_path(r'^ensaio/(?P<ensaioId>[0-9]+)/$', views.ensaioShow, name='ensaioShow'),
    re_path(r'^ensaio/(?P<ensaioId>[0-9]+)/delete/$', views.ensaioDelete, name='ensaioDelete'),
    re_path(r'^ensaio/(?P<ensaioId>[0-9]+)/edit/$', views.ensaioEdit, name='ensaioEdit'),

    re_path(r'^busca/$', views.busca, name='busca'),

    re_path(r'^caracteristica/$', views.caracteristicaList, name='caracteristicaList'),
    re_path(r'^caracteristica/new/$', views.caracteristicaNew, name='caracteristicaNew'),
    re_path(r'^caracteristica/(?P<caracteristicaId>[0-9]+)/$', views.caracteristicaShow, name='caracteristicaShow'),
    re_path(r'^caracteristica/(?P<caracteristicaId>[0-9]+)/delete/$', views.caracteristicaDelete, name='caracteristicaDelete'),
    re_path(r'^caracteristica/(?P<caracteristicaId>[0-9]+)/edit/$', views.caracteristicaEdit, name='caracteristicaEdit'),

    re_path(r'^informacao/$', views.informacaoList, name='informacaoList'),
    re_path(r'^informacao/new/$', views.informacaoNew, name='informacaoNew'),
    re_path(r'^informacao/(?P<informacaoId>[0-9]+)/$', views.informacaoShow, name='informacaoShow'),
    re_path(r'^informacao/(?P<informacaoId>[0-9]+)/delete/$', views.informacaoDelete, name='informacaoDelete'),
    re_path(r'^informacao/(?P<informacaoId>[0-9]+)/edit/$', views.informacaoEdit, name='informacaoEdit'),

    re_path(r'^atende/$', views.atendeList, name='atendeList'),
    re_path(r'^atende/new/$', views.atendeNew, name='atendeNew'),
    re_path(r'^atende/(?P<atendeId>[0-9]+)/$', views.atendeShow, name='atendeShow'),
    re_path(r'^atende/(?P<atendeId>[0-9]+)/delete/$', views.atendeDelete, name='atendeDelete'),
    re_path(r'^atende/(?P<atendeId>[0-9]+)/edit/$', views.atendeEdit, name='atendeEdit'),

    re_path(r'^formaPagamento/$', views.formaPagamentoList, name='formaPagamentoList'),
    re_path(r'^formaPagamento/new/$', views.formaPagamentoNew, name='formaPagamentoNew'),
    re_path(r'^formaPagamento/(?P<formaPagamentoId>[0-9]+)/$', views.formaPagamentoShow, name='formaPagamentoShow'),
    re_path(r'^formaPagamento/(?P<formaPagamentoId>[0-9]+)/delete/$', views.formaPagamentoDelete, name='formaPagamentoDelete'),
    re_path(r'^formaPagamento/(?P<formaPagamentoId>[0-9]+)/edit/$', views.formaPagamentoEdit, name='formaPagamentoEdit'),
    
    re_path(r'^olhos/$', views.olhosList, name='olhosList'),
    re_path(r'^olhos/new/$', views.olhosNew, name='olhosNew'),
    re_path(r'^olhos/(?P<olhosId>[0-9]+)/$', views.olhosShow, name='olhosShow'),
    re_path(r'^olhos/(?P<olhosId>[0-9]+)/delete/$', views.olhosDelete, name='olhosDelete'),
    re_path(r'^olhos/(?P<olhosId>[0-9]+)/edit/$', views.olhosEdit, name='olhosEdit'),

    re_path(r'^categoria/$', views.categoriaList, name='categoriaList'),
    re_path(r'^categoria/new/$', views.categoriaNew, name='categoriaNew'),
    re_path(r'^categoria/(?P<categoriaId>[0-9]+)/$', views.categoriaShow, name='categoriaShow'),
    re_path(r'^categoria/(?P<categoriaId>[0-9]+)/delete/$', views.categoriaDelete, name='categoriaDelete'),
    re_path(r'^categoria/(?P<categoriaId>[0-9]+)/edit/$', views.categoriaEdit, name='categoriaEdit'),

    re_path(r'^idioma/$', views.idiomaList, name='idiomaList'),
    re_path(r'^idioma/new/$', views.idiomaNew, name='idiomaNew'),
    re_path(r'^idioma/(?P<idiomaId>[0-9]+)/$', views.idiomaShow, name='idiomaShow'),
    re_path(r'^idioma/(?P<idiomaId>[0-9]+)/delete/$', views.idiomaDelete, name='idiomaDelete'),
    re_path(r'^idioma/(?P<idiomaId>[0-9]+)/edit/$', views.idiomaEdit, name='idiomaEdit'),

    re_path(r'^seios/$', views.seiosList, name='seiosList'),
    re_path(r'^seios/new/$', views.seiosNew, name='seiosNew'),
    re_path(r'^seios/(?P<seiosId>[0-9]+)/$', views.seiosShow, name='seiosShow'),
    re_path(r'^seios/(?P<seiosId>[0-9]+)/delete/$', views.seiosDelete, name='seiosDelete'),
    re_path(r'^seios/(?P<seiosId>[0-9]+)/edit/$', views.seiosEdit, name='seiosEdit'),

    re_path(r'^biotipo/$', views.biotipoList, name='biotipoList'),
    re_path(r'^biotipo/new/$', views.biotipoNew, name='biotipoNew'),
    re_path(r'^biotipo/(?P<biotipoId>[0-9]+)/$', views.biotipoShow, name='biotipoShow'),
    re_path(r'^biotipo/(?P<biotipoId>[0-9]+)/delete/$', views.biotipoDelete, name='biotipoDelete'),
    re_path(r'^biotipo/(?P<biotipoId>[0-9]+)/edit/$', views.biotipoEdit, name='biotipoEdit'),

    re_path(r'^cidade/$', views.cidadeList, name='cidadeList'),
    re_path(r'^cidade/new/$', views.cidadeNew, name='cidadeNew'),
    re_path(r'^cidade/(?P<cidadeId>[0-9]+)/$', views.cidadeShow, name='cidadeShow'),
    re_path(r'^cidade/(?P<cidadeId>[0-9]+)/delete/$', views.cidadeDelete, name='cidadeDelete'),
    re_path(r'^cidade/(?P<cidadeId>[0-9]+)/edit/$', views.cidadeEdit, name='cidadeEdit'),
    re_path(r'^filtraCidade/(?P<estadoId>[0-9]+)/$', views.filtraCidade, name='fitraCidade'),
    re_path(r'^alteraRegiao/$', views.alteraRegiao, name='alteraRegiao'),
    re_path(r'^pegaEstados/$', views.pegaEstados, name='pegaEstados'),

    re_path(r'^estado/$', views.estadoList, name='estadoList'),
    re_path(r'^estado/new/$', views.estadoNew, name='estadoNew'),
    re_path(r'^estado/(?P<estadoId>[0-9]+)/$', views.estadoShow, name='estadoShow'),
    re_path(r'^estado/(?P<estadoId>[0-9]+)/delete/$', views.estadoDelete, name='estadoDelete'),
    re_path(r'^estado/(?P<estadoId>[0-9]+)/edit/$', views.estadoEdit, name='estadoEdit'),


    re_path(r'^plano/$', views.planoList, name='planoList'),
    re_path(r'^plano/new/$', views.planoNew, name='planoNew'),
    re_path(r'^plano/(?P<planoId>[0-9]+)/$', views.planoShow, name='planoShow'),
    re_path(r'^plano/(?P<planoId>[0-9]+)/delete/$', views.planoDelete, name='planoDelete'),
    re_path(r'^plano/(?P<planoId>[0-9]+)/edit/$', views.planoEdit, name='planoEdit'),
    re_path(r'^filtrarPlano/(?P<cidadeId>[0-9]+)/$', views.filtrarPlano, name='filtrarPlano'),

        
]+static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


def breadcrumbResolve(url_name):
	urls = [
    {'name': 'index', 'title': 'Home', 'icon': 'fa-home'},    

    {'name': 'sign_in', 'title': 'Entrada de anunciantes', 'icon': 'fa-sign-in'},
    {'name': 'sign_out', 'title': 'Sair', 'icon': 'fa-sign-out'},

    {'name': 'anunciantePreNew',  'title': 'Como ser Anunciante', 'icon': 'fa-plus'},
    {'name': 'anunciantePreNew2',  'title': 'Como ser Anunciante', 'icon': 'fa-plus'},
    {'name': 'anuncianteNew',  'title': 'Criar Anunciante', 'icon': 'fa-plus'},
    {'name': 'anuncianteList', 'title': 'Lista de Anunciantes', 'icon': 'fa-user'},
    {'name': 'anuncianteShow', 'title': 'Anunciante', 'icon': 'fa-user'},
    {'name': 'anuncianteDelete', 'title': 'Apagar Anunciante', 'icon': 'fa-times'},
    {'name': 'anuncianteEdit', 'title': 'Editar Anunciante', 'icon': 'fa-pencil-square-o'},
    {'name': 'userEditConfirm', 'title': 'Editar Anunciante', 'icon': ''}, #no icon
    {'name': 'userNewConfirm', 'title': 'Criar Anunciante', 'icon': ''}, #no icon
    {'name': 'userChangePassword', 'title': 'Alterar Senha', 'icon': ''}, #no icon
    {'name': 'userPasswordNew', 'title': 'Recuperar Senha', 'icon': ''}, #no icon
    {'name': 'userPasswordNewConfirm', 'title': 'Criar Senha', 'icon': ''}, #no icon
    {'name': 'userConfirmationNew', 'title': u'Reenviar Instruções de Confirmação', 'icon': ''}, #no icon
    {'name': 'userConfirmationNewConfirm', 'title': u'Confirmação', 'icon': ''}, #no icon

    {'name': 'anuncioNew',  'title': 'Criar Anúncio', 'icon': 'fa-plus'},
    {'name': 'anuncioList', 'title': 'Anúncios', 'icon': 'fa-user'},
    {'name': 'anuncioShow', 'title': '', 'icon': 'fa-user'},
    {'name': 'anuncioDelete', 'title': 'Apagar Anúncio', 'icon': 'fa-times'},
    {'name': 'anuncioPause', 'title': 'Pausar/Reativar Anúncio', 'icon': 'fa-times'},
    {'name': 'anuncioEdit', 'title': 'Editar Anúncio', 'icon': 'fa-pencil-square-o'},
    {'name': 'anuncioRenew', 'title': 'Renovar Plano do Anúncio', 'icon': 'fa-plus'},

    {'name': 'anuncioDNew',  'title': 'Criar Anúncio', 'icon': 'fa-plus'},
    {'name': 'anuncioDList', 'title': 'Anúncios Duplas', 'icon': 'fa-user'},
    {'name': 'anuncioDShow', 'title': '', 'icon': 'fa-user'},
    {'name': 'anuncioDDelete', 'title': 'Apagar Anúncio', 'icon': 'fa-times'},
    {'name': 'anuncioDPause', 'title': 'Pausar/Reativar Anúncio', 'icon': 'fa-times'},
    {'name': 'anuncioDEdit', 'title': 'Editar Anúncio', 'icon': 'fa-pencil-square-o'},
    {'name': 'anuncioDRenew', 'title': 'Renovar Plano do Anúncio', 'icon': 'fa-plus'},

    {'name': 'contatoShow',  'title': 'Contato', 'icon': 'fa-plus'},

    {'name': 'paymentFailure',  'title': 'Erro no Pagamento', 'icon': 'fa-plus'},
    {'name': 'paymentPending',  'title': 'Pagamento Pendente', 'icon': 'fa-plus'},
    {'name': 'paymentSuccess',  'title': 'Pagamento Efetuado', 'icon': 'fa-plus'},
    {'name': 'paymentWebhook',  'title': 'Webhook de Pagamentos', 'icon': 'fa-plus'},
    {'name': 'paymentProcess',  'title': 'Processamento de Pagamentos', 'icon': 'fa-plus'},
    {'name': 'pagamentoList', 'title': 'Lista de Pagamentos', 'icon': 'fa-user'},
    {'name': 'pagamentoShow', 'title': '', 'icon': 'fa-user'},
    {'name': 'pagamentoDelete', 'title': 'Apagar Pagamento', 'icon': 'fa-times'},
    {'name': 'pagamentoEdit', 'title': 'Editar Pagamento', 'icon': 'fa-pencil-square-o'},

    {'name': 'ensaioNew',  'title': 'Criar Ensaio', 'icon': 'fa-plus'},
    {'name': 'ensaioList', 'title': 'Lista de Ensaios', 'icon': 'fa-user'},
    {'name': 'ensaioShow', 'title': '', 'icon': 'fa-user'},
    {'name': 'ensaioDelete', 'title': 'Apagar Ensaio', 'icon': 'fa-times'},
    {'name': 'ensaioEdit', 'title': 'Editar Ensaio', 'icon': 'fa-pencil-square-o'},

    {'name': 'busca', 'title': 'Buscar Anuncio', 'icon': 'fa-search'},

    {'name': 'caracteristicaNew',  'title': 'Criar Característica', 'icon': 'fa-plus'},
    {'name': 'caracteristicaList', 'title': 'Lista de Características', 'icon': 'fa-user'},
    {'name': 'caracteristicaShow', 'title': 'Característica', 'icon': 'fa-user'},
    {'name': 'caracteristicaDelete', 'title': 'Apagar Característica', 'icon': 'fa-times'},
    {'name': 'caracteristicaEdit', 'title': 'Editar Característica', 'icon': 'fa-pencil-square-o'},

    {'name': 'informacaoNew',  'title': 'Criar Informação', 'icon': 'fa-plus'},
    {'name': 'informacaoList', 'title': 'Lista de Informações', 'icon': 'fa-user'},
    {'name': 'informacaoShow', 'title': 'Informação', 'icon': 'fa-user'},
    {'name': 'informacaoDelete', 'title': 'Apagar Informação', 'icon': 'fa-times'},
    {'name': 'informacaoEdit', 'title': 'Editar Informação', 'icon': 'fa-pencil-square-o'},

    {'name': 'atendeNew',  'title': 'Criar Atende', 'icon': 'fa-plus'},
    {'name': 'atendeList', 'title': 'Lista de Atende', 'icon': 'fa-user'},
    {'name': 'atendeShow', 'title': 'Atende', 'icon': 'fa-user'},
    {'name': 'atendeDelete', 'title': 'Apagar Atende', 'icon': 'fa-times'},
    {'name': 'atendeEdit', 'title': 'Editar Atende', 'icon': 'fa-pencil-square-o'},

    {'name': 'formaPagamentoNew',  'title': 'Criar Forma de Pagamento', 'icon': 'fa-plus'},
    {'name': 'formaPagamentoList', 'title': 'Lista de Formas de Pagamento', 'icon': 'fa-user'},
    {'name': 'formaPagamentoShow', 'title': 'Forma de Pagamento', 'icon': 'fa-user'},
    {'name': 'formaPagamentoDelete', 'title': 'Apagar Forma de Pagamento', 'icon': 'fa-times'},
    {'name': 'formaPagamentoEdit', 'title': 'Editar Forma de Pagamento', 'icon': 'fa-pencil-square-o'},

    {'name': 'olhosNew',  'title': 'Criar Olhos', 'icon': 'fa-plus'},
    {'name': 'olhosList', 'title': 'Lista de Olhos', 'icon': 'fa-user'},
    {'name': 'olhosShow', 'title': 'Olhos', 'icon': 'fa-user'},
    {'name': 'olhosDelete', 'title': 'Apagar Olhos', 'icon': 'fa-times'},
    {'name': 'olhosEdit', 'title': 'Editar Olhos', 'icon': 'fa-pencil-square-o'},

    {'name': 'categoriaNew',  'title': 'Criar Categoria', 'icon': 'fa-plus'},
    {'name': 'categoriaList', 'title': 'Lista de Categorias', 'icon': 'fa-user'},
    {'name': 'categoriaShow', 'title': 'Categoria', 'icon': 'fa-user'},
    {'name': 'categoriaDelete', 'title': 'Apagar Categoria', 'icon': 'fa-times'},
    {'name': 'categoriaEdit', 'title': 'Editar Categoria', 'icon': 'fa-pencil-square-o'},

    {'name': 'biotipoNew',  'title': 'Criar Biotipo', 'icon': 'fa-plus'},
    {'name': 'biotipoList', 'title': 'Lista de Biotipos', 'icon': 'fa-user'},
    {'name': 'biotipoShow', 'title': 'Biotipo', 'icon': 'fa-user'},
    {'name': 'biotipoDelete', 'title': 'Apagar Biotipo', 'icon': 'fa-times'},
    {'name': 'biotipoEdit', 'title': 'Editar Biotipo', 'icon': 'fa-pencil-square-o'},

    {'name': 'seiosNew',  'title': 'Criar Seios', 'icon': 'fa-plus'},
    {'name': 'seiosList', 'title': 'Lista de Seios', 'icon': 'fa-user'},
    {'name': 'seiosShow', 'title': 'Seios', 'icon': 'fa-user'},
    {'name': 'seiosDelete', 'title': 'Apagar Seios', 'icon': 'fa-times'},
    {'name': 'seiosEdit', 'title': 'Editar Seios', 'icon': 'fa-pencil-square-o'},

    {'name': 'idiomaNew',  'title': 'Criar Idioma', 'icon': 'fa-plus'},
    {'name': 'idiomaList', 'title': 'Lista de Idiomas', 'icon': 'fa-user'},
    {'name': 'idiomaShow', 'title': 'Idioma', 'icon': 'fa-user'},
    {'name': 'idiomaDelete', 'title': 'Apagar Idioma', 'icon': 'fa-times'},
    {'name': 'idiomaEdit', 'title': 'Editar Idioma', 'icon': 'fa-pencil-square-o'},

    {'name': 'cidadeNew',  'title': 'Criar Cidade', 'icon': 'fa-plus'},
    {'name': 'cidadeList', 'title': 'Lista de Cidades', 'icon': 'fa-user'},
    {'name': 'cidadeShow', 'title': 'Cidade', 'icon': 'fa-user'},
    {'name': 'cidadeDelete', 'title': 'Apagar Cidade', 'icon': 'fa-times'},
    {'name': 'cidadeEdit', 'title': 'Editar Cidade', 'icon': 'fa-pencil-square-o'},
    {'name': 'filtraCidade', 'title': 'Filtrar Cidade', 'icon': ''},
    {'name': 'alteraRegiao', 'title': 'Alterar Regiao', 'icon': ''},
    {'name': 'pegaEstados', 'title': 'Pegar Estados', 'icon': ''},

    {'name': 'estadoNew',  'title': 'Criar Estado', 'icon': 'fa-plus'},
    {'name': 'estadoList', 'title': 'Lista de Estados', 'icon': 'fa-user'},
    {'name': 'estadoShow', 'title': 'Estado', 'icon': 'fa-user'},
    {'name': 'estadoDelete', 'title': 'Apagar Estado', 'icon': 'fa-times'},
    {'name': 'estadoEdit', 'title': 'Editar Estado', 'icon': 'fa-pencil-square-o'},

    {'name': 'planoNew',  'title': 'Criar Plano', 'icon': 'fa-plus'},
    {'name': 'planoList', 'title': 'Lista de Planos', 'icon': 'fa-user'},
    {'name': 'planoShow', 'title': 'Plano', 'icon': 'fa-user'},
    {'name': 'planoDelete', 'title': 'Apagar Plano', 'icon': 'fa-times'},
    {'name': 'planoEdit', 'title': 'Editar Plano', 'icon': 'fa-pencil-square-o'},
    {'name': 'filtrarPlano', 'title': 'Filtrar Plano', 'icon': ''},
    ]
    
	for url in urls:
		if url['name'] == url_name:
			return url['title'], url['icon']

