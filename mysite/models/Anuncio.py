#!/usr/bin/python
#-*- coding: utf-8 -*-
from django.db import models
from django.utils.encoding import smart_str
from mysite.models import *
from mysite.settings import BASE_PATH
from django.utils.translation import gettext_lazy as _
from django.core.files.storage import default_storage
from django.core.validators import  MinValueValidator 


class Anuncio (models.Model):
    def content_file_name(instance, filename):
        path = 'anuncio/'+ str(instance.anunciante.id)+'/'+ filename
        if default_storage.exists(path):
            return 'anuncio/'+ str(instance.anunciante.id)+ '/1_'+ filename
        else:
            return  path
    
    anunciante = models.ForeignKey(Anunciante, 
    related_name = 'anunciante',
    verbose_name = 'anunciante',
    on_delete = models.PROTECT,
    blank=False)

    categoria = models.ForeignKey(Categoria, verbose_name='Categoria', on_delete=models.PROTECT, blank=False, null = False)   

    estado = models.ForeignKey(Estado, verbose_name='Estado', on_delete=models.PROTECT, blank=False, null = False, default = 1)

    cidade = models.ForeignKey(Cidade, verbose_name='Cidade', on_delete=models.PROTECT, blank=False, null = False, default = 1)  

    plano = models.ForeignKey(Plano, verbose_name='Plano', on_delete=models.PROTECT, blank=False, null = False, default = 1,  help_text="Planos Destaque contém 4 ensaios com 10 fotos e aparecem no topo. Planos Básicos contem 2 ensaios com 10 fotos.")

    nome = models.CharField('Nome', max_length=50, blank=False,  help_text="Nome que os visitantes irão ver.")

    idade =  models.IntegerField(u'Idade', validators=[MinValueValidator(18)], blank=False)

    peso =  models.FloatField(u'Peso', blank=False)

    altura =  models.FloatField(u'Altura', blank=False,  help_text='Exemplo: 1,65 para 1m e 65cm')

    olhos = models.ForeignKey(Olhos, verbose_name='Olhos', on_delete=models.PROTECT, blank=False, null = False)

    seios = models.ForeignKey(Seios, verbose_name='Seios', on_delete=models.PROTECT, blank=False, null = False)   

    biotipo = models.ForeignKey(Biotipo, verbose_name='Biotipo', on_delete=models.PROTECT, blank=False, null = False)   

    idiomas = models.ManyToManyField(Idioma, verbose_name="Idiomas")

    telefone = models.CharField('Telefone', max_length=50, blank=False,  help_text='Este é o número pelo qual os visitantes entrarão em contato. Recomendamos que possua whatsapp.')

    local = models.BooleanField(_('Possui local'), default=False,  blank=True,  help_text='Assinale se possui local para atendimento')

    formasPagamento = models.ManyToManyField(FormaPagamento, verbose_name="Formas de Pagamento",  help_text='Formas de pagamento que aceita')

    texto = models.TextField('Texto', max_length=10000, blank = True,  help_text='Conte sobre você, seus serviços e o por quê devem entrar em contato. Ressalte seus pontos fortes, o que gosta de fazer.')

    cache =  models.IntegerField(u'Cachê', blank=False,  help_text='Valor cobrado pelo serviço. Se preferir combinar, deixe o valor 0 que aparecerá "A combinar" no anúncio.')

    onlyfans = models.CharField('Onlyfans', max_length=50, blank=True,  help_text="Caso possua, endereço do Onlyfans.")

    privacy = models.CharField('Privacy', max_length=50, blank=True,  help_text="Caso possua, endereço do Privacy.")

    twitter = models.CharField('Twitter', max_length=50, blank=True,  help_text="Caso possua, endereço do Twitter.")

    instagram = models.CharField('Instagram', max_length=50, blank=True,  help_text="Caso possua, endereço do Instagram.")

    atende = models.ManyToManyField(Atende, verbose_name="Atende",  help_text='Assinale o público que atende.')

    caracteristicas = models.ManyToManyField(Caracteristica, verbose_name="Características Físicas",  help_text='Assinale as características físicas na qual se enquadra.')

    informacoes = models.ManyToManyField(Informacao, verbose_name="Informações",  help_text='Assinale os itens que se enquadram nos serviços que executa.')

    capa = models.FileField('Foto principal', upload_to= content_file_name, blank=False,  help_text='Esta foto é a que chamará atenção para seu anúncio.')

    data_ini = models.DateTimeField(blank=True, null = True)

    data_exp = models.DateTimeField('Data de expiração do anúncio', auto_now_add=False, auto_now = False, blank=True, null = True)

    data_pause = models.DateTimeField(blank=True, null = True)

    pausado = models.BooleanField(default=False,  blank=True)
    

    def __str__(self):
  	  a = smart_str(self.nome)
  	  return a

    class Meta:

        verbose_name = u'Anúncio'
        ordering = ['-nome']