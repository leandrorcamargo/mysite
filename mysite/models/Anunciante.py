#!/usr/bin/python
#-*- coding: utf-8 -*-
from django.db import models
from django.conf import settings
from django.utils.encoding import smart_str
from django.utils.translation import gettext_lazy as _
from django.core.validators import  MinValueValidator 

from django.core.files.storage import default_storage
from mysite.models import *

class Anunciante (models.Model):

    def content_file_name(instance, filename):
        filename = filename.replace(" ", "").replace("(", "").replace(")", "")
        path_ini = 'pictures/anunciante/'
        path = path_ini + filename
        path_alt = path
        i = 0
        while default_storage.exists(path_alt):
            path_alt = path_ini + "_" + str(i) + filename
        return  path_alt

    nome = models.CharField('Nome', max_length=50, blank=False)

    user = models.OneToOneField(settings.AUTH_USER_MODEL, 
    on_delete=models.CASCADE, 
    verbose_name=_('User'))

    email = models.EmailField('E-mail', blank=False, help_text="Nele você receberá um e-mail de confirmação do cadastro.")

    idade =  models.IntegerField('Idade', validators=[MinValueValidator(18)], blank=False)

    rg = models.FileField('RG', upload_to= content_file_name, blank=False, help_text="Envie uma imagem do seu RG ou CNH para validação da identidade.")

    segurando_rg = models.FileField('Foto segurando RG', upload_to= content_file_name, blank=False, help_text="Envie uma foto que mostre seu rosto, segurando o documento enviado acima.")

    cpf = models.CharField('CPF', max_length=11, blank=False, unique = True,  help_text="Apenas números")

    telefone = models.CharField('Telefone', max_length=50, blank=False,  help_text="Número visto apenas pela equipe do site. Entraremos em contato por Whatsapp ou ligação para confirmar o cadastro.")

    cep = models.CharField('CEP', max_length=9, blank=False, unique = False)

    estado = models.CharField(u'Estado', max_length=2, blank=False)

    cidade = models.CharField(u'Cidade', max_length=80, blank=False)

    rua = models.CharField(u'Rua', max_length=80, blank=False)

    numero =  models.IntegerField(u'Número', blank=True)

    aprovado = models.BooleanField(_('Aprovado'), default=False,  blank=True)
    
    def __str__(self):
    	a = smart_str(self.nome)
    	return a

    class Meta:

        verbose_name = 'Anunciante'
        ordering = ['-nome']