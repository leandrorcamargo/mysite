#!/usr/bin/python
#-*- coding: utf-8 -*-
from django.db import models
from django.utils.encoding import smart_str
from mysite.models import *
from mysite.models import Anuncio, AnuncioD
from django.db.models import Manager, Q
from django.core.files.storage import default_storage


class Ensaio (models.Model):
    
    def content_file_name(instance, filename):
        filename = filename.replace(" ", "").replace("(", "").replace(")", "")
        if instance.anuncio:
            path = 'pictures/ensaio/'+ str(instance.anuncio.id) + '/'+ filename
            if default_storage.exists(path):
                return 'pictures/ensaio/' +  str(instance.anuncio.id)+ '/1_'+ filename
            else:
                return path
        else:
            path = 'pictures/ensaio/'+ str(instance.anuncioD.id) + '/'+ filename
            if default_storage.exists(path):
                return 'pictures/ensaio/' +  str(instance.anuncioD.id)+ '/1_'+ filename
            else:
                return path

    anuncio = models.ForeignKey(Anuncio, related_name = 'anuncio',
	verbose_name = 'anúncio',
	on_delete = models.PROTECT,
	blank=True, null=True)

    anuncioD = models.ForeignKey(AnuncioD, related_name = 'anuncioD',
	verbose_name = 'anúncio de Dupla',
	on_delete = models.CASCADE,
	blank=True, null=True)

    nome = models.CharField('Nome', max_length=50, blank=False,  help_text='Obrigatório. Titulo do ensaio. Pode ser o que desejar mas sugerimos algo que o descreva como "Natal" para roupas natalinas ou "Dominadora" para algo mais BDSM. Caso queira algo genérico por não ter um ensaio específico, "Lingerie", "Ensaio 1".')

    video = models.FileField('Video', upload_to= content_file_name, blank=True, help_text='Video para seu anúncio. ATENÇÃO: Formato do arquivo deve ser MP4 e tamanho máximo 50 MB.')
    
    foto1 = models.FileField('Foto 1', upload_to= content_file_name, blank=False, help_text='Obrigatório. Foto para seu anúncio. ATENÇÃO: Tamanho máximo 5 MB.')

    foto2 = models.FileField('Foto 2', default="0", upload_to= content_file_name, blank=True, help_text='Foto para seu anúncio. ATENÇÃO: Tamanho máximo 5 MB.')

    foto3 = models.FileField('Foto 3', default="0", upload_to= content_file_name, blank=True, help_text='Foto para seu anúncio. ATENÇÃO: Tamanho máximo 5 MB.')

    foto4 = models.FileField('Foto 4', default="0", upload_to= content_file_name, blank=True, help_text='Foto para seu anúncio. ATENÇÃO: Tamanho máximo 5 MB.')

    foto5 = models.FileField('Foto 5', default="0", upload_to= content_file_name, blank=True, help_text='Foto para seu anúncio. ATENÇÃO: Tamanho máximo 5 MB.')

    foto6 = models.FileField('Foto 6', default="0", upload_to= content_file_name, blank=True, help_text='Foto para seu anúncio. ATENÇÃO: Tamanho máximo 5 MB.')

    foto7 = models.FileField('Foto 7', default="0", upload_to= content_file_name, blank=True, help_text='Foto para seu anúncio. ATENÇÃO: Tamanho máximo 5 MB.')

    foto8 = models.FileField('Foto 8', default="0", upload_to= content_file_name, blank=True, help_text='Foto para seu anúncio. ATENÇÃO: Tamanho máximo 5 MB.')

    foto9 = models.FileField('Foto 9', default="0", upload_to= content_file_name, blank=True, help_text='Foto para seu anúncio. ATENÇÃO: Tamanho máximo 5 MB.')

    foto10 = models.FileField('Foto 10', default="0", upload_to= content_file_name, blank=True, help_text='Foto para seu anúncio. ATENÇÃO: Tamanho máximo 5 MB.')

    class Meta:

        verbose_name = 'Ensaio'
        ordering = ['-nome']
        constraints = [
            models.CheckConstraint(
                check=(
                    Q(anuncio__isnull=False) & 
                    Q(anuncioD__isnull=True)
                ) | (
                    Q(anuncio__isnull=True) & 
                    Q(anuncioD__isnull=False)
                ),
                name='need_one_anuncio',
            )
        ]