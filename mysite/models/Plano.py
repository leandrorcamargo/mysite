#!/usr/bin/python
#-*- coding: utf-8 -*-
from django.db import models
from django.utils.encoding import smart_str
from mysite.models import *


class Plano (models.Model):

  NORMAL = 1
  DESTAQUE = 2
    
  TIPOS_CHOICES = (
    (NORMAL, 'Normal'),
    (DESTAQUE, 'Destaque'),
    )
 

  nome = models.CharField('Nome', max_length=50, blank=False)

  tipo = models.IntegerField(u'Tipo do plano', choices = TIPOS_CHOICES, blank=False, default=False)

  duracao =  models.IntegerField(u'Duração', blank=False, default=False)

  valor = models.DecimalField('Valor Total', blank=False, decimal_places=2, max_digits=6)

  ensaios = models.IntegerField(u'Ensaios', blank=False, default=False)

  cidades = models.ManyToManyField(Cidade, verbose_name="Cidades")

  cortesia = models.BooleanField(default=False,  blank=True)

  dupla = models.BooleanField(default=False,  blank=True)

  ativo = models.BooleanField(default=True,  blank=True)

  def __str__(self):
    a = smart_str(self.nome)
    return a

  class Meta:

    verbose_name = 'Plano'
    ordering = ['-nome']