#!/usr/bin/python
#-*- coding: utf-8 -*-

from django.utils.encoding import smart_str
from django.db import models

class Estado (models.Model):

  nome = models.CharField('Nome', 
    max_length=22,
    blank=False)

  sigla = models.CharField('Sigla', 
    max_length=2,
    blank=False)

  def __str__(self):
    a = smart_str(self.nome)
    return a


  class Meta:

    verbose_name = 'Estado'
    ordering = ['-nome']