#!/usr/bin/python
#-*- coding: utf-8 -*-

from django.utils.encoding import smart_str
from django.db import models
from mysite.models import Anuncio, Anunciante, AnuncioD

class Pagamento (models.Model):

  mercado_pago_status = models.CharField(max_length=50,
    blank=True)

  mercado_pago_payment_id = models.CharField(max_length=50,
    blank=True)

  mercado_pago_merchant_order_id = models.CharField(max_length=50,
    blank=True)

  valor = models.FloatField('Valor', blank=False)

  anunciante = models.ForeignKey(Anunciante, 
    related_name = 'pagamento_anunciante',
    verbose_name = 'anunciante',
    on_delete = models.PROTECT,
    blank=False)

  anuncio = models.ForeignKey(Anuncio, related_name = 'pagamento_anuncio',
	verbose_name = 'anuncio',
	on_delete = models.PROTECT,
	blank=True, null=True)

  anuncioD = models.ForeignKey(AnuncioD, related_name = 'pagamento_anuncioD',
	verbose_name = 'anuncioD',
	on_delete = models.PROTECT,
	blank=True, null=True)

  mercado_pago_preference_id = models.CharField(max_length=50,
    blank=True)

  def __str__(self):
    a = smart_str(self.anuncio.nome)
    return a


  class Meta:

    verbose_name = 'Pagamento'