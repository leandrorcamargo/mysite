#!/usr/bin/python
#-*- coding: utf-8 -*-
from django.db import models
from django.utils.encoding import smart_str
from mysite.models import *
from mysite.settings import BASE_PATH
from django.utils.translation import gettext_lazy as _
from django.core.files.storage import default_storage
from django.core.validators import  MinValueValidator 

class AnuncioD (models.Model):
    def content_file_name(instance, filename):
        filename = filename.replace(" ", "").replace("(", "").replace(")", "")
        path = 'anuncio_dupla/'+ str(instance.anunciante.id)+'/'+ filename
        if default_storage.exists(path):
            return 'anuncio_dupla/'+ str(instance.anunciante.id)+ '/1_'+ filename
        else:
            return  path
    
    anunciante = models.ForeignKey(Anunciante, 
    related_name = 'anuncianteD',
    verbose_name = 'anunciante',
    on_delete = models.PROTECT,
    blank=False)

    plano = models.ForeignKey(Plano, verbose_name='Plano', on_delete=models.PROTECT, blank=False, null = False, default = 1,  help_text="Planos Destaque contém 4 ensaios com 10 fotos e aparecem no topo. Planos Básicos contem 2 ensaios com 10 fotos.")

    estado = models.ForeignKey(Estado, verbose_name='Estado', on_delete=models.PROTECT, blank=False, null = False, default = 1)

    cidade = models.ForeignKey(Cidade, verbose_name='Cidade', on_delete=models.PROTECT, blank=False, null = False, default = 1)

    categoria1 = models.ForeignKey(Categoria, related_name = 'categoria1', verbose_name='Categoria', on_delete=models.PROTECT, blank=False, null = False)

    categoria2 = models.ForeignKey(Categoria, related_name = 'categoria2', verbose_name='Categoria da Parceira', on_delete=models.PROTECT, blank=False, null = False)  

    nome = models.CharField('Nome', max_length=50, blank=False, help_text='Nome para o anúncio. Exemplo: "Julia e Camis"')

    primeiroNome1 = models.CharField(u'Primeiro Nome', max_length=50, blank=False, null=False, help_text='Seu primeiro nome, como o cliente irá te chamar. Exemplo: "Julia"')

    primeiroNome2 = models.CharField(u'Primeiro Nome da Parceira', max_length=50, blank=False, null=False, help_text='Primeiro nome de sua parceira, como o cliente irá chama-la. Exemplo: "Camis"')

    idade1 =  models.IntegerField(u'Idade', validators=[MinValueValidator(18)], blank=False)

    idade2 =  models.IntegerField(u'Idade da Parceira', validators=[MinValueValidator(18)], blank=False)

    peso1 =  models.FloatField(u'Peso', blank=False)

    peso2 =  models.FloatField(u'Peso da Parceira', blank=False)

    altura1 =  models.FloatField(u'Altura', blank=False,  help_text='Exemplo: 1,65 para 1m e 65cm')

    altura2 =  models.FloatField(u'Altura da Parceira', blank=False,  help_text='Exemplo: 1,65 para 1m e 65cm')

    olhos1 = models.ForeignKey(Olhos, related_name = 'olhos1', verbose_name='Olhos', on_delete=models.PROTECT, blank=False, null = False)

    olhos2 = models.ForeignKey(Olhos, related_name = 'olhos2', verbose_name='Olhos da Parceira', on_delete=models.PROTECT, blank=False, null = False)

    seios1 = models.ForeignKey(Seios, related_name = 'seios1', verbose_name='Seios', on_delete=models.PROTECT, blank=False, null = False)

    seios2 = models.ForeignKey(Seios, related_name = 'seios2', verbose_name='Seios da Parceira', on_delete=models.PROTECT, blank=False, null = False) 

    biotipo1 = models.ForeignKey(Biotipo, related_name = 'biotipo1', verbose_name='Biotipo', on_delete=models.PROTECT, blank=False, null = False)

    biotipo2 = models.ForeignKey(Biotipo, related_name = 'biotipo2', verbose_name='Biotipo da Parceira', on_delete=models.PROTECT, blank=False, null = False) 

    idiomas = models.ManyToManyField(Idioma, verbose_name="Idiomas")

    telefone = models.CharField('Telefone', max_length=50, blank=False,  help_text='Este é o número pelo qual os visitantes entrarão em contato. Recomendamos que possua whatsapp.')

    local = models.BooleanField(_('Possui local'), default=False,  blank=True,  help_text='Assinale se possui local para atendimento')

    formasPagamento = models.ManyToManyField(FormaPagamento, verbose_name="Formas de Pagamento",  help_text='Formas de pagamento que aceitam')

    texto = models.TextField('Texto', max_length=10000, blank = True,  help_text='Conte sobre vocês, seus serviços, como interagem e o por quê devem entrar em contato. Ressalte seus pontos fortes, o que gostam de fazer.')

    cache =  models.IntegerField(u'Cachê', blank=False,  help_text='Valor cobrado pelo serviço. Se preferir combinar, deixe o valor 0 que aparecerá "A combinar" no anúncio.')

    onlyfans1 = models.CharField('Onlyfans', max_length=50, blank=True,  help_text="Caso você possua, endereço do Onlyfans.")

    onlyfans2 = models.CharField('Onlyfans da Parceira', max_length=50, blank=True,  help_text="Caso sua parceira possua, endereço do Onlyfans.")

    privacy1 = models.CharField('Privacy', max_length=50, blank=True,  help_text="Caso você possua, endereço do Privacy.")

    privacy2 = models.CharField('Privacy da Parceira', max_length=50, blank=True,  help_text="Caso sua parceira possua, endereço do Privacy.")

    twitter1 = models.CharField('Twitter', max_length=50, blank=True,  help_text="Caso você possua, endereço do Twitter.")

    twitter2 = models.CharField('Twitter da Parceira', max_length=50, blank=True,  help_text="Caso sua parceira possua, endereço do Twitter.")

    instagram1 = models.CharField('Instagram', max_length=50, blank=True,  help_text="Caso você possua, endereço do Instagram.")

    instagram2 = models.CharField('Instagram da Parceira', max_length=50, blank=True,  help_text="Caso sua parceira possua, endereço do Instagram.")

    atende = models.ManyToManyField(Atende, verbose_name="Atende",  help_text='Assinale o público que atende.')

    caracteristicas1 = models.ManyToManyField(Caracteristica, related_name = 'caracteristicas1',verbose_name="Características Físicas",  help_text='Assinale as características físicas na qual você se enquadra.')

    caracteristicas2 = models.ManyToManyField(Caracteristica, related_name = 'caracteristicas2', verbose_name="Características Físicas da Parceira",  help_text='Assinale as características físicas na qual sua parceira se enquadra.')

    informacoes = models.ManyToManyField(Informacao, verbose_name="Informações",  help_text='Assinale os itens que se enquadram nos serviços que executam.')

    capa = models.FileField('Foto principal', upload_to= content_file_name, blank=False,  help_text='Utilize uma foto que ambas apareçam juntas. Esta foto é a que chamará atenção para seu anúncio.')

    data_ini = models.DateTimeField(blank=True, null = True)

    data_exp = models.DateTimeField('Data de expiração do anúncio', auto_now_add=False, auto_now = False, blank=True, null = True)

    data_pause = models.DateTimeField(blank=True, null = True)

    pausado = models.BooleanField(default=False,  blank=True)
    

    def __str__(self):
      a = smart_str(self.nome)
      return a

    class Meta:

        verbose_name = u'AnúncioD'
        ordering = ['-nome']