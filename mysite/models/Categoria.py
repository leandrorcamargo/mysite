#!/usr/bin/python
#-*- coding: utf-8 -*-

from django.utils.encoding import smart_str
from django.db import models

class Categoria (models.Model):

  nome = models.CharField('Nome', 
    max_length=50,
    blank=False)

  def __str__(self):
    a = smart_str(self.nome)
    return a


  class Meta:

    verbose_name = 'Categoria'
    ordering = ['-nome']