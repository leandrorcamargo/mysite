#!/usr/bin/python
#-*- coding: utf-8 -*-

from enum import IntEnum
from django.utils.encoding import smart_str
from django.db import models
from django.conf import settings
from django.utils import timezone 
from django.utils.translation import gettext_lazy as _

from mysite.models import *

class UserConfirmation(models.Model):
  """
  Stores events at :model:`mysite.UserConfirmation`.
  """

  user = models.ForeignKey(settings.AUTH_USER_MODEL, 
    on_delete=models.CASCADE, 
    verbose_name='User')

  token = models.CharField(_('Token'), 
    max_length=100, 
    unique=True)

  created_at = models.DateTimeField(_('Created at'),
    auto_now_add=True)

  category = models.IntegerField(_('Category'), 
    default=False, 
    blank=True)

  confirmed = models.BooleanField(_('Confirmed?'), 
    default=False, 
    blank=True)

  confirmed_at = models.DateTimeField(_('Confirmed at'),
    default= timezone.now, 
    blank=True)

  def __str__(self):
    a = smart_str(self.user+' '+ self.created_at)
    return a

  def save(self, *args, **kwargs):
    UserConfirmation.objects.filter(user=self.user, confirmed=False).update(confirmed=True, confirmed_at=timezone.now() )
    super(UserConfirmation,self).save(*args, **kwargs)

  class Meta:
    verbose_name = _('User Confirmation')
    verbose_name_plural = _('User Confirmations')
    ordering = ['-created_at']

class Category(IntEnum):
  """
  Enumerates categories, extends IntEnum
  """

  VERIFICATION = 1
  PASSWORD = 2