#!/usr/bin/python
#-*- coding: utf-8 -*-
from django.utils.encoding import smart_str
from django.db import models

from mysite.models import *
from mysite.models import Estado

class Cidade (models.Model):

  estado = models.ForeignKey(Estado, verbose_name='Estado', on_delete=models.PROTECT, blank=False)

  nome = models.CharField('Cidade', 
    max_length=50,
    blank=False)

  def __str__(self):
    a = smart_str(self.nome)
    return a


  class Meta:

    verbose_name = 'Cidade'
    ordering = ['-nome']