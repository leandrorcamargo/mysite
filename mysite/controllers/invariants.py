#!/usr/bin/python
#-*- coding: utf-8 -*-

from django.utils.translation import gettext_lazy as _

# Translators: This message appears on alert with %(model_name)s and %(item_name)s
alert_add_success    = u"%s <b>%s</b> was successfully added."
alert_update_success = u"%s <b>%s</b> was successfully updated."
alert_delete_success = u"%s <b>%s</b> was successfully deleted."
alert_delete_error = u"%s <b>%s</b> could not be deleted."
alert_rtu_match_error = u'%s <b>%s</b> doesnt match FSU <b>%s</b>'
alert_not_found_error = u'%s não encontrado.'
alert_token_error = u'Token de confirmação não encontrado.'

# Translators: This message appears on form with %(model_name)s and %(field_name)s
form_already_exists = u"%s with this %s already exists."
form_port_invalid = u'Invalid Port.'
form_port_used = u'Port already in use.'
form_qty_port_clean = u'Port %s is being used. Please, change the sensor port before changing FSU Port Quantity.'

# Translators: This message appears on button with %(model_name)s 
button_update = u"Atualizar %s"
button_create = u"Criar %s"
button_account_confirm = u'Confirmar e-mail'
button_set_password = u'Criar Senha'
button_insert = u"Inserir %s"
button_verify_anunciante = u"Verificar Anunciante"

message_body_confirmation = u"Olá %s, <br/> Seja bem-vindo ao site Hot Anúncios. <br/> Através deste e-mail confirmamos sua conta. Clique no link abaixo para definir uma senha para o seu cadastro.<br/> Guarde-a bem pois através dela você poderá editar seu anúncio ou mudar de plano quando quiser. "
message_subject_confirmation = u'Hot Anúncios - Instruções de Confirmação de Cadastro'
message_body_confirmation2 = u"Olá %s, <br/> Olá! <br/> Através deste e-mail confirmamos a alteração de e-mail em seu cadastro. <br/> Lembramos que ao confirmar esta alteração, você utilizará o endereço deste email para acessar o site. </br> Clique no link abaixo para definir uma senha para o seu cadastro.<br/> Guarde-a bem pois através dela você poderá editar seu anúncio ou mudar de plano quando quiser. "
message_subject_confirmation2 = u'Hot Anúncios - Instruções de Confirmação de Alteração de Email'

message_body_anunciante_created = u"Olá administrador! <br/> Através deste e-mail informamos que um novo anunciante, %s, teve o e-mail confirmado. <br> O contato e a aprovação do anunciante deve ocorrer o mais rápido possível. Favor entrar no site para verificar as informações. "
message_subject_anunciante_created = u'Hot Anúncios - Anunciante Criado'
