#!/usr/bin/python
#-*- coding: utf-8 -*-

from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib import messages
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_protect

from mysite.forms import PlanoForm
from mysite.models import Plano
from mysite.views.plano import planoList
from mysite.views.plano import *

@login_required
@csrf_protect
@user_passes_test(lambda u: u.is_superuser)
def planoNew(request):
  """
  Display region creation screen
  """
  form = PlanoForm(request.POST or None)
  if form.is_valid():    
    plano = form.save()
    messages.success(request, ('Plano %s criado com sucesso.')% (plano.nome))
    return redirect('planoList') 

  return render(request, 'plano/form.html', 
    {'form': form,
     'button': 'Criar plano'})