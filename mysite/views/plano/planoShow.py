#!/usr/bin/python
#-*- coding: utf-8 -*-

from django.contrib.auth.decorators import login_required, user_passes_test
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_protect

from mysite.models import Plano
from mysite.views import *
from mysite.views.plano import *

@login_required
@csrf_protect
@user_passes_test(lambda u: u.is_superuser)
def planoShow(request, planoId):
  """
  Display Region'

  :type  regionId: region identification number
  :param regionId: int
  """

  plano = Plano.objects.get(id=planoId)
  cidades = list(plano.cidades.all())

  return render(request, 'plano/show.html', 
    {'plano': plano, 'cidades': cidades})