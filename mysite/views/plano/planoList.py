#!/usr/bin/python
#-*- coding: utf-8 -*-

from django.contrib.auth.decorators import login_required, user_passes_test
from django.forms.models import fields_for_model
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_protect


from mysite.models import Plano
from mysite.views import *
from mysite.views.plano import *

@login_required
@csrf_protect
@user_passes_test(lambda u: u.is_superuser)
def planoList(request):
  """
  Display all regions 
  """

  planos = Plano.objects.all()

  return render(request, 'plano/list.html',
    {'tableHead': fields_for_model(Plano, fields=('nome', 'tipo', 'duracao', 'valor', 'ativo')),
     'planos': planos})