#!/usr/bin/python
#-*- coding: utf-8 -*-

from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib import messages
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_protect

from mysite.models import Plano
from mysite.views import *
from mysite.views.plano import *

@login_required
@csrf_protect
@user_passes_test(lambda u: u.is_superuser)
def planoDelete(request, planoId):
  """
  Display region edit screen
  if form is valid, saves and shows message

  :type  regionId: region identification number
  :param regionId: int
  """
  model_name = Plano._meta.verbose_name
  plano = Plano.objects.get(id=planoId)
  nome = plano.nome
  
  try:
    plano.delete()
    messages.success(request, 'Plano %s apagada com sucesso.' % (nome))
  except Exception as e:
    messages.error(request, 'Erro ao apagar plano.') 

  return redirect(planoList) 