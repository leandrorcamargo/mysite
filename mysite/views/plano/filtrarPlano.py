#!/usr/bin/python
#-*- coding: utf-8 -*-

from mysite.models import Plano, Cidade
from django.http import JsonResponse
import json
from django.views.decorators.csrf import csrf_protect

@csrf_protect
def filtrarPlano (request,cidadeId):

  planos_data = []
  planos = list(Plano.objects.filter(cidades__pk=cidadeId, ativo = True))
  for plano in planos:
    planos_data.append({ 
      'id': plano.id,
      'data': plano.nome,  
    })

  return JsonResponse((planos_data), status = 200, safe=False)