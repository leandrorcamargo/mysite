#!/usr/bin/python
#-*- coding: utf-8 -*-

from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib import messages
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_protect

from mysite.forms import PlanoForm
from mysite.models import Plano
from mysite.views.plano import *

@login_required
@csrf_protect
@user_passes_test(lambda u: u.is_superuser)
def planoEdit(request, planoId):
  """
  Display region edit screen
  if form is valid, saves and shows message

  :type  regionId: region identification number
  :param regionId: int
  """

  model_name = Plano._meta.verbose_name
  plano = Plano.objects.get(id=planoId) 
  

  form = PlanoForm(instance=plano, data=request.POST or None)
  
  if form.is_valid():    
    plano = form.save()
    messages.success(request, 'Cadastro do plano %s atualizado com sucesso.' % (plano.nome))
    return redirect(planoShow, planoId=planoId)

  return render(request, 'plano/form.html', 
    {'title': ': <b>%s</b>' % (plano.nome),
     'form': form,
     'button': 'Editar Plano'})