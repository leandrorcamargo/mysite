#!/usr/bin/python
#-*- coding: utf-8 -*-

from django.contrib.auth.decorators import login_required, user_passes_test
from django.forms.models import fields_for_model
from django.shortcuts import render
from django.views.decorators.csrf import csrf_protect


from mysite.models import FormaPagamento
from mysite.views import *
from mysite.views.formaPagamento import *

@login_required
@csrf_protect
@user_passes_test(lambda u: u.is_superuser)
def formaPagamentoList(request):
  """
  Display all regions 
  """

  formasPagamento = FormaPagamento.objects.all()

  return render(request, 'formaPagamento/list.html',
    {'tableHead': fields_for_model(FormaPagamento, fields=('nome', 'id')),
     'formasPagamento': formasPagamento})