#!/usr/bin/python
#-*- coding: utf-8 -*-

from django.contrib.auth.decorators import login_required, user_passes_test
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_protect

from mysite.models import FormaPagamento
from mysite.views import *
from mysite.views.formaPagamento import *

@login_required
@csrf_protect
@user_passes_test(lambda u: u.is_superuser)
def formaPagamentoShow(request, formaPagamentoId):
  """
  Display Region'

  :type  regionId: region identification number
  :param regionId: int
  """

  formaPagamento = FormaPagamento.objects.get(id=formaPagamentoId)

  return render(request, 'formaPagamento/show.html', 
    {'formaPagamento': formaPagamento})