#!/usr/bin/python
#-*- coding: utf-8 -*-

from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib import messages
from django.views.decorators.csrf import csrf_protect
from django.http import HttpResponseRedirect
from mysite.settings import BASE_PATH

from mysite.models import FormaPagamento
from mysite.views import *
from mysite.views.formaPagamento import *

@login_required
@csrf_protect
@user_passes_test(lambda u: u.is_superuser)
def formaPagamentoDelete(request, formaPagamentoId):
  """
  Display region edit screen
  if form is valid, saves and shows message

  :type  regionId: region identification number
  :param regionId: int
  """
  model_name = FormaPagamento._meta.verbose_name
  formaPagamento = FormaPagamento.objects.get(id=formaPagamentoId)
  nome = formaPagamento.nome
  
  try:
    formaPagamento.delete()
    messages.success(request, 'Forma de Pagamento %s apagada com sucesso.' % (nome))
  except Exception as e:
    messages.error(request, 'Erro ao apagar forma de pagamento.') 

  location = BASE_PATH + 'formaPagamento/'
  return HttpResponseRedirect(location)