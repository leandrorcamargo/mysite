#!/usr/bin/python
#-*- coding: utf-8 -*-

from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib import messages
from django.shortcuts import render
from django.views.decorators.csrf import csrf_protect
from django.http import HttpResponseRedirect
from mysite.settings import BASE_PATH

from mysite.forms import FormaPagamentoForm
from mysite.views.formaPagamento import *

@login_required
@csrf_protect
@user_passes_test(lambda u: u.is_superuser)
def formaPagamentoNew(request):
  """
  Display region creation screen
  """
  form = FormaPagamentoForm(request.POST or None)
  if form.is_valid():    
    formaPagamento = form.save()
    messages.success(request, ('FormaPagamento %s criada com sucesso.')% (formaPagamento.nome))
    location = BASE_PATH + 'formaPagamento/'
    return HttpResponseRedirect(location)

  return render(request, 'formaPagamento/form.html', 
    {'form': form,
     'button': 'Criar Forma de Pagamento'})