#!/usr/bin/python
#-*- coding: utf-8 -*-

from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib import messages
from django.shortcuts import render
from django.views.decorators.csrf import csrf_protect
from django.http import HttpResponseRedirect
from mysite.settings import BASE_PATH

from mysite.forms import FormaPagamentoForm
from mysite.models import FormaPagamento
from mysite.views.formaPagamento import *

@login_required
@csrf_protect
@user_passes_test(lambda u: u.is_superuser)
def formaPagamentoEdit(request, formaPagamentoId):
  """
  Display region edit screen
  if form is valid, saves and shows message

  :type  regionId: region identification number
  :param regionId: int
  """

  model_name = FormaPagamento._meta.verbose_name
  formaPagamento = FormaPagamento.objects.get(id=formaPagamentoId) 
  

  form = FormaPagamentoForm(instance=formaPagamento, data=request.POST or None)
  
  if form.is_valid():    
    formaPagamento = form.save()
    messages.success(request, 'Cadastro da Forma de Pagamento %s atualizado com sucesso.' % (formaPagamento.nome))
    location = BASE_PATH + 'formaPagamento/'
    return HttpResponseRedirect(location)

  return render(request, 'formaPagamento/form.html', 
    {'title': ': <b>%s</b>' % (formaPagamento.nome),
     'form': form,
     'button': 'Editar Forma de Pagamento'})