#!/usr/bin/python
#-*- coding: utf-8 -*-

#from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.views.decorators.csrf import csrf_protect
from django.http import HttpResponseRedirect
from mysite.settings import BASE_PATH

from mysite.models import AnuncioD, Pagamento
from mysite.views import *
from mysite.views.anuncioD import *

@login_required
@csrf_protect
def anuncioDDelete(request, anuncioDId):

  anuncioD = AnuncioD.objects.get(id=anuncioDId)
  user = anuncioD.anunciante.user
  nome =anuncioD.nome
  
  try:
    if request.user.is_superuser or user==request.user:
      pagamentos = list (Pagamento.objects.filter(anuncioD = anuncioD))
      for pagamento in pagamentos:
        pagamento.delete()
      try:
        anuncioD.capa.delete(save=False)
      except:
        pass
      anuncioD.delete()
      del request.session['anunciosD_list'] 
      messages.success(request, u'Anúncio %s apagado com sucesso.' % (nome))
    else:
      messages.error(request, u'Usuário sem autorização para deletar este anúncio.') 

  except Exception as e:
    #messages.error(request, e) 
    messages.error(request, "Erro ao apagar o anúncio. Contate nosso suporte que resolveremos para você.") 


  location = BASE_PATH + 'anuncioD/'
  return HttpResponseRedirect(location)