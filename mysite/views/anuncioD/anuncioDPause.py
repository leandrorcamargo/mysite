#!/usr/bin/python
#-*- coding: utf-8 -*-

from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.views.decorators.csrf import csrf_protect
from django.http import HttpResponseRedirect
from mysite.settings import BASE_PATH
from django.utils import timezone
import datetime

from mysite.models import AnuncioD
from mysite.views import *
from mysite.views.anuncioD import *

@login_required
@csrf_protect
def anuncioDPause(request, anuncioDId):

  model_name = AnuncioD._meta.verbose_name
  anuncioD = AnuncioD.objects.get(id=anuncioDId)
  user = anuncioD.anunciante.user
  nome =anuncioD.nome
  pausado = anuncioD.pausado
  data_pause = anuncioD.data_pause
  today =  timezone.now()
  
  try:
    if request.user.is_superuser or user==request.user:
      if pausado:
        anuncioD.pausado = False
        incremento = abs(today - data_pause).days
        anuncioD.data_exp = anuncioD.data_exp + datetime.timedelta(days = incremento)
        anuncioD.data_pause = None
        anuncioD.save()
        messages.success(request, u'Anúncio %s reativado com sucesso.' % (nome))
      else:
        anuncioD.pausado = True
        anuncioD.data_pause = today
        anuncioD.save()
        messages.success(request, u'Anúncio %s pausado com sucesso.' % (nome))
    else:
      messages.error(request, u'Usuário sem autorização para pausar este anúncio.') 

  except Exception as e:
    messages.error(request, u'Erro ao pausar anúncio.') 

  location = BASE_PATH + 'anuncioD/'
  return HttpResponseRedirect(location)