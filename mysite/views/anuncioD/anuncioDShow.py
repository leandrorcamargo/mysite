#!/usr/bin/python
#-*- coding: utf-8 -*-

from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_protect

from mysite.models import AnuncioD, Ensaio, Pagamento
from mysite.views import *
from mysite.views.anuncioD import *


@csrf_protect
def anuncioDShow(request, anuncioDId):
  
  anuncioD = AnuncioD.objects.get(id=anuncioDId)
  ensaios = list (Ensaio.objects.filter(anuncioD = anuncioD))
  caracteristicas1 = list(anuncioD.caracteristicas1.all())
  caracteristicas2 = list(anuncioD.caracteristicas2.all())
  idiomas = list(anuncioD.idiomas.all())
  informacoes = list(anuncioD.informacoes.all())
  formasPagamento = list(anuncioD.formasPagamento.all())
  atende = list(anuncioD.atende.all())

  usuario = request.user

  if usuario.is_superuser:
    usuario = anuncioD.anunciante.user

  if ((anuncioD.anunciante.user == usuario) and ('anunciante' in request.session) and (Pagamento.objects.filter(anuncioD=anuncioD, mercado_pago_payment_id = '').exists())): 
    pagamento_pendente = Pagamento.objects.get(anuncioD=anuncioD, mercado_pago_payment_id = '')
  else:
    pagamento_pendente = False

  return render(request, 'anuncioD/show.html', 
    {'anuncioD': anuncioD,
    'caracteristicas1': caracteristicas1,
    'caracteristicas2': caracteristicas2,
    'informacoes': informacoes,
    'formasPagamento':formasPagamento,
    'idiomas':idiomas,
    'atende':atende,
    'ensaios': ensaios,
    'pagamento_pendente':pagamento_pendente})