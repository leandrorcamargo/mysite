#!/usr/bin/python
#-*- coding: utf-8 -*-

from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_protect
from django.http import JsonResponse
from django.core.files.storage import default_storage as storage
from mysite.settings import BASE_PATH, MEDIA_URL, MERCADO_PG_TKN
from mysite.controllers import invariants
from mysite.models import Anunciante, AnuncioD, Pagamento, Plano
from mysite.forms import AnuncioDForm
from django.utils import timezone
from django.core.files.storage import default_storage

import mercadopago
import datetime
import os
from io import BytesIO
from PIL import Image, ExifTags, ImageOps
import magic

def add_watermark(image_file, filesRotated, anuncianteId, logo_file ="static/mysite/logo2.png"):
  filesRotated[image_file].name = filesRotated[image_file].name.replace(" ", "").replace("(", "").replace(")", "")
  path = 'anuncio_dupla/' + str(anuncianteId)+ '/1_'+ filesRotated[image_file].name
  if default_storage.exists(path):
    im = storage.open(r'anuncio_dupla/' + str(anuncianteId) + r'/1_' +filesRotated[image_file].name ,'rb')
  else:
    im = storage.open(r'anuncio_dupla/' + str(anuncianteId) + r'/' +filesRotated[image_file].name ,'rb')
  img = Image.open(im).convert('RGBA')
  

  sfile = BytesIO()

  logo = Image.open(logo_file).convert('RGBA')
  logo.load()
  new_size = min(img.size[0], img.size[1])
  logo = logo.resize((new_size, new_size))
  paste_mask = logo.point(lambda i: i * 20 / 100)

  # position the watermark
  offset_x = int(img.size[0]/2 - logo.size[0]/2)
  offset_y = int(img.size[1]/2 - logo.size[1]/2)

  
  img.paste(logo, (offset_x, offset_y), mask = paste_mask)
  img = img.convert('RGB')
  img.save(sfile, "JPEG")
  im.close()
  img.close()
  #imw = storage.open(MEDIA_URL + 'pictures/ensaio/' + str(anuncianteId) + '/' +filesRotated[image_file].name,'wb')
  if default_storage.exists(path):
    imw = storage.open(r'anuncio_dupla/' + str(anuncianteId) + r'/1_' +filesRotated[image_file].name, 'wb')
  else:
    imw = storage.open(r'anuncio_dupla/' + str(anuncianteId) + r'/' +filesRotated[image_file].name ,'wb')
  imw.write(sfile.getvalue())
  imw.flush()
  imw.close()

@login_required
@csrf_protect
def anuncioDNew(request):
  
  mp = mercadopago.SDK(MERCADO_PG_TKN)
  usuario = request.user
  model_name = AnuncioD._meta.verbose_name
  anunciante = Anunciante.objects.get(id=request.session['anunciante'])
  if not ('cidade' in request.session):
    return redirect("index")
      
  request.POST._mutable = True
  filesRotated = request.FILES or None
  if filesRotated:
    for f in filesRotated:
      try:
        image=Image.open(filesRotated[f])
        for orientation in ExifTags.TAGS.keys():
          if ExifTags.TAGS[orientation]=='Orientation':
            break
        exif=dict(image._getexif().items())

        if exif[orientation] == 3:
          image = image.rotate(180, expand=True)
        elif exif[orientation] == 6:
          image=image.rotate(270, expand=True)
        elif exif[orientation] == 8:
          image = image.rotate(90, expand=True)
        image.thumbnail((1000 , 1000), Image.ANTIALIAS)
        image.save(filesRotated[f].temporary_file_path(), "JPEG")
        image.close()
      except (AttributeError, KeyError, IndexError):
        pass
  
  form = AnuncioDForm(anunciante = anunciante, user = usuario, edit = False, data=request.POST or None, files = filesRotated or None)
  if request.session['tem_anuncio']:
    form.fields["plano"].queryset = Plano.objects.filter(cidades__pk=request.session['cidade'], dupla=True)
  else:
    form.fields["plano"].queryset = Plano.objects.filter(cidades__pk=request.session['cidade'], dupla=True, cortesia=False)

  if form.is_valid():
    anuncioD = form.save()
    anuncioD.data_ini = timezone.now() - datetime.timedelta(hours=3)
    anuncioD.data_exp = anuncioD.data_ini 
    anuncioD.save()

    valid_mime_types = ['image/jpeg', 'image/png', 'image/gif']
    for idx, img_file in enumerate(filesRotated):
      file_mime_type = magic.from_buffer(img_file, mime=True)
      if file_mime_type in valid_mime_types:
        add_watermark(img_file, filesRotated,  anuncioD.anunciante.id)

    pagamento = Pagamento.objects.create(anunciante = anunciante, anuncio = None, anuncioD=anuncioD, valor = anuncioD.plano.valor)
    if anuncioD.plano.cortesia:
      pagamento.save()
      novo_plano = Plano.objects.get(cidades__pk=anuncioD.cidade.id, tipo = anuncioD.plano.tipo, duracao = anuncioD.plano.duracao, dupla = True, cortesia = False)
      anuncioD.plano = novo_plano
      anuncioD.save()
      messages.success(request, u"Anúncio criado com sucesso.")
      location = BASE_PATH + 'success/'  + str(pagamento.id)+"/?payment_id=0&merchant_order_id=0&status=approved"
      return JsonResponse((location), status = 200, safe=False)

    else:
      preference_data = {
      "items": [
          {
              "title": str(anuncioD.plano.nome),
              "quantity": 1,
              "unit_price": float(anuncioD.plano.valor)
          }
        ],
        "back_urls": {
          "success": BASE_PATH + "success/"  + str(pagamento.id)+"/",
          "failure": BASE_PATH + "failure/"  + str(pagamento.id)+"/",
          "pending": BASE_PATH + "pending/"  + str(pagamento.id)+"/",
        },
      "auto_return": "approved"
      }
    
      preference_response = mp.preference().create(preference_data)
      preference = preference_response["response"]
      preference_id = preference["id"]

      pagamento.mercado_pago_preference_id = preference_id
      pagamento.save()

      del request.session['anunciosD_list'] 
      messages.success(request, u"Anúncio criado com sucesso.")
      #location = BASE_PATH + 'anuncioD/' 
      location = BASE_PATH + 'process/' + preference_id +'/getForm/' + str(anuncioD.id) +'/'
      return JsonResponse((location), status = 200, safe=False)

  return render(request, 'anuncioD/form.html', 
    {'form': form,
     'button':u"Criar e pagar anúncio"})