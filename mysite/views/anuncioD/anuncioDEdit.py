
#!/usr/bin/python
#-*- coding: utf-8 -*-

from mysite.settings import BASE_PATH, MEDIA_URL
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.shortcuts import render, redirect
from django.core.files.storage import default_storage as storage
from django.views.decorators.csrf import csrf_protect
from django.http import JsonResponse, HttpResponseRedirect
from django.core.files.storage import default_storage

from mysite.forms import AnuncioDForm
from mysite.models import AnuncioD, Anunciante, Plano, Cidade
from mysite.views.anuncioD import  anuncioDList
from mysite.views.anuncioD import *
from io import BytesIO
from PIL import Image, ExifTags, ImageOps
import magic

def add_watermark(image_file, filesRotated, anuncianteId, logo_file ="static/mysite/logo2.png"):
  filesRotated[image_file].name = filesRotated[image_file].name.replace(" ", "").replace("(", "").replace(")", "")
  path = 'anuncio_dupla/' + str(anuncianteId)+ '/1_'+ filesRotated[image_file].name
  if default_storage.exists(path):
    im = storage.open(r'anuncio_dupla/' + str(anuncianteId) + r'/1_' +filesRotated[image_file].name ,'rb')
  else:
    im = storage.open(r'anuncio_dupla/' + str(anuncianteId) + r'/' +filesRotated[image_file].name ,'rb')
  img = Image.open(im).convert('RGBA')
  

  sfile = BytesIO()

  logo = Image.open(logo_file).convert('RGBA')
  logo.load()
  new_size = min(img.size[0], img.size[1])
  logo = logo.resize((new_size, new_size))
  paste_mask = logo.point(lambda i: i * 20 / 100)

  # position the watermark
  offset_x = int(img.size[0]/2 - logo.size[0]/2)
  offset_y = int(img.size[1]/2 - logo.size[1]/2)

  
  img.paste(logo, (offset_x, offset_y), mask = paste_mask)
  img = img.convert('RGB')
  img.save(sfile, "JPEG")
  im.close()
  img.close()
  if default_storage.exists(path):
    imw = storage.open(r'anuncio_dupla/' + str(anuncianteId) + r'/1_' +filesRotated[image_file].name,'wb')
  else:
    imw = storage.open(r'anuncio_dupla/' + str(anuncianteId) + r'/' +filesRotated[image_file].name ,'wb')
  imw.write(sfile.getvalue())
  imw.flush()
  imw.close()

@login_required
@csrf_protect
def anuncioDEdit(request, anuncioDId):
  """
  Display region edit screen
  if form is valid, saves and shows message

  :type  regionId: region identification number
  :param regionId: int
  """

  usuario = request.user
  model_name = AnuncioD._meta.verbose_name
  anuncioD = AnuncioD.objects.get(id=anuncioDId)
  if usuario.is_superuser:
    anunciante = anuncioD.anunciante
  else:
    anunciante = Anunciante.objects.get(id=request.session['anunciante'])
    if anuncioD.anunciante.user == request.user:
      pass
    else:
      return redirect(anuncioDList)
  
  request.POST._mutable = True
  filesRotated = request.FILES or None
  if filesRotated:
    for f in filesRotated:
      try:
        image=Image.open(filesRotated[f])
        for orientation in ExifTags.TAGS.keys():
          if ExifTags.TAGS[orientation]=='Orientation':
            break
        exif=dict(image._getexif().items())

        if exif[orientation] == 3:
          image = image.rotate(180, expand=True)
        elif exif[orientation] == 6:
          image=image.rotate(270, expand=True)
        elif exif[orientation] == 8:
          image = image.rotate(90, expand=True)
        image.thumbnail((1000 , 1000), Image.ANTIALIAS)
        image.save(filesRotated[f].temporary_file_path(), "JPEG")
        image.close()
      except (AttributeError, KeyError, IndexError):
        pass
  form = AnuncioDForm(instance=anuncioD, anunciante = anunciante, user = usuario, edit= True,  data=request.POST or None, files = filesRotated or None)
  form.fields["cidade"].queryset = Cidade.objects.filter(estado__pk=request.session['estado'])
  form.fields["plano"].queryset = Plano.objects.filter(cidades__pk=request.session['cidade'], ativo = True)

  
  if form.is_valid():
    if form.cleaned_data.get('capa') != anuncioD.capa:
      try:
        anuncioD.capa.delete(save=False)
      except:
        pass
    anuncioD = form.save()
    if filesRotated:
      valid_mime_types = ['image/jpeg', 'image/png', 'image/gif']
      for idx, img_file in enumerate(filesRotated):
        file_mime_type = magic.from_buffer(img_file, mime=True)
        if file_mime_type in valid_mime_types:
          add_watermark(img_file, filesRotated,  anunciante.id)
    messages.success(request, u'Anúncio %s atualizado com sucesso.' % (anuncioD.nome))
    location = BASE_PATH + 'anuncioD/s' + str(anuncioD.id)+"/"
    return JsonResponse((location), status = 200, safe=False)
    #return HttpResponseRedirect(location)
  else:
    form = AnuncioDForm(instance=anuncioD, anunciante = anunciante, user = usuario, edit = True)
    form.fields["plano"].queryset = Plano.objects.filter(cidades__pk=request.session['cidade'], ativo = True)
    
    return render(request, 'anuncioD/form.html', 
      {'title': ': <b>%s</b>' % (anuncioD.nome),
      'form': form,
      'button': u'Editar Anúncio'})

