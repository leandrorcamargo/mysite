#!/usr/bin/python
#-*- coding: utf-8 -*-

#from django.contrib.auth.decorators import login_required
from random import shuffle
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_protect


from mysite.models import AnuncioD, Categoria
from mysite.views import *
from mysite.views.anuncioD import *
from mysite.views.index import *
from datetime import datetime

#@login_required
@csrf_protect
def anuncioDList(request):
  
  today =  datetime.now()
  anuncios_destaques = []
  anuncios_basicos = []
  if 'cidade' in request.session:
    estado = request.session['estado']
    cidade = request.session['cidade']
    anuncios = list(AnuncioD.objects.filter(estado = estado, cidade = cidade, anunciante__aprovado = True, data_exp__gte = today, pausado = False ))
    for anuncioD in anuncios:
      if anuncioD.plano.tipo == 1:
        anuncios_basicos.append(anuncioD)
      else:
        anuncios_destaques.append(anuncioD)
    shuffle(anuncios_basicos)
    shuffle(anuncios_destaques)

  else:
    return redirect("index")

  return render(request, 'anuncioD/list.html',
    {'anuncios_basicos': anuncios_basicos,
    'anuncios_destaques': anuncios_destaques})