#!/usr/bin/python
#-*- coding: utf-8 -*-

from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.views.decorators.csrf import csrf_protect
from django.http import JsonResponse, HttpResponseRedirect
from mysite.settings import BASE_PATH
from django.utils import timezone
import datetime

from mysite.models import Anuncio
from mysite.views import *
from mysite.views.anuncio import *

@login_required
@csrf_protect
def anuncioPause(request, anuncioId):

  model_name = Anuncio._meta.verbose_name
  anuncio = Anuncio.objects.get(id=anuncioId)
  user = anuncio.anunciante.user
  nome =anuncio.nome
  pausado = anuncio.pausado
  data_pause = anuncio.data_pause
  today =  timezone.now()
  
  try:
    if request.user.is_superuser or user==request.user:
      if pausado:
        anuncio.pausado = False
        incremento = abs(today - data_pause).days
        anuncio.data_exp = anuncio.data_exp + datetime.timedelta(days = incremento)
        anuncio.data_pause = None
        anuncio.save()
        messages.success(request, u'Anúncio %s reativado com sucesso.' % (nome))
      else:
        anuncio.pausado = True
        anuncio.data_pause = today
        anuncio.save()
        messages.success(request, u'Anúncio %s pausado com sucesso.' % (nome))
    else:
      messages.error(request, u'Usuário sem autorização para pausar este anúncio.') 

  except Exception as e:
    messages.error(request, u'Erro ao pausar anúncio.') 

  location = BASE_PATH + 'anuncio/'
  return HttpResponseRedirect(location)