#!/usr/bin/python
#-*- coding: utf-8 -*-

from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_protect
from django.http import HttpResponseRedirect, JsonResponse
from django.core.files.storage import default_storage as storage
from mysite.settings import BASE_PATH, MEDIA_URL, MERCADO_PG_TKN
from mysite.controllers import invariants
from mysite.models import Anunciante, Anuncio, Pagamento, Plano
from mysite.views.anuncio import  anuncioList
from mysite.forms import AnuncioForm
from django.utils import timezone

import mercadopago
import datetime
import os
from io import BytesIO

@login_required
@csrf_protect
def anuncioRenew(request, anuncioId):
  
  mp = mercadopago.SDK(MERCADO_PG_TKN)
  model_name = Anuncio._meta.verbose_name
  usuario = request.user
  model_name = Anuncio._meta.verbose_name
  anuncio = Anuncio.objects.get(id=anuncioId)
  if usuario.is_superuser:
    anunciante = anuncio.anunciante
  else:
    anunciante = Anunciante.objects.get(id=request.session['anunciante'])
    if anuncio.anunciante.user == request.user:
      pass
    else:
      return redirect(anuncioList)
      
  
  pagamento = Pagamento.objects.create(anunciante = anunciante, anuncio=anuncio, anuncioD = None, valor = anuncio.plano.valor)
  preference_data = {
  "items": [
      {
          "title": str(anuncio.plano.nome),
          "quantity": 1,
          "unit_price": float(anuncio.plano.valor)
      }
    ],
    "back_urls": {
      "success": BASE_PATH + "success/"  + str(pagamento.id)+"/",
      "failure": BASE_PATH + "failure/"  + str(pagamento.id)+"/",
      "pending": BASE_PATH + "pending/"  + str(pagamento.id)+"/",
    },
  "auto_return": "approved"
  }
  preference_response = mp.preference().create(preference_data)
  preference = preference_response["response"]
  preference_id = preference["id"]

  pagamento.mercado_pago_preference_id = preference_id
  pagamento.save()

  messages.success(request, u"Pedido de renovação feito com sucesso. Aguardando pagamento.")
  location = BASE_PATH + 'process/' + preference_id +'/getForm/' + str(anuncio.id) +'/'
  return HttpResponseRedirect(location)
