#!/usr/bin/python
#-*- coding: utf-8 -*-

#from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.views.decorators.csrf import csrf_protect
from django.http import JsonResponse, HttpResponseRedirect
from mysite.settings import BASE_PATH

from mysite.models import Anuncio, Pagamento
from mysite.views import *
from mysite.views.anuncio import *

@login_required
@csrf_protect
def anuncioDelete(request, anuncioId):

  model_name = Anuncio._meta.verbose_name
  anuncio = Anuncio.objects.get(id=anuncioId)
  user = anuncio.anunciante.user
  nome =anuncio.nome
  
  try:
    if request.user.is_superuser or user==request.user:
      pagamentos = list (Pagamento.objects.filter(anuncio = anuncio))
      for pagamento in pagamentos:
        pagamento.delete()
      try:
        anuncio.capa.delete(save=False)
      except:
        pass
      anuncio.delete()
      del request.session['anuncios_list'] 
      messages.success(request, u'Anúncio %s apagado com sucesso.' % (nome))
    else:
      messages.error(request, u'Usuário sem autorização para deletar este anúncio.') 

  except Exception as e:
    messages.error(request, "Erro ao apagar o anúncio. Contate nosso suporte que resolveremos para você.") 

  location = BASE_PATH + 'anuncio/'
  return HttpResponseRedirect(location)