#!/usr/bin/python
#-*- coding: utf-8 -*-

from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_protect

from mysite.models import Anuncio, Ensaio, Pagamento
from mysite.views import *
from mysite.views.anuncio import *


@csrf_protect
def anuncioShow(request, anuncioId):
  
  anuncio = Anuncio.objects.get(id=anuncioId)
  ensaios = list (Ensaio.objects.filter(anuncio = anuncio))
  caracteristicas = list(anuncio.caracteristicas.all())
  idiomas = list(anuncio.idiomas.all())
  informacoes = list(anuncio.informacoes.all())
  formasPagamento = list(anuncio.formasPagamento.all())
  atende = list(anuncio.atende.all())

  usuario = request.user

  if usuario.is_superuser:
    usuario = anuncio.anunciante.user

  if ((anuncio.anunciante.user == usuario) and ('anunciante' in request.session) and (Pagamento.objects.filter(anuncio=anuncio, mercado_pago_payment_id = '').exists())): 
    pagamento_pendente = Pagamento.objects.get(anuncio=anuncio, mercado_pago_payment_id = '')
  else:
    pagamento_pendente = False

  return render(request, 'anuncio/show.html', 
    {'anuncio': anuncio,
    'caracteristicas': caracteristicas,
    'informacoes': informacoes,
    'formasPagamento':formasPagamento,
    'idiomas':idiomas,
    'atende':atende,
    'ensaios': ensaios,
    'pagamento_pendente':pagamento_pendente})