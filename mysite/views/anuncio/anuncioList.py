#!/usr/bin/python
#-*- coding: utf-8 -*-

#from django.contrib.auth.decorators import login_required
from random import shuffle
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_protect


from mysite.models import Anuncio, Categoria
from mysite.views import *
from mysite.views.anuncio import *
from mysite.views.index import *
from datetime import datetime

#@login_required
@csrf_protect
def anuncioList(request, categoriaId=None):
  
  today =  datetime.now()
  anuncios_destaques = []
  anuncios_basicos = []
  if 'cidade' in request.session:
    estado = request.session['estado']
    cidade = request.session['cidade']
    if categoriaId != None:
      categoria = Categoria.objects.get(id=categoriaId)
      anuncios = list(Anuncio.objects.filter(estado = estado, cidade = cidade, categoria=categoria, anunciante__aprovado = True, data_exp__gte = today, pausado = False))
    else:
      anuncios = list(Anuncio.objects.filter(estado = estado, cidade = cidade, anunciante__aprovado = True, data_exp__gte = today, pausado = False ))
    for anuncio in anuncios:
      if anuncio.plano.tipo == 1:
        anuncios_basicos.append(anuncio)
      else:
        anuncios_destaques.append(anuncio)
    shuffle(anuncios_basicos)
    shuffle(anuncios_destaques)

  else:
    return redirect("index")

  return render(request, 'anuncio/list.html',
    {'anuncios_basicos': anuncios_basicos,
    'anuncios_destaques': anuncios_destaques})