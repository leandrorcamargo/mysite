#!/usr/bin/python
#-*- coding: utf-8 -*-
from mysite.views.anuncio.anuncioList import *
from mysite.views.anuncio.anuncioShow import *
from mysite.views.anuncio.anuncioNew import *
from mysite.views.anuncio.anuncioDelete import *
from mysite.views.anuncio.anuncioPause import *
from mysite.views.anuncio.anuncioEdit import *
from mysite.views.anuncio.anuncioRenew import *