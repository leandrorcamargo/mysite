#!/usr/bin/python
#-*- coding: utf-8 -*-

from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.views.decorators.csrf import csrf_protect
from django.http import JsonResponse, HttpResponseRedirect
from mysite.settings import BASE_PATH

from mysite.models import Anunciante
from mysite.views import *
from mysite.views.anunciante import *

@login_required
@csrf_protect
def anuncianteDelete(request, anuncianteId):

  anunciante = Anunciante.objects.get(id=anuncianteId)
  user = anunciante.user
  nome =anunciante.nome
  
  try:
    user.delete()
    messages.success(request, 'Anunciante %s apagada/apagado com sucesso.' % (nome))
  except Exception as e:
    messages.error(request, 'Erro ao apagar anunciante.') 

  location = BASE_PATH + 'anuncio/'
  return HttpResponseRedirect(location)