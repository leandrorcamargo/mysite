#!/usr/bin/python
#-*- coding: utf-8 -*-

from django.contrib import messages
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_protect
from django.http import JsonResponse, HttpResponseRedirect
from mysite.settings import BASE_PATH

from mysite.forms import AnuncianteForm
from mysite.views.user import *

@csrf_protect
def anuncianteNew(request):
  

  form = AnuncianteForm(request.POST or None, request.FILES or None)
  if form.is_valid():    
    anunciante = form.save(request.build_absolute_uri(None))
    messages.success(request, "Cadastro de anunciante criado com sucesso.")
    location = BASE_PATH + 'anuncio/'
    return HttpResponseRedirect(location)

  return render(request, 'anunciante/form.html', 
    {'form': form,
     'button':"Criar anunciante"})