#!/usr/bin/python
#-*- coding: utf-8 -*-

from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_protect

from mysite.forms import AlunoForm
from mysite.models import Aluno
from mysite.views.aluno import alunoList
from mysite.views.aluno import *
from mysite.views.cobranca import cobrancaNew

@login_required
@csrf_protect
def renovaMatriculaEdit(request, alunoId):
  
  
  model_name = Aluno._meta.verbose_name
  aluno = Aluno.objects.get(id=alunoId) 
  

  form = NovaMatriculaForm(instance=aluno, data=request.POST or None)
  
  if form.is_valid():    
    aluno = form.save()
    inicio = aluno.inicio
    dias = 30 * aluno.plano.periodo
    fim = aluno.inicio + timedelta(days = dias)
    aluno.fim = fim
    aluno.save()
    messages.success(request, 'Matrícula da/do aluna/aluno %s renovada com sucesso.' % (aluno.nome))
    cobrancaNew(request, aluno.vencimento, aluno.plano, aluno.parcelas, aluno.id)
    return redirect(alunoShow, alunoId=alunoId)

  return render(request, 'aluno/form.html', 
    {'title': ': <b>%s</b>' % (aluno.nome),
     'form': form,
     'button': 'Renovar Matrícula'})