#!/usr/bin/python
#-*- coding: utf-8 -*-

#from django.contrib.auth.decorators import login_required
from django.forms.models import fields_for_model
from django.shortcuts import render
from django.views.decorators.csrf import csrf_protect
from django.contrib.auth.decorators import login_required, user_passes_test


from mysite.models import Anunciante
from mysite.views import *
from mysite.views.anunciante import *

@login_required
@csrf_protect
@user_passes_test(lambda u: u.is_superuser)
def anuncianteList(request):

  usuario = request.user
  # if usuario.is_superuser:
  anunciantes = Anunciante.objects.all()

  return render(request, 'anunciante/list.html',
    {'tableHead': fields_for_model(Anunciante, fields=('nome', 'idade', 'estado', 'cidade', 'telefone', 'email', 'aprovado')),
     'anunciantes': anunciantes})
