#!/usr/bin/python
#-*- coding: utf-8 -*-

from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_protect

from mysite.models import Anunciante
from mysite.views import *
from mysite.views.anunciante import *

@login_required
@csrf_protect
def anuncianteShow(request, anuncianteId):
  
  usuario = request.user
  if usuario.is_superuser:
    anunciante = Anunciante.objects.get(id=anuncianteId)
  else:
    anunciante = Anunciante.objects.get(id=request.session['anunciante'])

  return render(request, 'anunciante/show.html', 
    {'anunciante': anunciante})