#!/usr/bin/python
#-*- coding: utf-8 -*-

#from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.shortcuts import render
from django.views.decorators.csrf import csrf_protect
from django.http import JsonResponse, HttpResponseRedirect
from mysite.settings import BASE_PATH

from mysite.forms import AnuncianteEditForm
from mysite.models import Anunciante
from mysite.views.anunciante import *


@login_required
@csrf_protect
def anuncianteEdit(request, anuncianteId):
  
  usuario = request.user
  model_name = Anunciante._meta.verbose_name
  if usuario.is_superuser:
    anunciante = Anunciante.objects.get(id=anuncianteId)
  else:
    anunciante = Anunciante.objects.get(id=request.session['anunciante'])
  

  form = AnuncianteEditForm(instance=anunciante, usuario = usuario, data=request.POST or None, files=request.FILES or None)
  
  if form.is_valid():
    if form.cleaned_data.get('segurando_rg') != anunciante.segurando_rg:
      try:
        anunciante.segurando_rg.delete(save=False)
      except:
        pass
    if form.cleaned_data.get('rg') != anunciante.rg:
      try:
        anunciante.rg.delete(save=False)
      except:
        pass
    anunciante = form.save(request.build_absolute_uri(None))
    messages.success(request, 'Cadastro atualizado com sucesso.')
    location = BASE_PATH + 'anunciante/' + str(anunciante.id)+"/"
    return HttpResponseRedirect(location)
  else:
    form = AnuncianteEditForm(instance=anunciante, usuario = usuario)

  return render(request, 'anunciante/form.html', 
    {'title': ': <b>%s</b>' % (anunciante.nome),
     'form': form,
     'button': 'Editar cadastro'})