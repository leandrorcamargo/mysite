#!/usr/bin/python
#-*- coding: utf-8 -*-

from django.shortcuts import render
from django.views.decorators.csrf import csrf_protect

@csrf_protect
def contatoShow(request):
  
  return render(request, 'contato/show.html')