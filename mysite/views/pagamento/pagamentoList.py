#!/usr/bin/python
#-*- coding: utf-8 -*-

from django.contrib.auth.decorators import login_required, user_passes_test
from django.forms.models import fields_for_model
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_protect


from mysite.models import Pagamento
from mysite.views import *
from mysite.views.pagamento import *

@login_required
@csrf_protect
@user_passes_test(lambda u: u.is_superuser)
def pagamentoList(request):
  """
  Display all regions 
  """

  pagamentos = Pagamento.objects.all()

  return render(request, 'pagamento/list.html',
    {'tableHead': fields_for_model(Pagamento, fields=('mercado_pago_payment_id', 'anunciante', 'anuncio', 'valor', 'mercado_pago_status')),
     'pagamentos': pagamentos})