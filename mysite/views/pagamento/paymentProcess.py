#!/usr/bin/python
#-*- coding: utf-8 -*-

from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_protect

from mysite.settings import BASE_PATH, MERCADO_PG_KEY

from mysite.models import Ensaio, Anuncio, Pagamento
from mysite.views.plano import *

import datetime

@login_required
@csrf_protect
def paymentProcess(request, preferenceId, anuncioId):
  """
  Display region edit screen
  if form is valid, saves and shows message

  :type  regionId: region identification number
  :param regionId: int
  """
  anuncio = Anuncio.objects.get(id=anuncioId)
  ensaios = list (Ensaio.objects.filter(anuncio = anuncio))
  caracteristicas = list(anuncio.caracteristicas.all())
  informacoes = list(anuncio.informacoes.all())
  formasPagamento = list(anuncio.formasPagamento.all())

  if (('anunciante' in request.session) and (Pagamento.objects.filter(anuncio=anuncio, mercado_pago_payment_id = '').exists())): 
    pagamento_pendente = Pagamento.objects.get(anuncio=anuncio, mercado_pago_payment_id = '')
  else:
    pagamento_pendente = False


  return render(request, 'anuncio/show2.html', 
    {'preference': preferenceId,
     'public_key': MERCADO_PG_KEY,
     'anuncio': anuncio,
     'caracteristicas': caracteristicas,
     'informacoes': informacoes,
     'formasPagamento':formasPagamento,
     'ensaios': ensaios,
     'pagamento_pendente':pagamento_pendente})