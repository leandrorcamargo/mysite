#!/usr/bin/python
#-*- coding: utf-8 -*-

from django.contrib.auth.decorators import login_required, user_passes_test
from django.shortcuts import render
from django.views.decorators.csrf import csrf_protect

from mysite.models import Pagamento
from mysite.views import *
from mysite.views.pagamento import *

@login_required
@csrf_protect
@user_passes_test(lambda u: u.is_superuser)
def pagamentoShow(request, pagamentoId):
  """
  Display Region'

  :type  regionId: region identification number
  :param regionId: int
  """

  pagamento = Pagamento.objects.get(id=pagamentoId)

  return render(request, 'pagamento/show.html', 
    {'pagamento': pagamento})