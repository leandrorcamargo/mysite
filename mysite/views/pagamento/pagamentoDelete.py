#!/usr/bin/python
#-*- coding: utf-8 -*-

from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib import messages
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_protect

from mysite.models import Pagamento
from mysite.views import *
from mysite.views.pagamento import *

@login_required
@csrf_protect
@user_passes_test(lambda u: u.is_superuser)
def pagamentoDelete(request, pagamentoId):
  """
  Display region edit screen
  if form is valid, saves and shows message

  :type  regionId: region identification number
  :param regionId: int
  """
  model_name = Pagamento._meta.verbose_name
  pagamento = Pagamento.objects.get(id=pagamentoId)
  
  try:
    pagamento.delete()
    messages.success(request, 'Pagamento apagado com sucesso.')
  except Exception as e:
    messages.error(request, 'Erro ao apagar pagamento.') 

  return redirect(pagamentoList) 