#!/usr/bin/python
#-*- coding: utf-8 -*-

from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_POST
from django.utils import timezone
from django.http import HttpResponse

import datetime

from mysite.settings import MERCADO_PG_TKN

from mysite.models import  Pagamento, Anuncio, AnuncioD
from mysite.views.plano import *

import mercadopago

@csrf_exempt
@require_POST
def paymentWebhook(request):
  """
  Display region creation screen
  """

  mp = mercadopago.SDK(MERCADO_PG_TKN)

  topic = request.GET.get('topic')
  id = request.GET.get('id')
  
  if topic == 'payment':
    pagamento = Pagamento.objects.get(mercado_pago_payment_id=id)
    payment_mp = mp.payment().get(id)

    pagamento.mercado_pago_status = payment_mp['response']['status']

    pagamento.save()
    
    if pagamento.mercado_pago_status == 'approved':
      if pagamento.anuncio is not None:
        anuncio = Anuncio.objects.get(id=pagamento.anuncio.id)
      else:
        anuncio = AnuncioD.objects.get(id=pagamento.anuncioD.id)
      anuncio.data_ini = timezone.now() - datetime.timedelta(hours=3)
      if anuncio.data_exp > anuncio.data_ini:
        data_final = anuncio.data_exp + datetime.timedelta(days=anuncio.plano.duracao)
      else:
        data_final = anuncio.data_ini + datetime.timedelta(days=anuncio.plano.duracao)
      anuncio.data_exp = data_final
      anuncio.save()

  elif topic == 'merchant_orders':
    merchant_order = mp.merchant_order().get(id)
    pagamento = Pagamento.objects.get(mercado_pago_merchant_order_id=id)
    if pagamento.mercado_pago_status == "approved":
      return HttpResponse(status=200)
    else:
      pagamentos = merchant_order['payments']
      if len(pagamentos) >1:
        for pagamento_listed in pagamentos:
          if pagamento_listed.status == "approved":
            if ((str(pagamento_listed.id) != pagamento.mercado_pago_payment_id) and (pagamento.mercado_pago_status == "rejected")):
              pagamento.mercado_pago_payment_id = pagamento_listed.id
              pagamento.mercado_pago_status = pagamento_listed.status
              if pagamento.anuncio is not None:
                anuncio = Anuncio.objects.get(id=pagamento.anuncio.id)
              else:
                anuncio = AnuncioD.objects.get(id=pagamento.anuncioD.id)
              anuncio = Anuncio.objects.get(id=pagamento.anuncio.id)
              anuncio.data_ini = timezone.now() - datetime.timedelta(hours=3)
              if anuncio.data_exp > anuncio.data_ini:
                data_final = anuncio.data_exp + datetime.timedelta(days=anuncio.plano.duracao)
              else:
                data_final = anuncio.data_ini + datetime.timedelta(days=anuncio.plano.duracao)
              anuncio.data_exp = data_final
              anuncio.save()

      pagamento.save()

  return HttpResponse(status=200)