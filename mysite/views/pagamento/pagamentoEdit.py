#!/usr/bin/python
#-*- coding: utf-8 -*-

from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib import messages
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_protect

from mysite.forms import PagamentoForm
from mysite.models import Pagamento
from mysite.views.pagamento import *

@login_required
@csrf_protect
@user_passes_test(lambda u: u.is_superuser)
def pagamentoEdit(request, pagamentoId):
  """
  Display region edit screen
  if form is valid, saves and shows message

  :type  regionId: region identification number
  :param regionId: int
  """

  model_name = Pagamento._meta.verbose_name
  pagamento = Pagamento.objects.get(id=pagamentoId) 
  

  form = PagamentoForm(instance=pagamento, data=request.POST or None)
  
  if form.is_valid():    
    pagamento = form.save()
    messages.success(request, 'Cadastro do pagamento %s atualizado com sucesso.' % (pagamento.mercado_pago_payment_id))
    return redirect(pagamentoShow, pagamentoId=pagamentoId)

  return render(request, 'pagamento/form.html', 
    {'title': ': <b>%s</b>' % (pagamento.mercado_pago_payment_id),
     'form': form,
     'button': 'Editar Pagamento'})