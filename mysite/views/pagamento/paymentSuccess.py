#!/usr/bin/python
#-*- coding: utf-8 -*-

from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_protect
from django.utils import timezone
from django.http import JsonResponse, HttpResponseRedirect

from mysite.settings import BASE_PATH

from mysite.forms import PagamentoForm
from mysite.models import Plano, Pagamento, Anuncio, AnuncioD
from mysite.views.plano import *

import datetime

@login_required
@csrf_protect
def paymentSuccess(request, pagamentoId):
  """
  Display region edit screen
  if form is valid, saves and shows message

  :type  regionId: region identification number
  :param regionId: int
  """

  model_name = Pagamento._meta.verbose_name
  pagamento = Pagamento.objects.get(id=pagamentoId)

  payment_id = request.GET.get('payment_id')
  status = request.GET.get('status')
  merchant_order_id = request.GET.get('merchant_order_id')

  pagamento.mercado_pago_payment_id = payment_id
  pagamento.mercado_pago_status = status
  pagamento.mercado_pago_merchant_order_id = merchant_order_id

  pagamento.save()
  if pagamento.anuncio is not None:
    anuncio = Anuncio.objects.get(id=pagamento.anuncio.id)
  else:
    anuncio = AnuncioD.objects.get(id=pagamento.anuncioD.id)
  anuncio.data_ini = timezone.now() - datetime.timedelta(hours=3)
  if anuncio.data_exp > anuncio.data_ini:
    data_final = anuncio.data_exp + datetime.timedelta(days=anuncio.plano.duracao)
  else:
    data_final = anuncio.data_ini + datetime.timedelta(days=anuncio.plano.duracao)
  print(data_final)
  anuncio.data_exp = data_final
  anuncio.save()

  messages.success(request, 'Plano pago com sucesso.')
  location = BASE_PATH + 'anuncio/'
  return HttpResponseRedirect(location)