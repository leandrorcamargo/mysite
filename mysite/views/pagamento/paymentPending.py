#!/usr/bin/python
#-*- coding: utf-8 -*-

from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_protect
from django.utils import timezone
from django.http import JsonResponse, HttpResponseRedirect

from mysite.settings import BASE_PATH

from mysite.forms import PagamentoForm
from mysite.models import Plano, Pagamento, Anuncio
from mysite.views.plano import *

import datetime

@login_required
@csrf_protect
def paymentPending(request, pagamentoId):
  """
  Display region edit screen
  if form is valid, saves and shows message

  :type  regionId: region identification number
  :param regionId: int
  """

  model_name = Pagamento._meta.verbose_name
  pagamento = Pagamento.objects.get(id=pagamentoId)

  payment_id = request.GET.get('payment_id')
  status = request.GET.get('status')
  merchant_order_id = request.GET.get('merchant_order_id')

  pagamento.mercado_pago_payment_id = payment_id
  pagamento.mercado_pago_status = status
  pagamento.mercado_pago_merchant_order_id = merchant_order_id
  
  pagamento.save()
  messages.success(request, 'Seu pagamento com o Mercado Pago está pendente. Assim que for aprovado seu anúncio será ativado.')
  location = BASE_PATH + 'anuncio/'
  return HttpResponseRedirect(location)
