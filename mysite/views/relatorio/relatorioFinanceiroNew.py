#!/usr/bin/python
#-*- coding: utf-8 -*-

from django.contrib.auth.decorators import login_required
from django.forms.models import fields_for_model
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_protect

from mysite.forms import RelatorioFinanceiroForm
from mysite.models import Aluno, Cobranca, Despesa, Pagamento, Recebimento
from mysite.views import *
from mysite.views.aluno import *

from datetime import datetime

@login_required
@csrf_protect
def relatorioFinanceiroNew(request):
  """
  Display all regions 
  """
  today = datetime.today()
  form = RelatorioFinanceiroForm(request.POST or None)
  if form.is_valid():    
    data = form.cleaned_data
    data_inicio = data['data_inicio']
    data_fim = data['data_fim']
    matriculas_total = 0
    alunos_matr = Aluno.objects.filter(vencimento__gte=data_inicio, vencimento__lte=data_fim, matricula = True)
    for aluno in alunos_matr:
      matriculas_total += aluno.plano.matricula
    cobrancas = Cobranca.objects.filter(data__gte=data_inicio, data__lte=data_fim)
    despesas = Despesa.objects.filter(data__gte=data_inicio, data__lte=data_fim)
    pagamentos = Pagamento.objects.filter(data__gte=data_inicio, data__lte=data_fim)
    recebimentos = Recebimento.objects.filter(data__gte=data_inicio, data__lte=data_fim)
    cobrancas_total = 0
    despesas_total = 0
    pagamentos_total = 0
    recebimentos_total = 0
    cobrancas_atraso = 0
    cobrancas_perdidas = 0
    lucro_prejuizo = 0
    cobrancas_perd = list()
    cobrancas_atrasadas = list()
    for cobranca in cobrancas:
      if cobranca.perdida == False:
        cobrancas_total += cobranca.valor      
        if cobranca.pago == False and cobranca.data <= data_fim:
          cobrancas_atrasadas.append(cobranca)
      else:
        cobrancas_perdidas += cobranca.valor
        cobrancas_perd.append(cobranca)


    for despesa in despesas:
      despesas_total += despesa.valor

    for pagamento in pagamentos:
      pagamentos_total += pagamento.valor

    for recebimento in recebimentos:
      recebimentos_total += recebimento.valor

    cobrancas_atraso = cobrancas_total - recebimentos_total
    lucro_prejuizo = recebimentos_total - despesas_total - pagamentos_total + matriculas_total

    return render(request, 'relatorio/financeiro_show.html', 
    {'data_inicio': data_inicio,
    'data_fim': data_fim,
    'recebimentos_total': recebimentos_total,
    'cobrancas_total': cobrancas_total,
    'despesas_total': despesas_total,
    'pagamentos_total': pagamentos_total,
    'cobrancas_atraso': cobrancas_atraso,
    'lucro_prejuizo': lucro_prejuizo,
    'cobrancas_perd': cobrancas_perd,
    'cobrancas_atrasadas': cobrancas_atrasadas,
    'cobrancas_perdidas': cobrancas_perdidas,
    'recebimentos': recebimentos,
    'pagamentos': pagamentos,
    'despesas': despesas,
    'alunos_matr': alunos_matr,
    'matriculas_total': matriculas_total})

  return render(request, 'relatorio/form.html', 
    {'form': form,
     'button': 'Pesquisar'})