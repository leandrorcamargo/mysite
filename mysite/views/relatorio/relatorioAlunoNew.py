#!/usr/bin/python
#-*- coding: utf-8 -*-

from django.contrib.auth.decorators import login_required
from django.forms.models import fields_for_model
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_protect

from mysite.forms import RelatorioAlunoForm
from mysite.models import Aluno, Cobranca
from mysite.views import *
from mysite.views.aluno import *

from datetime import datetime

@login_required
@csrf_protect
def relatorioAlunoNew(request):
  """
  Display all regions 
  """
  form = RelatorioAlunoForm(request.POST or None)
  today = datetime.today()
  if form.is_valid():    
    data = form.cleaned_data
    opcao = data['opcao']
    if opcao == 'opcao1':
      alunos = Aluno.objects.filter(fim__year=today.year, fim__month=today.month)
      return render(request, 'aluno/list.html',
    {'tableHead': fields_for_model(Aluno, fields=('nome', 'plano', 'fim', 'telefone', 'email')),
     'alunos': alunos})
    elif opcao == 'opcao2':
      alunos = Aluno.objects.filter(fim__lte = today)
      return render(request, 'aluno/list.html',
    {'tableHead': fields_for_model(Aluno, fields=('nome', 'plano', 'fim', 'telefone', 'email')),
     'alunos': alunos})
    elif opcao == 'opcao3':
      cobrancas = Cobranca.objects.filter(pago = False, data__lte = today)
      alunos = []
      for cobranca in cobrancas:
        aluno = Aluno.objects.get(id=cobranca.aluno.id)
        if aluno not in alunos:
          alunos.append(aluno)
      return render(request, 'aluno/list.html',
    {'tableHead': fields_for_model(Aluno, fields=('nome', 'plano', 'fim', 'telefone', 'email')),
     'alunos': alunos}) 
    elif opcao == 'opcao4':
      modalidade = data['modalidade']
      alunos = Aluno.objects.filter(interesses__in=[modalidade]) 
      return render(request, 'aluno/list.html',
    {'tableHead': fields_for_model(Aluno, fields=('nome', 'plano', 'fim', 'telefone', 'email')),
     'alunos': alunos})  

  return render(request, 'relatorio/form_aluno.html', 
    {'form': form,
     'button': 'Pesquisar'})