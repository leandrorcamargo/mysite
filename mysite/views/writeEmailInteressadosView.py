#!/usr/bin/python
#-*- coding: utf-8 -*-

from django.contrib.auth.decorators import login_required
from django.template.loader import render_to_string
from django.conf import settings
from django.contrib import messages
from django.core.mail import EmailMultiAlternatives
from email.mime.image import MIMEImage
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_protect

from mysite.models import Modalidade, Aluno
from mysite.views import *
from mysite.forms import *

@login_required
@csrf_protect
def writeEmailInteressadosView(request):
    template = 'write_email.html'
    if request.POST:
        mail_data = request.POST.copy()
        mail_data.update(request.FILES)
        form = WriteEmailForm(request.POST)
        if form.is_valid():
            from_email = request.user.email
            subject = form.cleaned_data['subject']
            modalidades = form.cleaned_data['para']
            para = []
            for modalidade in modalidades:
                modalidade_Obj = Modalidade.objects.get(nome = modalidade)
                alunos = list(Aluno.objects.filter(interesses__id__exact = modalidade_Obj.id))
                for aluno in alunos:
                    para.append (aluno.email )
            body = form.cleaned_data['body']
            imagem = (request.FILES.get('imagem' or None))
            msg = EmailMultiAlternatives(subject, body, from_email, [], bcc = para)
            if (imagem != None):
                img_bytes = imagem.read()
                imagem_send = MIMEImage(img_bytes, 'jpeg')
                imagem_send.add_header('Content-ID', '<image1>')
                html_content = body + '<br> <br> <img src="cid:image1">'
                msg = EmailMultiAlternatives(subject, body , from_email, [], bcc = para)
                msg.attach_alternative(html_content, "text/html")                             
                msg.attach(imagem_send)
            msg.send()
            messages.success(request, ('Mensagem enviada com sucesso.'))
            return render(request, template, { 'form': form })
        else:
            return render(request, template, { 'form': form })
    else:
        form = WriteEmailForm()
        return render(request, template, { 'form': form })