#!/usr/bin/python
#-*- coding: utf-8 -*-

from django.contrib.auth.decorators import login_required, user_passes_test
from django.forms.models import fields_for_model
from django.shortcuts import render
from django.views.decorators.csrf import csrf_protect


from mysite.models import Olhos
from mysite.views import *
from mysite.views.olhos import *

@login_required
@csrf_protect
@user_passes_test(lambda u: u.is_superuser)
def olhosList(request):
  """
  Display all regions 
  """

  olhos_ = Olhos.objects.all()

  return render(request, 'olhos/list.html',
    {'tableHead': fields_for_model(Olhos, fields=('nome', 'id')),
     'olhos_': olhos_})