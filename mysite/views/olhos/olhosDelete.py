#!/usr/bin/python
#-*- coding: utf-8 -*-

from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib import messages
from django.views.decorators.csrf import csrf_protect
from django.http import HttpResponseRedirect
from mysite.settings import BASE_PATH

from mysite.models import Olhos
from mysite.views import *
from mysite.views.categoria import *

@login_required
@csrf_protect
@user_passes_test(lambda u: u.is_superuser)
def olhosDelete(request, olhosId):
  """
  Display region edit screen
  if form is valid, saves and shows message

  :type  regionId: region identification number
  :param regionId: int
  """
  model_name = Olhos._meta.verbose_name
  olhos = Olhos.objects.get(id=olhosId)
  nome = olhos.nome
  
  try:
    olhos.delete()
    messages.success(request, 'Olhos %s apagada com sucesso.' % (nome))
  except Exception as e:
    messages.error(request, 'Erro ao apagar olhos.') 

  location = BASE_PATH + 'olhos/'
  return HttpResponseRedirect(location)