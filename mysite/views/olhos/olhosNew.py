#!/usr/bin/python
#-*- coding: utf-8 -*-

from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib import messages
from django.shortcuts import render
from django.views.decorators.csrf import csrf_protect
from django.http import HttpResponseRedirect
from mysite.settings import BASE_PATH

from mysite.forms import OlhosForm
from mysite.models import Olhos
from mysite.views.olhos import *

@login_required
@csrf_protect
@user_passes_test(lambda u: u.is_superuser)
def olhosNew(request):
  """
  Display region creation screen
  """
  form = OlhosForm(request.POST or None)
  if form.is_valid():    
    olhos = form.save()
    messages.success(request, ('Olhos %s criado com sucesso.')% (olhos.nome))
    location = BASE_PATH + 'olhos/'
    return HttpResponseRedirect(location)

  return render(request, 'olhos/form.html', 
    {'form': form,
     'button': 'Criar olhos'})