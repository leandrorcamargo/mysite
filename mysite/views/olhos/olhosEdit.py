#!/usr/bin/python
#-*- coding: utf-8 -*-

from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib import messages
from django.shortcuts import render
from django.views.decorators.csrf import csrf_protect
from django.http import HttpResponseRedirect
from mysite.settings import BASE_PATH

from mysite.forms import OlhosForm
from mysite.models import Olhos
from mysite.views.olhos import *

@login_required
@csrf_protect
@user_passes_test(lambda u: u.is_superuser)
def olhosEdit(request, olhosId):
  """
  Display region edit screen
  if form is valid, saves and shows message

  :type  regionId: region identification number
  :param regionId: int
  """

  model_name = Olhos._meta.verbose_name
  olhos = Olhos.objects.get(id=olhosId) 
  

  form = OlhosForm(instance=olhos, data=request.POST or None)
  
  if form.is_valid():    
    olhos = form.save()
    messages.success(request, 'Cadastro de olhos %s atualizado com sucesso.' % (olhos.nome))
    location = BASE_PATH + 'olhos/'
    return HttpResponseRedirect(location)

  return render(request, 'olhos/form.html', 
    {'title': ': <b>%s</b>' % (olhos.nome),
     'form': form,
     'button': 'Editar Olhos'})