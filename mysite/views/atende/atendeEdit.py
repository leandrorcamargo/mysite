#!/usr/bin/python
#-*- coding: utf-8 -*-

from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib import messages
from django.shortcuts import render
from django.views.decorators.csrf import csrf_protect
from django.http import HttpResponseRedirect
from mysite.settings import BASE_PATH

from mysite.forms import AtendeForm
from mysite.models import Atende
from mysite.views.atende import *

@login_required
@csrf_protect
@user_passes_test(lambda u: u.is_superuser)
def atendeEdit(request, atendeId):
  """
  Display region edit screen
  if form is valid, saves and shows message

  :type  regionId: region identification number
  :param regionId: int
  """

  model_name = Atende._meta.verbose_name
  atende = Atende.objects.get(id=atendeId) 
  

  form = AtendeForm(instance=atende, data=request.POST or None)
  
  if form.is_valid():    
    atende = form.save()
    messages.success(request, 'Cadastro do atende %s atualizado com sucesso.' % (atende.nome))
    location = BASE_PATH + 'atende/'
    return HttpResponseRedirect(location)

  return render(request, 'atende/form.html', 
    {'title': ': <b>%s</b>' % (atende.nome),
     'form': form,
     'button': 'Editar atende'})