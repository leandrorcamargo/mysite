#!/usr/bin/python
#-*- coding: utf-8 -*-

from django.contrib.auth.decorators import login_required, user_passes_test
from django.forms.models import fields_for_model
from django.shortcuts import render
from django.views.decorators.csrf import csrf_protect


from mysite.models import Atende
from mysite.views import *
from mysite.views.atende import *

@login_required
@csrf_protect
@user_passes_test(lambda u: u.is_superuser)
def atendeList(request):
  """
  Display all regions 
  """

  atendes = Atende.objects.all()

  return render(request, 'atende/list.html',
    {'tableHead': fields_for_model(Atende, fields=('nome', 'id')),
     'atendes': atendes})