#!/usr/bin/python
#-*- coding: utf-8 -*-

from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib import messages
from django.views.decorators.csrf import csrf_protect
from django.http import HttpResponseRedirect
from mysite.settings import BASE_PATH

from mysite.models import Atende
from mysite.views import *
from mysite.views.atende import *

@login_required
@csrf_protect
@user_passes_test(lambda u: u.is_superuser)
def atendeDelete(request, atendeId):
  """
  Display region edit screen
  if form is valid, saves and shows message

  :type  regionId: region identification number
  :param regionId: int
  """
  model_name = Atende._meta.verbose_name
  atende = Atende.objects.get(id=atendeId)
  nome = atende.nome
  
  try:
    atende.delete()
    messages.success(request, 'Atende %s apagado com sucesso.' % (nome))
  except Exception as e:
    messages.error(request, 'Erro ao apagar atende.') 

  location = BASE_PATH + 'atende/'
  return HttpResponseRedirect(location)