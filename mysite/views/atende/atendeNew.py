#!/usr/bin/python
#-*- coding: utf-8 -*-

from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib import messages
from django.shortcuts import render
from django.views.decorators.csrf import csrf_protect
from django.http import HttpResponseRedirect
from mysite.settings import BASE_PATH

from mysite.forms import AtendeForm
from mysite.views.atende import *

@login_required
@csrf_protect
@user_passes_test(lambda u: u.is_superuser)
def atendeNew(request):
  """
  Display region creation screen
  """
  form = AtendeForm(request.POST or None)
  if form.is_valid():    
    atende = form.save()
    messages.success(request, ('Atende %s criado com sucesso.')% (atende.nome))
    location = BASE_PATH + 'atende/'
    return HttpResponseRedirect(location)

  return render(request, 'atende/form.html', 
    {'form': form,
     'button': 'Criar atende'})