#!/usr/bin/python
#-*- coding: utf-8 -*-

from django.contrib.auth.decorators import login_required, user_passes_test
from django.shortcuts import render
from django.views.decorators.csrf import csrf_protect

from mysite.models import Atende
from mysite.views import *
from mysite.views.atende import *

@login_required
@csrf_protect
@user_passes_test(lambda u: u.is_superuser)
def atendeShow(request, atendeId):
  """
  Display Region'

  :type  regionId: region identification number
  :param regionId: int
  """

  atende = Atende.objects.get(id=atendeId)

  return render(request, 'seios/show.html', 
    {'atende': atende})