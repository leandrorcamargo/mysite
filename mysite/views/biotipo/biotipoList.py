#!/usr/bin/python
#-*- coding: utf-8 -*-

from django.contrib.auth.decorators import login_required, user_passes_test
from django.forms.models import fields_for_model
from django.shortcuts import render
from django.views.decorators.csrf import csrf_protect


from mysite.models import Biotipo
from mysite.views import *
from mysite.views.biotipo import *

@login_required
@csrf_protect
@user_passes_test(lambda u: u.is_superuser)
def biotipoList(request):
  """
  Display all regions 
  """

  biotipos = Biotipo.objects.all()

  return render(request, 'biotipo/list.html',
    {'tableHead': fields_for_model(Biotipo, fields=('nome', 'id')),
     'biotipos': biotipos})