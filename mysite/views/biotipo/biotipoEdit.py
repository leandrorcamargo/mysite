#!/usr/bin/python
#-*- coding: utf-8 -*-

from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib import messages
from django.shortcuts import render
from django.views.decorators.csrf import csrf_protect
from django.http import HttpResponseRedirect
from mysite.settings import BASE_PATH

from mysite.forms import BiotipoForm
from mysite.models import Biotipo
from mysite.views.biotipo import *

@login_required
@csrf_protect
@user_passes_test(lambda u: u.is_superuser)
def biotipoEdit(request, biotipoId):
  """
  Display region edit screen
  if form is valid, saves and shows message

  :type  regionId: region identification number
  :param regionId: int
  """

  model_name = Biotipo._meta.verbose_name
  biotipo = Biotipo.objects.get(id=biotipoId) 
  

  form = BiotipoForm(instance=biotipo, data=request.POST or None)
  
  if form.is_valid():    
    biotipo = form.save()
    messages.success(request, 'Cadastro do biotipo %s atualizado com sucesso.' % (biotipo.nome))
    location = BASE_PATH + 'biotipo/'
    return HttpResponseRedirect(location)

  return render(request, 'biotipo/form.html', 
    {'title': ': <b>%s</b>' % (biotipo.nome),
     'form': form,
     'button': 'Editar biotipo'})