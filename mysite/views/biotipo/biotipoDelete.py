#!/usr/bin/python
#-*- coding: utf-8 -*-

from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib import messages
from django.views.decorators.csrf import csrf_protect
from django.http import JsonResponse
from mysite.settings import BASE_PATH

from mysite.models import Biotipo
from mysite.views import *
from mysite.views.biotipo import *

@login_required
@csrf_protect
@user_passes_test(lambda u: u.is_superuser)
def biotipoDelete(request, biotipoId):
  """
  Display region edit screen
  if form is valid, saves and shows message

  :type  regionId: region identification number
  :param regionId: int
  """
  model_name = Biotipo._meta.verbose_name
  biotipo = Biotipo.objects.get(id=biotipoId)
  nome = biotipo.nome
  
  try:
    biotipo.delete()
    messages.success(request, 'Biotipo %s apagado com sucesso.' % (nome))
  except Exception as e:
    messages.error(request, 'Erro ao apagar biotipo.') 

  location = BASE_PATH + 'biotipo/'
  return HttpResponseRedirect(location)