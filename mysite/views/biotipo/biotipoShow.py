#!/usr/bin/python
#-*- coding: utf-8 -*-

from django.contrib.auth.decorators import login_required, user_passes_test
from django.shortcuts import render
from django.views.decorators.csrf import csrf_protect

from mysite.models import Biotipo
from mysite.views import *
from mysite.views.biotipo import *

@login_required
@csrf_protect
@user_passes_test(lambda u: u.is_superuser)
def biotipoShow(request, biotipoId):
  """
  Display Region'

  :type  regionId: region identification number
  :param regionId: int
  """

  biotipo = Biotipo.objects.get(id=biotipoId)

  return render(request, 'seios/show.html', 
    {'biotipo': biotipo})