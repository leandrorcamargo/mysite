#!/usr/bin/python
#-*- coding: utf-8 -*-

from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib import messages
from django.shortcuts import render
from django.views.decorators.csrf import csrf_protect
from django.http import HttpResponseRedirect
from mysite.settings import BASE_PATH

from mysite.forms import BiotipoForm
from mysite.views.biotipo import *

@login_required
@csrf_protect
@user_passes_test(lambda u: u.is_superuser)
def biotipoNew(request):
  """
  Display region creation screen
  """
  form = BiotipoForm(request.POST or None)
  if form.is_valid():    
    biotipo = form.save()
    messages.success(request, ('Biotipo %s criado com sucesso.')% (biotipo.nome))
    location = BASE_PATH + 'biotipo/'
    return HttpResponseRedirect(location)

  return render(request, 'biotipo/form.html', 
    {'form': form,
     'button': 'Criar biotipo'})