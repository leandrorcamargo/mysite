#!/usr/bin/python
#-*- coding: utf-8 -*-

from django.shortcuts import render, redirect

from mysite.forms import IndexForm
from mysite.views.anuncio import anuncioList
from mysite.views import *
from django.views.decorators.csrf import csrf_protect
from mysite.models import Estado, Cidade
from django.http import JsonResponse
import json
from django.core import serializers

from mysite import settings

def is_ajax(request):
  return request.META.get('HTTP_X_REQUESTED_WITH') == 'XMLHttpRequest'


@csrf_protect
def index(request):

  form = IndexForm(request.POST or None)
  if form.is_valid():
    estado = form.cleaned_data['estado']
    cidade = form.cleaned_data['cidade']
    request.session['estado'] = estado.id
    request.session['cidade'] = cidade.id 
    return redirect('anuncioList')
  if is_ajax(request):
    bla = request.GET.get("bla" or None)
    estado = Estado.objects.get(nome=bla)
    cidades= Cidade.objects.filter(estado=estado)
    estado_data = []
    estado_data.append({ 
      'id': estado.id,
    })
    return JsonResponse((estado_data), status = 200, safe=False)


  return render(request, 'novo/index.html', 
    {'form': form,
     'button': 'Entrar'})

