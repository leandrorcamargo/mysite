#!/usr/bin/python
#-*- coding: utf-8 -*-

from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib import messages
from django.shortcuts import render
from django.views.decorators.csrf import csrf_protect
from django.http import HttpResponseRedirect
from mysite.settings import BASE_PATH

from mysite.forms import CaracteristicaForm
from mysite.models import Caracteristica
from mysite.views.caracteristica import *

@login_required
@csrf_protect
@user_passes_test(lambda u: u.is_superuser)
def caracteristicaEdit(request, caracteristicaId):
  """
  Display region edit screen
  if form is valid, saves and shows message

  :type  regionId: region identification number
  :param regionId: int
  """

  model_name = Caracteristica._meta.verbose_name
  caracteristica = Caracteristica.objects.get(id=caracteristicaId) 
  

  form = CaracteristicaForm(instance=caracteristica, data=request.POST or None)
  
  if form.is_valid():    
    caracteristica = form.save()
    messages.success(request, 'Cadastro da caracteristica %s atualizado com sucesso.' % (caracteristica.nome))
    location = BASE_PATH + 'caracteristica/'
    return HttpResponseRedirect(location)

  return render(request, 'caracteristica/form.html', 
    {'title': ': <b>%s</b>' % (caracteristica.nome),
     'form': form,
     'button': 'Editar Caracteristica'})