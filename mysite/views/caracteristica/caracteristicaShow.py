#!/usr/bin/python
#-*- coding: utf-8 -*-

from django.contrib.auth.decorators import login_required, user_passes_test
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_protect

from mysite.models import Caracteristica
from mysite.views import *
from mysite.views.caracteristica import *

@login_required
@csrf_protect
@user_passes_test(lambda u: u.is_superuser)
def caracteristicaShow(request, caracteristicaId):
  """
  Display Region'

  :type  regionId: region identification number
  :param regionId: int
  """

  caracteristica = Caracteristica.objects.get(id=caracteristicaId)

  return render(request, 'caracteristica/show.html', 
    {'caracteristica': caracteristica})