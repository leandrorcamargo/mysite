#!/usr/bin/python
#-*- coding: utf-8 -*-

from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib import messages
from django.shortcuts import render
from django.views.decorators.csrf import csrf_protect
from django.http import HttpResponseRedirect
from mysite.settings import BASE_PATH

from mysite.forms import CaracteristicaForm
from mysite.views.caracteristica import *

@login_required
@csrf_protect
@user_passes_test(lambda u: u.is_superuser)
def caracteristicaNew(request):
  """
  Display region creation screen
  """
  form = CaracteristicaForm(request.POST or None)
  if form.is_valid():    
    caracteristica = form.save()
    messages.success(request, ('Caracteristica %s criada com sucesso.')% (caracteristica.nome))
    location = BASE_PATH + 'caracteristica/'
    return HttpResponseRedirect(location)

  return render(request, 'caracteristica/form.html', 
    {'form': form,
     'button': 'Criar caracteristica'})