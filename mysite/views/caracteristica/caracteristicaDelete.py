#!/usr/bin/python
#-*- coding: utf-8 -*-

from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib import messages
from django.views.decorators.csrf import csrf_protect
from django.http import HttpResponseRedirect
from mysite.settings import BASE_PATH

from mysite.models import Caracteristica
from mysite.views import *
from mysite.views.caracteristica import *

@login_required
@csrf_protect
@user_passes_test(lambda u: u.is_superuser)
def caracteristicaDelete(request, caracteristicaId):
  """
  Display region edit screen
  if form is valid, saves and shows message

  :type  regionId: region identification number
  :param regionId: int
  """
  model_name = Caracteristica._meta.verbose_name
  caracteristica = Caracteristica.objects.get(id=caracteristicaId)
  nome = caracteristica.nome
  
  try:
    caracteristica.delete()
    messages.success(request, 'Caracteristica %s apagada com sucesso.' % (nome))
  except Exception as e:
    messages.error(request, 'Erro ao apagar caracteristica.') 

  location = BASE_PATH + 'caracteristica/'
  return HttpResponseRedirect(location)