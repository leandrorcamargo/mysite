#!/usr/bin/python
#-*- coding: utf-8 -*-

from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect

from mysite.views.anuncio import anuncioList
from mysite.views import *
from django.views.decorators.csrf import csrf_protect
from mysite.models import Estado, Cidade
from django.http import JsonResponse
import json
from django.core import serializers

from mysite import settings


@csrf_protect
def alteraRegiao(request):

  form = IndexForm(request.POST or None)
  if form.is_valid():
    estado = form.cleaned_data['estado']
    cidade = form.cleaned_data['cidade']
    request.session['estado'] = estado.id
    request.session['cidade'] = cidade.id
    if 'anuncios_list' in request.session:
      del request.session['anuncios_list']
    if 'anunciosD_list' in request.session:
      del request.session['anunciosD_list']
    if 'categorias_list' in request.session:
      del request.session['categorias_list']

    return redirect('anuncioList')


  return render(request, 'novo/index.html', 
    {'form': form,
     'button': 'Entrar'})