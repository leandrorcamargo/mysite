#!/usr/bin/python
#-*- coding: utf-8 -*-

from django.contrib.auth.decorators import login_required, user_passes_test
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_protect

from mysite.models import Cidade
from mysite.views import *
from mysite.views.cidade import *

@login_required
@csrf_protect
def cidadeShow(request, cidadeId):
  """
  Display Region'

  :type  regionId: region identification number
  :param regionId: int
  """

  cidade = Cidade.objects.get(id=cidadeId)

  return render(request, 'cidade/show.html', 
    {'cidade': cidade})