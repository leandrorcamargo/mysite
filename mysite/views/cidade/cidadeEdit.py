#!/usr/bin/python
#-*- coding: utf-8 -*-

from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib import messages
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_protect

from mysite.forms import CidadeForm
from mysite.models import Cidade
from mysite.views.cidade import cidadeList, cidadeShow
from mysite.views.cidade import *

@login_required
@csrf_protect
def cidadeEdit(request, cidadeId):
  """
  Display region edit screen
  if form is valid, saves and shows message

  :type  regionId: region identification number
  :param regionId: int
  """

  model_name = Cidade._meta.verbose_name
  cidade = Cidade.objects.get(id=cidadeId) 
  

  form = CidadeForm(instance=cidade, data=request.POST or None)
  
  if form.is_valid():    
    cidade = form.save()
    messages.success(request, 'Cidade %s atualizada com sucesso.' % (cidade.nome))
    return redirect(cidadeShow, cidadeId=cidadeId)

  return render(request, 'cidade/form.html', 
    {'title': ': <b>%s</b>' % (cidade.nome),
     'form': form,
     'button': 'Editar Cidade'})