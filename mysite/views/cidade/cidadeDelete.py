#!/usr/bin/python
#-*- coding: utf-8 -*-

from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib import messages
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_protect

from mysite.models import Cidade
from mysite.views import *
from mysite.views.cidade import *

@login_required
@csrf_protect
def cidadeDelete(request, cidadeId):
  """
  Display region edit screen
  if form is valid, saves and shows message

  :type  regionId: region identification number
  :param regionId: int
  """
  model_name = Cidade._meta.verbose_name
  cidade = Cidade.objects.get(id=cidadeId)
  nome = cidade.nome
  
  try:
    cidade.delete()
    messages.success(request, 'Cidade %s apagada com sucesso.' % (nome))
  except Exception as e:
    messages.error(request, 'Erro ao apagar cidade.') 

  return redirect(cidadeList)