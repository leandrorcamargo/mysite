#!/usr/bin/python
#-*- coding: utf-8 -*-

from mysite.models import Estado, Cidade
from django.http import JsonResponse
import json
from django.views.decorators.csrf import csrf_protect

@csrf_protect
def filtraCidade (request,estadoId):

  estado = Estado.objects.get(id=estadoId)
  cidades_data = []
  cidades = list(Cidade.objects.filter(estado=estado))
  for cidade in cidades:
    cidades_data.append({ 
      'id': cidade.id,
      'data': cidade.nome,  
    })

  return JsonResponse((cidades_data), status = 200, safe=False)