#-*- coding: utf-8 -*-
from mysite.views.cidade.cidadeList import *
from mysite.views.cidade.cidadeNew import *
from mysite.views.cidade.cidadeShow import *
from mysite.views.cidade.cidadeDelete import *
from mysite.views.cidade.cidadeEdit import *
from mysite.views.cidade.filtraCidade import *
from mysite.views.cidade.alteraRegiao import *