#!/usr/bin/python
#-*- coding: utf-8 -*-

from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib import messages
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_protect

from mysite.forms import CidadeForm
from mysite.models import Cidade
from mysite.views.cidade import cidadeList
from mysite.views.cidade import *

@login_required
@csrf_protect
def cidadeNew(request):
  """
  Display region creation screen
  """
  form = CidadeForm(request.POST or None)
  if form.is_valid():    
    cidade = form.save()
    messages.success(request, ('Cidade %s criada com sucesso.')% (cidade.nome))
    return redirect('cidadeList') 

  return render(request, 'cidade/form.html', 
    {'form': form,
     'button': 'Criar cidade'})