#!/usr/bin/python
#-*- coding: utf-8 -*-

from django.contrib.auth.decorators import login_required, user_passes_test
from django.forms.models import fields_for_model
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_protect


from mysite.models import Cidade
from mysite.views import *
from mysite.views.cidade import *

@login_required
@csrf_protect
def cidadeList(request):
  """
  Display all regions 
  """

  cidades = Cidade.objects.all()

  return render(request, 'cidade/list.html',
    {'tableHead': fields_for_model(Cidade, fields=('nome', 'estado', 'id')),
     'cidades': cidades})