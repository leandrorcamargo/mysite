#!/usr/bin/python
#-*- coding: utf-8 -*-

from mysite.views.index import *
from mysite.views.estado import *
from mysite.views.cidade import *
from mysite.views.categoria import *
from mysite.views.contato import *
from mysite.views.idioma import *
from mysite.views.atende import *
from mysite.views.biotipo import *
from mysite.views.seios import *
from mysite.views.informacao import *
from mysite.views.user import *
from mysite.views.anunciante import *
from mysite.views.plano import *
from mysite.views.caracteristica import *
from mysite.views.formaPagamento import *
from mysite.views.olhos import *
from mysite.views.ensaio import *
from mysite.views.anuncio import *
from mysite.views.anuncioD import *
from mysite.views.busca import *
from mysite.views.pagamento import *