#!/usr/bin/python
#-*- coding: utf-8 -*-

from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect

from mysite.forms import BuscaForm, AnuncioBuscaForm
from mysite.views import *
from django.views.decorators.csrf import csrf_protect
from mysite.models import Categoria, Anuncio
from django.db.models import Q
from random import shuffle
from datetime import datetime



def conditional_estado_filter(estado):
    if estado != None:
        return Q(estado=estado)
    else:
        return Q()
def conditional_cidade_filter(cidade):
    if cidade != None:
        return Q(cidade=cidade)
    else:
        return Q()
def conditional_nome_filter(nome):
    if nome != '':
        return Q(nome=nome)
    else:
        return Q()
def conditional_idade_filter(idade):
    if idade != None:
        return Q(idade=idade)
    else:
        return Q()
def conditional_categoria_filter(categoria):
    if categoria != None:
        return Q(categoria=categoria)
    else:
        return Q()
def conditional_olhos_filter(olhos):
    if olhos != None:
        return Q(olhos=olhos)
    else:
        return Q()
def conditional_seios_filter(seios):
    if seios != None:
        return Q(seios=seios)
    else:
        return Q()
def conditional_biotipo_filter(biotipo):
    if biotipo != None:
        return Q(biotipo=biotipo)
    else:
        return Q()

def conditional_idade1_filter(idade):
    if idade != None:
        return Q(idade1=idade)
    else:
        return Q()
def conditional_categoria1_filter(categoria):
    if categoria != None:
        return Q(categoria1=categoria)
    else:
        return Q()
def conditional_olhos1_filter(olhos):
    if olhos != None:
        return Q(olhos1=olhos)
    else:
        return Q()
def conditional_seios1_filter(seios):
    if seios != None:
        return Q(seios1=seios)
    else:
        return Q()
def conditional_biotipo1_filter(biotipo):
    if biotipo != None:
        return Q(biotipo1=biotipo)
    else:
        return Q()
def conditional_idade2_filter(idade):
    if idade != None:
        return Q(idade2=idade)
    else:
        return Q()
def conditional_categoria2_filter(categoria):
    if categoria != None:
        return Q(categoria2=categoria)
    else:
        return Q()
def conditional_olhos2_filter(olhos):
    if olhos != None:
        return Q(olhos2=olhos)
    else:
        return Q()
def conditional_seios2_filter(seios):
    if seios != None:
        return Q(seios2=seios)
    else:
        return Q()
def conditional_biotipo2_filter(biotipo):
    if biotipo != None:
        return Q(biotipo2=biotipo)
    else:
        return Q()

@csrf_protect
def busca(request):

  form2 = AnuncioBuscaForm(request.POST or None)
  anuncios_destaques = []
  anuncios_basicos = []
  anunciosD_destaques = []
  anunciosD_basicos = []
  today = datetime.now()
  if form2.is_valid():
    estado = form2.cleaned_data['estado']
    cidade = form2.cleaned_data['cidade']
    categoria = form2.cleaned_data['categoria']
    nome = form2.cleaned_data['nome']
    idade_min = form2.cleaned_data['idade_min']
    idade_max = form2.cleaned_data['idade']
    peso_min = form2.cleaned_data['peso_min']
    peso_max = form2.cleaned_data['peso']
    altura_min = form2.cleaned_data['altura_min']
    altura_max = form2.cleaned_data['altura']
    olhos = form2.cleaned_data['olhos']
    biotipo = form2.cleaned_data['biotipo']
    seios = form2.cleaned_data['seios']
    caracteristicas = list(form2.cleaned_data['caracteristicas'])
    informacoes = list(form2.cleaned_data['informacoes'])
    idiomas = list(form2.cleaned_data['idiomas'])
    atende = list(form2.cleaned_data['atende'])
    formasPagamento = list(form2.cleaned_data['formasPagamento'])

    if idade_min == None:
        idade_min = 0
    if idade_max == None:
        idade_max = 100
    if peso_min == None:
        peso_min = 0
    if peso_max == None:
        peso_max = 200
    if altura_min == None:
        altura_min = 0
    if altura_max == None:
        altura_max = 200

    anuncios = Anuncio.objects.filter(conditional_estado_filter(estado), conditional_cidade_filter(cidade), conditional_categoria_filter(categoria), 
    conditional_nome_filter(nome), conditional_olhos_filter(olhos), conditional_biotipo_filter(biotipo), conditional_seios_filter(seios), anunciante__aprovado = True, 
    data_exp__gte = today, idade__gte = idade_min, idade__lte = idade_max, peso__gte = peso_min, peso__lte = peso_max, altura__gte = altura_min, altura__lte = altura_max).distinct()

    anunciosD1 = AnuncioD.objects.filter(conditional_estado_filter(estado), conditional_cidade_filter(cidade), conditional_categoria1_filter(categoria), 
    conditional_nome_filter(nome), conditional_olhos1_filter(olhos), conditional_biotipo1_filter(biotipo), conditional_seios1_filter(seios), anunciante__aprovado = True, 
    data_exp__gte = today, idade1__gte = idade_min, idade1__lte = idade_max, peso1__gte = peso_min, peso1__lte = peso_max, altura1__gte = altura_min, altura1__lte = altura_max).distinct()

    anunciosD2 = AnuncioD.objects.filter(conditional_estado_filter(estado), conditional_cidade_filter(cidade), conditional_categoria2_filter(categoria), 
    conditional_nome_filter(nome), conditional_olhos2_filter(olhos), conditional_biotipo2_filter(biotipo), conditional_seios2_filter(seios), anunciante__aprovado = True, 
    data_exp__gte = today, idade2__gte = idade_min, idade2__lte = idade_max, peso2__gte = peso_min, peso2__lte = peso_max, altura2__gte = altura_min, altura2__lte = altura_max).distinct()


    for caracteristica in caracteristicas:
      anuncios = anuncios.filter(caracteristicas__pk=caracteristica.id).distinct()
      anunciosD1 = anunciosD1.filter(caracteristicas1__pk=caracteristica.id).distinct()
      anunciosD2 = anunciosD2.filter(caracteristicas2__pk=caracteristica.id).distinct()
    for informacao in informacoes:
      anuncios = anuncios.filter(informacoes__pk=informacao.id).distinct()
      anunciosD1 = anunciosD1.filter(informacoes__pk=informacao.id).distinct()
      anunciosD2 = anunciosD2.filter(informacoes__pk=informacao.id).distinct()
    for idioma in idiomas:
      anuncios = anuncios.filter(idiomas__pk=idioma.id).distinct()
      anunciosD1 = anunciosD1.filter(idiomas__pk=idioma.id).distinct()
      anunciosD2 = anunciosD2.filter(idiomas__pk=idioma.id).distinct()
    for atende_item in atende:
      anuncios = anuncios.filter(atende__pk=atende_item.id).distinct()
      anunciosD1 = anunciosD1.filter(atende__pk=atende_item.id).distinct()
      anunciosD2 = anunciosD2.filter(atende__pk=atende_item.id).distinct()
    for formaPagamento in formasPagamento:
      anuncios = anuncios.filter(formasPagamento__pk=formaPagamento.id).distinct()
      anunciosD1 = anunciosD1.filter(formasPagamento__pk=formaPagamento.id).distinct()
      anunciosD2 = anunciosD2.filter(formasPagamento__pk=formaPagamento.id).distinct()
    anuncios = list(anuncios)
    anunciosD1 = list(anunciosD1)
    anunciosD2 = list(anunciosD2)
    for anuncio in anuncios:
        if anuncio.plano.tipo == 1:
          anuncios_basicos.append(anuncio)
        else:
          anuncios_destaques.append(anuncio)

    anunciosD = list(set(anunciosD1 + anunciosD2))
    for anuncioD in anunciosD:
        if anuncioD.plano.tipo == 1:
          anunciosD_basicos.append(anuncioD)
        else:
          anunciosD_destaques.append(anuncioD)    
    shuffle(anuncios_basicos)
    shuffle(anuncios_destaques)
    shuffle(anunciosD_basicos)
    shuffle(anunciosD_destaques)
    return render(request, 'anuncio/list.html',
    {'anuncios_basicos': anuncios_basicos,
    'anuncios_destaques': anuncios_destaques,
    'anunciosD_basicos': anunciosD_basicos,
    'anunciosD_destaques': anunciosD_destaques})

  return render(request, 'busca.html', 
    {'form2': form2,
     'button': 'Buscar'})