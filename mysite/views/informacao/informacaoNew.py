#!/usr/bin/python
#-*- coding: utf-8 -*-

from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib import messages
from django.shortcuts import render
from django.views.decorators.csrf import csrf_protect
from django.http import HttpResponseRedirect
from mysite.settings import BASE_PATH

from mysite.forms import InformacaoForm
from mysite.views.informacao import *

@login_required
@csrf_protect
@user_passes_test(lambda u: u.is_superuser)
def informacaoNew(request):
  """
  Display region creation screen
  """
  form = InformacaoForm(request.POST or None)
  if form.is_valid():    
    informacao = form.save()
    messages.success(request, ('Informação %s criada com sucesso.')% (informacao.nome))
    location = BASE_PATH + 'informacao/'
    return HttpResponseRedirect(location)

  return render(request, 'informacao/form.html', 
    {'form': form,
     'button': 'Criar informação'})