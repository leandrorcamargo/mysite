#!/usr/bin/python
#-*- coding: utf-8 -*-

from django.contrib.auth.decorators import login_required, user_passes_test
from django.forms.models import fields_for_model
from django.shortcuts import render
from django.views.decorators.csrf import csrf_protect


from mysite.models import Informacao
from mysite.views import *
from mysite.views.informacao import *

@login_required
@csrf_protect
@user_passes_test(lambda u: u.is_superuser)
def informacaoList(request):
  """
  Display all regions 
  """

  informacoes = Informacao.objects.all()

  return render(request, 'informacao/list.html',
    {'tableHead': fields_for_model(Informacao, fields=('nome', 'id')),
     'informacoes': informacoes})