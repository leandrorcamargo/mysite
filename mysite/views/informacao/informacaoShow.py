#!/usr/bin/python
#-*- coding: utf-8 -*-

from django.contrib.auth.decorators import login_required, user_passes_test
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_protect

from mysite.models import Informacao
from mysite.views import *
from mysite.views.informacao import *

@login_required
@csrf_protect
@user_passes_test(lambda u: u.is_superuser)
def informacaoShow(request, informacaoId):
  """
  Display Region'

  :type  regionId: region identification number
  :param regionId: int
  """

  informacao = Informacao.objects.get(id=informacaoId)

  return render(request, 'informacao/show.html', 
    {'informacao': informacao})