#!/usr/bin/python
#-*- coding: utf-8 -*-

from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib import messages
from django.views.decorators.csrf import csrf_protect
from django.http import JsonResponse
from mysite.settings import BASE_PATH

from mysite.models import Informacao
from mysite.views import *
from mysite.views.informacao import *

@login_required
@csrf_protect
@user_passes_test(lambda u: u.is_superuser)
def informacaoDelete(request, informacaoId):
  """
  Display region edit screen
  if form is valid, saves and shows message

  :type  regionId: region identification number
  :param regionId: int
  """
  model_name = Informacao._meta.verbose_name
  informacao = Informacao.objects.get(id=informacaoId)
  nome = informacao.nome
  
  try:
    informacao.delete()
    messages.success(request, 'Informação %s apagada com sucesso.' % (nome))
  except Exception as e:
    messages.error(request, 'Erro ao apagar informação.') 

  location = BASE_PATH + 'informacao/'
  return JsonResponse((location), status = 200, safe=False)