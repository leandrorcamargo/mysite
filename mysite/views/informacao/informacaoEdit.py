#!/usr/bin/python
#-*- coding: utf-8 -*-

from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib import messages
from django.shortcuts import render
from django.views.decorators.csrf import csrf_protect
from django.http import HttpResponseRedirect
from mysite.settings import BASE_PATH

from mysite.forms import InformacaoForm
from mysite.models import Informacao
from mysite.views.informacao import *

@login_required
@csrf_protect
@user_passes_test(lambda u: u.is_superuser)
def informacaoEdit(request, informacaoId):
  """
  Display region edit screen
  if form is valid, saves and shows message

  :type  regionId: region identification number
  :param regionId: int
  """

  model_name = Informacao._meta.verbose_name
  informacao = Informacao.objects.get(id=informacaoId) 
  

  form = InformacaoForm(instance=informacao, data=request.POST or None)
  
  if form.is_valid():    
    informacao = form.save()
    messages.success(request, 'Cadastro da informação %s atualizado com sucesso.' % (informacao.nome))
    location = BASE_PATH + 'informacao/'
    return HttpResponseRedirect(location)

  return render(request, 'informacao/form.html', 
    {'title': ': <b>%s</b>' % (informacao.nome),
     'form': form,
     'button': 'Editar Informação'})