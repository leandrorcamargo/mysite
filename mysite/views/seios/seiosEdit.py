#!/usr/bin/python
#-*- coding: utf-8 -*-

from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib import messages
from django.shortcuts import render
from django.views.decorators.csrf import csrf_protect
from django.http import HttpResponseRedirect
from mysite.settings import BASE_PATH

from mysite.forms import SeiosForm
from mysite.models import Seios
from mysite.views.seios import *

@login_required
@csrf_protect
@user_passes_test(lambda u: u.is_superuser)
def seiosEdit(request, seiosId):
  """
  Display region edit screen
  if form is valid, saves and shows message

  :type  regionId: region identification number
  :param regionId: int
  """

  model_name = Seios._meta.verbose_name
  seios = Seios.objects.get(id=seiosId) 
  

  form = SeiosForm(instance=seios, data=request.POST or None)
  
  if form.is_valid():    
    seios = form.save()
    messages.success(request, 'Cadastro dos seios %s atualizado com sucesso.' % (seios.nome))
    location = BASE_PATH + 'seios/'
    return HttpResponseRedirect(location)

  return render(request, 'seios/form.html', 
    {'title': ': <b>%s</b>' % (seios.nome),
     'form': form,
     'button': 'Editar seios'})