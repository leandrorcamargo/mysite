#!/usr/bin/python
#-*- coding: utf-8 -*-

from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib import messages
from django.views.decorators.csrf import csrf_protect
from django.http import JsonResponse
from mysite.settings import BASE_PATH

from mysite.models import Seios
from mysite.views import *
from mysite.views.seios import *

@login_required
@csrf_protect
@user_passes_test(lambda u: u.is_superuser)
def seiosDelete(request, seiosId):
  """
  Display region edit screen
  if form is valid, saves and shows message

  :type  regionId: region identification number
  :param regionId: int
  """
  model_name = Seios._meta.verbose_name
  seios = Seios.objects.get(id=seiosId)
  nome = seios.nome
  
  try:
    seios.delete()
    messages.success(request, 'Seios %s apagado com sucesso.' % (nome))
  except Exception as e:
    messages.error(request, 'Erro ao apagar seios.') 

  location = BASE_PATH + 'seios/'
  return HttpResponseRedirect(location)