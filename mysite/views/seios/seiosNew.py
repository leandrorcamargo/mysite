#!/usr/bin/python
#-*- coding: utf-8 -*-

from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib import messages
from django.shortcuts import render
from django.views.decorators.csrf import csrf_protect
from django.http import HttpResponseRedirect
from mysite.settings import BASE_PATH

from mysite.forms import SeiosForm
from mysite.views.seios import *

@login_required
@csrf_protect
@user_passes_test(lambda u: u.is_superuser)
def seiosNew(request):
  """
  Display region creation screen
  """
  form = SeiosForm(request.POST or None)
  if form.is_valid():    
    seios = form.save()
    messages.success(request, ('Seios %s criado com sucesso.')% (seios.nome))
    location = BASE_PATH + 'seios/'
    return HttpResponseRedirect(location)

  return render(request, 'seios/form.html', 
    {'form': form,
     'button': 'Criar seios'})