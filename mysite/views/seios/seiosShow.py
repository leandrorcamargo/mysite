#!/usr/bin/python
#-*- coding: utf-8 -*-

from django.contrib.auth.decorators import login_required, user_passes_test
from django.shortcuts import render
from django.views.decorators.csrf import csrf_protect

from mysite.models import Seios
from mysite.views import *
from mysite.views.seios import *

@login_required
@csrf_protect
@user_passes_test(lambda u: u.is_superuser)
def seiosShow(request, seiosId):
  """
  Display Region'

  :type  regionId: region identification number
  :param regionId: int
  """

  seios = Seios.objects.get(id=seiosId)

  return render(request, 'seios/show.html', 
    {'seios': seios})