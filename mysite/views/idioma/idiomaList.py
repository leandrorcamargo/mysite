#!/usr/bin/python
#-*- coding: utf-8 -*-

from django.contrib.auth.decorators import login_required, user_passes_test
from django.forms.models import fields_for_model
from django.shortcuts import render
from django.views.decorators.csrf import csrf_protect


from mysite.models import Idioma
from mysite.views import *
from mysite.views.idioma import *

@login_required
@csrf_protect
@user_passes_test(lambda u: u.is_superuser)
def idiomaList(request):
  """
  Display all regions 
  """

  idiomas = Idioma.objects.all()

  return render(request, 'idioma/list.html',
    {'tableHead': fields_for_model(Idioma, fields=('nome', 'id')),
     'idiomas': idiomas})