#!/usr/bin/python
#-*- coding: utf-8 -*-

from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib import messages
from django.views.decorators.csrf import csrf_protect
from django.http import JsonResponse
from mysite.settings import BASE_PATH

from mysite.models import Idioma
from mysite.views import *
from mysite.views.idioma import *

@login_required
@csrf_protect
@user_passes_test(lambda u: u.is_superuser)
def idiomaDelete(request, idiomaId):
  """
  Display region edit screen
  if form is valid, saves and shows message

  :type  regionId: region identification number
  :param regionId: int
  """
  model_name = Idioma._meta.verbose_name
  idioma = Idioma.objects.get(id=idiomaId)
  nome = idioma.nome
  
  try:
    idioma.delete()
    messages.success(request, 'Idioma %s apagado com sucesso.' % (nome))
  except Exception as e:
    messages.error(request, 'Erro ao apagar idioma.') 

  location = BASE_PATH + 'idioma/'
  return HttpResponseRedirect(location)