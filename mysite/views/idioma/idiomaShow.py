#!/usr/bin/python
#-*- coding: utf-8 -*-

from django.contrib.auth.decorators import login_required, user_passes_test
from django.shortcuts import render
from django.views.decorators.csrf import csrf_protect

from mysite.models import Idioma
from mysite.views import *
from mysite.views.idioma import *

@login_required
@csrf_protect
@user_passes_test(lambda u: u.is_superuser)
def idiomaShow(request, idiomaId):
  """
  Display Region'

  :type  regionId: region identification number
  :param regionId: int
  """

  idioma = Idioma.objects.get(id=idiomaId)

  return render(request, 'idioma/show.html', 
    {'idioma': idioma})