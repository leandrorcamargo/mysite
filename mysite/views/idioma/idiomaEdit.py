#!/usr/bin/python
#-*- coding: utf-8 -*-

from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib import messages
from django.shortcuts import render
from django.views.decorators.csrf import csrf_protect
from django.http import HttpResponseRedirect
from mysite.settings import BASE_PATH

from mysite.forms import IdiomaForm
from mysite.models import Idioma
from mysite.views.idioma import *

@login_required
@csrf_protect
@user_passes_test(lambda u: u.is_superuser)
def idiomaEdit(request, idiomaId):
  """
  Display region edit screen
  if form is valid, saves and shows message

  :type  regionId: region identification number
  :param regionId: int
  """

  model_name = Idioma._meta.verbose_name
  idioma = Idioma.objects.get(id=idiomaId) 
  

  form = IdiomaForm(instance=idioma, data=request.POST or None)
  
  if form.is_valid():    
    idioma = form.save()
    messages.success(request, 'Cadastro do idioma %s atualizado com sucesso.' % (idioma.nome))
    location = BASE_PATH + 'idioma/'
    return HttpResponseRedirect(location)

  return render(request, 'idioma/form.html', 
    {'title': ': <b>%s</b>' % (idioma.nome),
     'form': form,
     'button': 'Editar Idioma'})