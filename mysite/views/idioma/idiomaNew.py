#!/usr/bin/python
#-*- coding: utf-8 -*-

from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib import messages
from django.shortcuts import render
from django.views.decorators.csrf import csrf_protect
from django.http import HttpResponseRedirect
from mysite.settings import BASE_PATH

from mysite.forms import IdiomaForm
from mysite.views.idioma import *

@login_required
@csrf_protect
@user_passes_test(lambda u: u.is_superuser)
def idiomaNew(request):
  """
  Display region creation screen
  """
  form = IdiomaForm(request.POST or None)
  if form.is_valid():    
    idioma = form.save()
    messages.success(request, ('Idioma %s criado com sucesso.')% (idioma.nome))
    location = BASE_PATH + 'idioma/'
    return HttpResponseRedirect(location)

  return render(request, 'idioma/form.html', 
    {'form': form,
     'button': 'Criar idioma'})