#!/usr/bin/python
#-*- coding: utf-8 -*-

from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib import messages
from django.views.decorators.csrf import csrf_protect
from django.http import HttpResponseRedirect
from mysite.settings import BASE_PATH

from mysite.models import Categoria
from mysite.views import *
from mysite.views.categoria import *

@login_required
@csrf_protect
@user_passes_test(lambda u: u.is_superuser)
def categoriaDelete(request, categoriaId):
  """
  Display region edit screen
  if form is valid, saves and shows message

  :type  regionId: region identification number
  :param regionId: int
  """
  model_name = Categoria._meta.verbose_name
  categoria = Categoria.objects.get(id=categoriaId)
  nome = categoria.nome
  
  try:
    categoria.delete()
    messages.success(request, 'Categoria %s apagada com sucesso.' % (nome))
  except Exception as e:
    messages.error(request, 'Erro ao apagar categoria.') 

  location = BASE_PATH + 'categoria/'
  return HttpResponseRedirect(location)