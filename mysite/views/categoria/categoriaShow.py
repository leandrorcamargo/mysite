#!/usr/bin/python
#-*- coding: utf-8 -*-

from django.contrib.auth.decorators import login_required, user_passes_test
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_protect

from mysite.models import Categoria
from mysite.views import *
from mysite.views.categoria import *

@login_required
@csrf_protect
@user_passes_test(lambda u: u.is_superuser)
def categoriaShow(request, categoriaId):
  """
  Display Region'

  :type  regionId: region identification number
  :param regionId: int
  """

  categoria = Categoria.objects.get(id=categoriaId)

  return render(request, 'categoria/show.html', 
    {'categoria': categoria})