#!/usr/bin/python
#-*- coding: utf-8 -*-

from django.contrib.auth.decorators import login_required, user_passes_test
from django.forms.models import fields_for_model
from django.shortcuts import render
from django.views.decorators.csrf import csrf_protect


from mysite.models import Categoria
from mysite.views import *
from mysite.views.categoria import *

@login_required
@csrf_protect
@user_passes_test(lambda u: u.is_superuser)
def categoriaList(request):
  """
  Display all regions 
  """

  categorias = Categoria.objects.all()

  return render(request, 'categoria/list.html',
    {'tableHead': fields_for_model(Categoria, fields=('nome', 'id')),
     'categorias': categorias})