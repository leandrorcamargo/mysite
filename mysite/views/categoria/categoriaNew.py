#!/usr/bin/python
#-*- coding: utf-8 -*-

from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib import messages
from django.shortcuts import render
from django.views.decorators.csrf import csrf_protect
from django.http import HttpResponseRedirect
from mysite.settings import BASE_PATH

from mysite.forms import CategoriaForm
from mysite.models import Categoria
from mysite.views.categoria import *

@login_required
@csrf_protect
@user_passes_test(lambda u: u.is_superuser)
def categoriaNew(request):
  """
  Display region creation screen
  """
  form = CategoriaForm(request.POST or None)
  if form.is_valid():    
    categoria = form.save()
    messages.success(request, ('Categoria %s criada com sucesso.')% (categoria.nome))
    location = BASE_PATH + 'categoria/'
    return HttpResponseRedirect(location)

  return render(request, 'categoria/form.html', 
    {'form': form,
     'button': 'Criar categoria'})