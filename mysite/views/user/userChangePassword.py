#!/usr/bin/python
#-*- coding: utf-8 -*-

from django.contrib import messages
from django.contrib.auth import authenticate, login, update_session_auth_hash
from django.contrib.auth.models import User
from django.shortcuts import render, redirect
from django.views.decorators.cache import never_cache
from django.views.decorators.csrf import csrf_protect
from django.contrib.auth.forms import PasswordChangeForm

from mysite.models import Anunciante
from mysite.views import index
from mysite.views.anunciante import *
from mysite.views.user import *

@csrf_protect
@never_cache
def userChangePassword(request, anuncianteId):
  form = PasswordChangeForm(user=request.user, data=request.POST or None)
  if form.is_valid():
    form.save()
    update_session_auth_hash(request, form.user)
    messages.success(request, 'Senha alterada com sucesso.')
    return redirect(anuncianteShow, anuncianteId=anuncianteId)

  return render(request, 'template_login.html', 
    {'form': form,
     'button': 'Alterar senha'})