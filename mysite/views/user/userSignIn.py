#!/usr/bin/python
#-*- coding: utf-8 -*-

from django.contrib import messages
from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User
from django.shortcuts import render, redirect
from django.views.decorators.cache import never_cache
from django.views.decorators.csrf import csrf_protect

from mysite.models import Anunciante, Anuncio, AnuncioD
from mysite.forms import UserSignInForm
from mysite.views import index
from mysite.views.anuncio import anuncioList
from mysite.views import *
from mysite.views.user import *

@csrf_protect
@never_cache
def userSignIn(request):
  """
  Shows authentication screen
  Logs user in
  """
  # if request.user.is_authenticated():
  #   return redirect(index)

  form = UserSignInForm(request.POST or None)
  if form.is_valid():
    email = form.cleaned_data['email']
    password = form.cleaned_data['password']

    user = None
    if '@' in email:
      if User.objects.filter(email=email).exists():
        user = User.objects.get(email=email)
        user = authenticate(username=user.username, password=password)    
    if user is not None:
      if 'cidade' in request.session:
        estado = request.session['estado']
        cidade = request.session['cidade']
        login(request, user)
        request.session['estado'] = estado
        request.session['cidade'] = cidade
      
      else:
        login(request, user)
      if 'anuncios_list' in request.session:
        del request.session['anuncios_list']
      if 'anunciosD_list' in request.session:
        del request.session['anunciosD_list'] 
      request.session.set_expiry(3000)
      anunciante = Anunciante.objects.get(user=user)
      request.session['anunciante'] = anunciante.id
      request.session['aprovado'] = anunciante.aprovado
      if Anuncio.objects.filter(anunciante__pk=anunciante.id).exists() or AnuncioD.objects.filter(anunciante__pk=anunciante.id).exists():
        request.session['tem_anuncio'] = True
      else:
        request.session['tem_anuncio'] = False
      return redirect(anuncioList)
    else:
      messages.error(request, u'Email ou senha inválidos')

  return render(request, 'template_login.html', 
    {'form': form,
     'button': 'Entrar',
     'title': ''})