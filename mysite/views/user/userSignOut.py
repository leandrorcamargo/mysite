#!/usr/bin/python
#-*- coding: utf-8 -*-

from django.contrib.auth import logout
from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect

from mysite.views import *
from mysite.views.user import *
from mysite.views.anuncio import anuncioList

@login_required
def userSignOut(request):
  """
  Logout the user
  """
  estado = request.session['estado']
  cidade = request.session['cidade']
  logout(request)
  request.session['estado'] = estado
  request.session['cidade'] = cidade
  return redirect(anuncioList)