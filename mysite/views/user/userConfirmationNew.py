#!/usr/bin/python
#-*- coding: utf-8 -*-

from django.contrib import messages
from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User
from django.shortcuts import render, redirect
from django.views.decorators.cache import never_cache
from django.views.decorators.csrf import csrf_protect
from django.utils.translation import gettext_lazy as _

from mysite.forms import UserConfirmationForm
from mysite.models import UserConfirmation, Category
from mysite.views.anuncio import anuncioList
from mysite.views import *
from mysite.views.user import *

@csrf_protect
@never_cache
def userConfirmationNew(request):
  """
  Confirmation View

  Display a confirmation form of :model:`optisensor.UserConfirmation`.

  **Templates**

    :template:`template_login.html`.

  **Context**

    title
      'Resend account activation instructions'
    form
      Form related to :model:`optisensor.UserConfirmation`.
    button
      Resend Instructions.
    links
      Link to URL: sign_in.
      Link to URL: userPasswordNew.
  """

  if request.user.is_authenticated():
    return redirect(anuncioList)
    
  form = UserConfirmationForm(request.POST or None)

  if form.is_valid(): 
    if User.objects.filter(email=form.cleaned_data['email']).exists(): #if email is found
      user = User.objects.get(email=form.cleaned_data['email'])
      confirmation = UserConfirmation.objects.get(user = user, confirmed=False) #if email not confirmed
      if confirmation != None:
        subject = invariants.message_subject_confirmation
        
        message = invariants.message_body_confirmation % (user.first_name)

        button = invariants.button_account_confirm

        context = {'confirmation_url': request.build_absolute_uri(None)+confirmation.token, 
          'email': user.email, 
          'message': message, 
          'button': button}

        form.save(subject, context, user.email)
        messages.info(request, _('Confirmations already done.'))

        return redirect(index) 
      else:
        messages.error(request, _('No confirmation pending linked to this email address.'))
    else:
      messages.error(request, invariants.alert_not_found_error % (user.email._meta.get_field('email').verbose_name))

  return render(request, 'template_login.html', 
    {'title': _('Resend account activation instructions'),
     'form': form,
     'button': _('Resend instructions'),
     'links': [{'name':_('Sign In'), 'url': 'sign_in'},
               {'name':_('Forgot your password?'), 'url': 'userPasswordNew'} ]})