#!/usr/bin/python
#-*- coding: utf-8 -*-

from django.contrib.auth.forms import SetPasswordForm
from django.http import Http404
from django.shortcuts import render, redirect, get_object_or_404
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from django.views.decorators.cache import never_cache
from django.views.decorators.csrf import csrf_protect

from mysite.controllers import invariants, mail
from mysite.settings import DEFAULT_FROM_EMAIL

from mysite.models import UserConfirmation, Category
from mysite.views import *
from mysite.views.user import *
from mysite.views.anuncio import anuncioList

@csrf_protect
@never_cache
def userPasswordNewConfirm(request, token):
  """
  Confirmaçao de E-mail com criação de senha
  """
  
  if UserConfirmation.objects.filter(token=token, category=Category.PASSWORD, confirmed=False).exists():
    reset = get_object_or_404(UserConfirmation, token=token)
    form = SetPasswordForm(user=reset.user, data=request.POST or None)
    if form.is_valid(): 
      form.save()
      reset.confirmed = True #Changes confirmation status
      reset.confirmed_at = timezone.now()
      reset.confirmed_by = 1
      reset.save()
      subject = invariants.message_subject_anunciante_created
      message = invariants.message_body_anunciante_created % (reset.user.username )
      button = invariants.button_verify_anunciante
      context = {'confirmation_url': request.build_absolute_uri(None)+"anunciante/", 
        'message': message,
        'button': button}
      mail.sendMailTemplate(subject, context, [DEFAULT_FROM_EMAIL])
      return redirect(anuncioList)
  else:
    raise Http404(invariants.alert_token_error)

  return render(request, 'template_login.html', 
    {'form': form, 
     'button': invariants.button_set_password })
