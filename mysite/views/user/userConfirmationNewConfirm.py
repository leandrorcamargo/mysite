#!/usr/bin/python
#-*- coding: utf-8 -*-

from django.contrib import messages
from django.contrib.auth.models import User
from django.http import Http404
from django.shortcuts import render, redirect, get_object_or_404
from django.utils import timezone
from django.views.decorators.cache import never_cache
from django.views.decorators.csrf import csrf_protect
from django.utils.translation import gettext_lazy as _

from mysite.controllers import invariants
from mysite.models import UserConfirmation, Category
from mysite.views.anuncio import anuncioList
from mysite.views import *
from mysite.views.user import *

@csrf_protect
@never_cache
def userConfirmationNewConfirm(request, anuncianteId, token):
  """
  Confirmação de email após alteração de endereço
  """
  if UserConfirmation.objects.filter(token=token, confirmed=False).exists(): #Verify if unconfirmed token exists
    reset = get_object_or_404(UserConfirmation, token=token)
    if reset.category == Category.PASSWORD: #checks if token category isnt 'verification'
      return userPasswordNewConfirm(request, token)
    else:
      reset.confirmed = True #confirms the email
      reset.confirmed_at = timezone.now()
      reset.confirmed_by = 1
      reset.save()
      reset.user.username = reset.user.email
      reset.user.save()
      messages.success(request, _('Confirmation done successfully.'))

    return redirect(anuncioList)
  else:
    raise Http404(invariants.alert_token_error)

  return render(request, 'template_login.html')
