#!/usr/bin/python
#-*- coding: utf-8 -*-

from django.contrib import messages
from django.contrib.auth.models import User
from django.shortcuts import render, redirect
from django.utils.translation import gettext_lazy as _
from django.views.decorators.cache import never_cache
from django.views.decorators.csrf import csrf_protect

from mysite.controllers import utils
from mysite.forms import UserConfirmationForm
from mysite.models import UserConfirmation, Category
from mysite.views import *
from mysite.views.user import *
from mysite.views.anuncio import anuncioList

@csrf_protect
@never_cache
def userPasswordNew(request):
  """
  Confirmation View

  Display password reset request screen.  Sends password reset email if form is valid. Display error message if email not found.

  **Templates**

    :template:`template_login.html`.

  **Context**

    title
      'Password Recovery'
    label
      We'll send you the password reset instructions
    form
      Form related to :model:`optisensor.UserConfirmation`.
    button
      Send me the instructions.
    links
      Link to URL: sign_in.
      Link to URL: userConfirmationNew.
  """

  #if request.user.is_authenticated():
    #return redirect(anuncioList)
    
  form = UserConfirmationForm(request.POST or None)

  if form.is_valid(): 
    if User.objects.filter(email=form.cleaned_data['email']).exists():
      user = User.objects.get(email=form.cleaned_data['email'])
      token = utils.generateHashKey(user.username)
      reset = UserConfirmation(token=token, user=user, category=Category.PASSWORD)
      reset.save()

      subject = _('Hot Anúncios Instruções de Reset de Senha')
      
      message = _(u'''Olá %s, <br/>
        Você tem certeza que deseja resetar sua senha? 
        Clique aqui para confirmar.
        ''') % (user.first_name)

      button = _('Resetar Senha')

      context = {'confirmation_url': request.build_absolute_uri(None)+reset.token, 
        'email': user.email, 
        'message': message, 
        'button': button}

      form.save(subject, context, user.email)
      messages.success(request, _('Password reset instructions sent successfully.'))

      return redirect(anuncioList) 
    else:
      messages.error(request, invariants.alert_not_found_error % (user.email._meta.get_field('email').verbose_name))

  return render(request, 'template_login.html', 
    {'label': _(u"Enviaremos as instruções para recuperar a senha. Digite o e-mail cadastrado:"),
     'form': form,
     'button': _(u'Enviar instruções')})
