#!/usr/bin/python
#-*- coding: utf-8 -*-

#from django.contrib.auth.decorators import login_required
from django.forms.models import fields_for_model
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_protect
from django.contrib.auth.decorators import login_required


from mysite.models import Anunciante, Anuncio, Ensaio, AnuncioD
from mysite.views import *
from mysite.views.ensaio import *

@login_required
@csrf_protect
def ensaioList(request):

  usuario = request.user  
  anuncianteId = request.session['anunciante']
  if usuario.is_superuser:
    anuncios = list(Anuncio.objects.all())
    anunciosD = list(AnuncioD.objects.all())
  else:
    anuncios = list(Anuncio.objects.filter(anunciante__pk=anuncianteId))
    anunciosD = list(AnuncioD.objects.filter(anunciante__pk=anuncianteId))
  ensaios = []
  ensaiosAnuncio = list()
  for anuncio in anuncios:
  	ensaiosAnuncio = list(Ensaio.objects.filter(anuncio = anuncio))
  	if (ensaiosAnuncio):
  		ensaios += list(set(ensaiosAnuncio))
  for anuncioD in anunciosD:
  	ensaiosAnuncioD = list(Ensaio.objects.filter(anuncioD = anuncioD))
  	if (ensaiosAnuncioD):
  		ensaios += list(set(ensaiosAnuncioD))
  

  return render(request, 'ensaio/list.html',
    {'tableHead': fields_for_model(Ensaio, fields=('nome', 'anuncio', 'anuncioD')),
     'ensaios': ensaios})