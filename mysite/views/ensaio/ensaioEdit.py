
#!/usr/bin/python
#-*- coding: utf-8 -*-

from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib import messages
from django.shortcuts import render, redirect
from django.core.files.storage import default_storage as storage
from django.views.decorators.csrf import csrf_protect
from django.http import JsonResponse, HttpResponseRedirect
from django.core.files.storage import default_storage

from mysite.settings import BASE_PATH, MEDIA_URL
from mysite.forms import EnsaioForm
from mysite.models import Ensaio
from mysite.models import Anunciante, Anuncio
from mysite.views.ensaio import ensaioList
from mysite.views.ensaio import *
from io import BytesIO
from PIL import Image, ExifTags, ImageOps
import time
import magic

def add_watermark(image_file, filesRotated, anuncioId, logo_file ="static/mysite/logo2.png"):
  filesRotated[image_file].name = filesRotated[image_file].name.replace(" ", "").replace("(", "").replace(")", "")
  path = 'pictures/ensaio/'+ str(anuncioId)+ '/1_'+ filesRotated[image_file].name
  if default_storage.exists(path):
    im = storage.open(r'pictures/ensaio/' + str(anuncioId) + r'/1_' +filesRotated[image_file].name,'rb')
  else:
    im = storage.open(r'pictures/ensaio/' + str(anuncioId) + r'/' +filesRotated[image_file].name ,'rb')
  img = Image.open(im).convert('RGBA')
  

  sfile = BytesIO()

  logo = Image.open(logo_file).convert('RGBA')
  logo.load()
  new_size = min(img.size[0], img.size[1])
  logo = logo.resize((new_size, new_size))
  paste_mask = logo.point(lambda i: i * 20 / 100)

  # position the watermark
  offset_x = int(img.size[0]/2 - logo.size[0]/2)
  offset_y = int(img.size[1]/2 - logo.size[1]/2)

  
  img.paste(logo, (offset_x, offset_y), mask = paste_mask)
  img = img.convert('RGB')
  img.save(sfile, "JPEG")
  im.close()
  img.close()
  #imw = storage.open(MEDIA_URL + 'pictures/ensaio/' + str(anuncioId) + '/' +filesRotated[image_file].name,'wb')
  if default_storage.exists(path):
    imw = storage.open(r'pictures/ensaio/' + str(anuncioId) + r'/1_' +filesRotated[image_file].name,'wb')
  else:
    imw = storage.open(r'pictures/ensaio/' + str(anuncioId) + r'/' +filesRotated[image_file].name ,'wb')
  imw.write(sfile.getvalue())
  imw.flush()
  imw.close()


@login_required
@csrf_protect
def ensaioEdit(request, ensaioId):
  """
  Display region edit screen
  if form is valid, saves and shows message

  :type  regionId: region identification number
  :param regionId: int
  """

  usuario = request.user
  model_name = Ensaio._meta.verbose_name
  ensaio = Ensaio.objects.get(id=ensaioId)
  anuncio = ensaio.anuncio
  valid_mime_types = ['image/jpeg', 'image/png', 'image/gif']
  if usuario.is_superuser:
    pass
  else:
    if anuncio:
      if ensaio.anuncio.anunciante.user == request.user:
        pass
      else:
        return redirect(ensaioList)
    else:
      if ensaio.anuncioD.anunciante.user == request.user:
        pass
      else:
        return redirect(ensaioList)
  request.POST._mutable = True
  filesRotated = request.FILES or None
  if filesRotated:
    for f in filesRotated:
      file_mime_type = magic.from_buffer(filesRotated[f].read(), mime=True)
      if file_mime_type in valid_mime_types:
        try:
          image=Image.open(filesRotated[f])
          for orientation in ExifTags.TAGS.keys():
            if ExifTags.TAGS[orientation]=='Orientation':
              break
          exif=dict(image._getexif().items())

          if exif[orientation] == 3:
            image = image.rotate(180, expand=True)
          elif exif[orientation] == 6:
            image=image.rotate(270, expand=True)
          elif exif[orientation] == 8:
            image = image.rotate(90, expand=True)
          image.thumbnail((1000 , 1000), Image.ANTIALIAS)
          image.save(filesRotated[f].temporary_file_path(), "JPEG")
          image.close()
        except (AttributeError, KeyError, IndexError):
          pass
  
  if anuncio:
    form = EnsaioForm(instance=ensaio, initial={'anuncio':anuncio}, data = request.POST or None, files = filesRotated or None)
  else:
    form = EnsaioForm(instance=ensaio, initial={'anuncioD':ensaio.anuncioD}, data = request.POST or None, files = filesRotated or None)
  if not request.user.is_superuser:
    form.fields["anuncio"].queryset = Anuncio.objects.filter(anunciante__pk=request.session['anunciante'])
    form.fields["anuncioD"].queryset = AnuncioD.objects.filter(anunciante__pk=request.session['anunciante'])
  
  if form.is_valid():
    fotos_list = [ensaio.foto1, ensaio.foto2, ensaio.foto3, ensaio.foto4, ensaio.foto5, ensaio.foto6, ensaio.foto7, ensaio.foto8, ensaio.foto9, ensaio.foto10]
    video_original = ensaio.video
    fotos_form_list = [form.cleaned_data.get('foto1'), form.cleaned_data.get('foto2'), form.cleaned_data.get('foto3'), form.cleaned_data.get('foto4'),
    form.cleaned_data.get('foto5'), form.cleaned_data.get('foto6'), form.cleaned_data.get('foto7'), form.cleaned_data.get('foto8'),
    form.cleaned_data.get('foto9'), form.cleaned_data.get('foto10')]
    video_new = form.cleaned_data.get('video')
    for i in range(0,10):
      if fotos_list[i] != fotos_form_list[i]:
        try:
          fotos_list[i].delete(save=False)
        except:
          pass

    if video_new != video_original:
      try:
        video_original.delete(save=False)
      except:
        pass

    ensaio = form.save()
    if filesRotated:
      for idx, img_file in enumerate(filesRotated):
        file_mime_type = magic.from_buffer(img_file, mime=True)
        if file_mime_type in valid_mime_types:
          if anuncio:
            add_watermark(img_file, filesRotated,  anuncio.id)
          else:
            add_watermark(img_file, filesRotated,  ensaio.anuncioD.id)
    messages.success(request, u"Ensaio editado com sucesso.") 
    location = BASE_PATH + 'ensaio/'
    return HttpResponseRedirect(location)

  return render(request, 'ensaio/form.html', 
    {'form': form,
     'button':u"Editar ensaio"})