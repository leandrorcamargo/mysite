#!/usr/bin/python
#-*- coding: utf-8 -*-

from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.shortcuts import render, redirect
from django.core.files.storage import default_storage as storage
from django.views.decorators.csrf import csrf_protect
from django.http import HttpResponseRedirect
from django.core.files.storage import default_storage

from mysite.settings import BASE_PATH, MEDIA_URL
from mysite.controllers import invariants
from mysite.models import Anuncio, Ensaio, Anunciante, Plano, AnuncioD
from mysite.forms import EnsaioForm
from mysite.views.ensaio import ensaioShow
from io import BytesIO
from PIL import Image, ExifTags, ImageOps
import magic



def add_watermark(image_file, filesRotated, anuncioId, logo_file ="static/mysite/logo2.png"):
  filesRotated[image_file].name = filesRotated[image_file].name.replace(" ", "").replace("(", "").replace(")", "")

  path = 'pictures/ensaio/'+ str(anuncioId)+ '/1_'+ filesRotated[image_file].name
  if default_storage.exists(path):
    im = storage.open(r'pictures/ensaio/' + str(anuncioId) + r'/1_' +filesRotated[image_file].name,'rb')
  else:
    im = storage.open(r'pictures/ensaio/' + str(anuncioId) + r'/' +filesRotated[image_file].name ,'rb')
  img = Image.open(im).convert('RGBA')
  

  sfile = BytesIO()

  logo = Image.open(logo_file).convert('RGBA')
  logo.load()
  new_size = min(img.size[0], img.size[1])
  logo = logo.resize((new_size, new_size))
  paste_mask = logo.point(lambda i: i * 20 / 100)

  # position the watermark
  offset_x = int(img.size[0]/2 - logo.size[0]/2)
  offset_y = int(img.size[1]/2 - logo.size[1]/2)

  
  img.paste(logo, (offset_x, offset_y), mask = paste_mask)
  img = img.convert('RGB')
  img.save(sfile, "JPEG")
  im.close()
  img.close()
  #imw = storage.open(MEDIA_URL + 'pictures/ensaio/' + str(anuncioId) + '/' +filesRotated[image_file].name,'wb')
  if default_storage.exists(path):
    imw = storage.open(r'pictures/ensaio/' + str(anuncioId) + r'/1_' +filesRotated[image_file].name,'wb')
  else:
    imw = storage.open(r'pictures/ensaio/' + str(anuncioId) + r'/' +filesRotated[image_file].name ,'wb')
  imw.write(sfile.getvalue())
  imw.flush()
  imw.close()


@login_required
@csrf_protect
def ensaioNew(request):
  
  usuario = request.user
  model_name = Ensaio._meta.verbose_name
    
  if not request.session['tem_anuncio']:
    return redirect("index")
    
  request.POST._mutable = True
  filesRotated = request.FILES or None
  if filesRotated:
    for f in filesRotated:
      try:
        image=Image.open(filesRotated[f])
        for orientation in ExifTags.TAGS.keys():
          if ExifTags.TAGS[orientation]=='Orientation':
            break
        exif=dict(image._getexif().items())

        if exif[orientation] == 3:
          image = image.rotate(180, expand=True)
        elif exif[orientation] == 6:
          image=image.rotate(270, expand=True)
        elif exif[orientation] == 8:
          image = image.rotate(90, expand=True)
        image.thumbnail((1000 , 1000), Image.ANTIALIAS)
        image.save(filesRotated[f].temporary_file_path(), "JPEG")
        image.close()
      except (AttributeError, KeyError, IndexError):
        pass
  
  form = EnsaioForm(data = request.POST or None, files = filesRotated or None)
  if not request.user.is_superuser:
    form.fields["anuncio"].queryset = Anuncio.objects.filter(anunciante__pk=request.session['anunciante'])
    form.fields["anuncioD"].queryset = AnuncioD.objects.filter(anunciante__pk=request.session['anunciante'])

  if form.is_valid():
    anuncio = form.instance.anuncio
    anuncioD = form.instance.anuncioD
    if anuncio:
      ensaios = list (Ensaio.objects.filter(anuncio = anuncio)) 
      if anuncio.plano.ensaios > len(ensaios):        
        ensaio = form.save()
        if filesRotated:
          for idx, img_file in enumerate(filesRotated):
            add_watermark(img_file, filesRotated,  ensaio.anuncio.id)
        messages.success(request, u"Ensaio criado com sucesso.")    
        location = BASE_PATH + 'ensaio/'
        return HttpResponseRedirect(location)
      else:
        messages.error(request, 'Você já criou os ' + str(anuncio.plano.ensaios) + ' ensaios disponíveis no seu plano.')
        location = BASE_PATH + 'anuncio/s' + str(anuncio.id)+"/"
        return HttpResponseRedirect(location)
    else:
      ensaios = list (Ensaio.objects.filter(anuncioD = anuncioD)) 
      if anuncioD.plano.ensaios > len(ensaios):        
        ensaio = form.save()
        if filesRotated:
          valid_mime_types = ['image/jpeg', 'image/png', 'image/gif']
          for idx, img_file in enumerate(filesRotated):
            file_mime_type = magic.from_buffer(img_file, mime=True)
            if file_mime_type in valid_mime_types:
              add_watermark(img_file, filesRotated,  ensaio.anuncioD.id)
        messages.success(request, u"Ensaio criado com sucesso.")    
        location = BASE_PATH + 'ensaio/'
        return HttpResponseRedirect(location)
      else:
        messages.error(request, 'Você já criou os ' + str(anuncioD.plano.ensaios) + ' ensaios disponíveis no seu plano.')
        location = BASE_PATH + 'anuncio/s' + str(anuncioD.id)+"/"
        return HttpResponseRedirect(location)

  return render(request, 'ensaio/form.html', 
    {'form': form,
     'button':u"Criar ensaio"})