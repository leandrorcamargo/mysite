#!/usr/bin/python
#-*- coding: utf-8 -*-
from mysite.views.ensaio.ensaioList import *
from mysite.views.ensaio.ensaioShow import *
from mysite.views.ensaio.ensaioNew import *
from mysite.views.ensaio.ensaioDelete import *
from mysite.views.ensaio.ensaioEdit import *