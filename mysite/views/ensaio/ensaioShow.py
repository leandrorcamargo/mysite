#!/usr/bin/python
#-*- coding: utf-8 -*-

from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_protect

from mysite.models import Ensaio
from mysite.views import *
from mysite.views.ensaio import *


@csrf_protect
def ensaioShow(request, ensaioId):
  
  ensaio = Ensaio.objects.get(id=ensaioId)

  return render(request, 'ensaio/show.html', 
    {'ensaio': ensaio})
