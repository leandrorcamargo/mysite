#!/usr/bin/python
#-*- coding: utf-8 -*-

#from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_protect
from django.http import HttpResponseRedirect
from mysite.settings import BASE_PATH

from mysite.models import Ensaio
from mysite.views import *
from mysite.views.ensaio import *

@login_required
@csrf_protect
def ensaioDelete(request, ensaioId):

  model_name = Ensaio._meta.verbose_name
  ensaio = Ensaio.objects.get(id=ensaioId)
  anuncio = ensaio.anuncio
  if anuncio:
    user = ensaio.anuncio.anunciante.user
  else:
    user = ensaio.anuncioD.anunciante.user
  nome =ensaio.nome
  
  try:
    if request.user.is_superuser or user==request.user:
      fotos_list = [ensaio.foto1, ensaio.foto2, ensaio.foto3, ensaio.foto4, ensaio.foto5, ensaio.foto6, ensaio.foto7, ensaio.foto8, ensaio.foto9, ensaio.foto10]
      for foto in fotos_list:
        try:
          foto.delete(save=False)
        except:
          pass
      try:
        ensaio.video.delete(save=False)
      except:
        pass
      ensaio.delete()
      messages.success(request, u'Ensaio %s pagado com sucesso.' % (nome))
    else:
      messages.error(request, u'Usuário sem autorização para deletar este ensaio.') 

  except Exception as e:
    messages.error(request, u'Erro ao apagar ensaio.') 

  location = BASE_PATH + 'ensaio/'
  return HttpResponseRedirect(location)