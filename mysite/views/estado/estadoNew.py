#!/usr/bin/python
#-*- coding: utf-8 -*-

from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib import messages
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_protect

from mysite.forms import EstadoForm
from mysite.models import Estado
from mysite.views.estado import estadoList
from mysite.views.estado import *

@login_required
@csrf_protect
def estadoNew(request):
  """
  Display region creation screen
  """
  form = EstadoForm(request.POST or None)
  if form.is_valid():    
    estado = form.save()
    messages.success(request, ('Estado %s criado com sucesso.')% (estado.nome))
    return redirect('estadoList') 

  return render(request, 'estado/form.html', 
    {'form': form,
     'button': 'Criar estado'})