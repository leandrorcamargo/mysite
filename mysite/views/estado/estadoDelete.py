#!/usr/bin/python
#-*- coding: utf-8 -*-

from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib import messages
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_protect

from mysite.models import Estado
from mysite.views import *
from mysite.views.estado import *

@login_required
@csrf_protect
def estadoDelete(request, estadoId):
  """
  Display region edit screen
  if form is valid, saves and shows message

  :type  regionId: region identification number
  :param regionId: int
  """
  model_name = Estado._meta.verbose_name
  estado = Estado.objects.get(id=estadoId)
  nome = estado.nome
  
  try:
    estado.delete()
    messages.success(request, 'Estado %s apagado com sucesso.' % (nome))
  except Exception as e:
    messages.error(request, 'Erro ao apagar estado.') 

  return redirect(estadoList)