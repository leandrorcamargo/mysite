#!/usr/bin/python
#-*- coding: utf-8 -*-

from django.contrib.auth.decorators import login_required, user_passes_test
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_protect

from mysite.models import Estado
from mysite.views import *
from mysite.views.estado import *

@login_required
@csrf_protect
def estadoShow(request, estadoId):
  """
  Display Region'

  :type  regionId: region identification number
  :param regionId: int
  """

  estado = Estado.objects.get(id=estadoId)

  return render(request, 'estado/show.html', 
    {'estado': estado})