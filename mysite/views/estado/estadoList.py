#!/usr/bin/python
#-*- coding: utf-8 -*-

from django.contrib.auth.decorators import login_required, user_passes_test
from django.forms.models import fields_for_model
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_protect


from mysite.models import Estado
from mysite.views import *
from mysite.views.estado import *

@login_required
@csrf_protect
def estadoList(request):
  """
  Display all regions 
  """

  estados = Estado.objects.all().order_by('nome')

  return render(request, 'estado/list.html',
    {'tableHead': fields_for_model(Estado, fields=('nome',)),
     'estados': estados})