#!/usr/bin/python
#-*- coding: utf-8 -*-

from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib import messages
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_protect

from mysite.forms import EstadoForm
from mysite.models import Estado
from mysite.views.estado import estadoList
from mysite.views.estado import *

@login_required
@csrf_protect
def estadoEdit(request, estadoId):
  """
  Display region edit screen
  if form is valid, saves and shows message

  :type  regionId: region identification number
  :param regionId: int
  """

  model_name = Estado._meta.verbose_name
  estado = Estado.objects.get(id=estadoId) 
  

  form = EstadoForm(instance=estado, data=request.POST or None)
  
  if form.is_valid():    
    estado = form.save()
    messages.success(request, 'Estado %s atualizado com sucesso.' % (estado.nome))
    return redirect(estadoShow, estadoId=estadoId)

  return render(request, 'estado/form.html', 
    {'title': ': <b>%s</b>' % (estado.nome),
     'form': form,
     'button': 'Editar Estado'})