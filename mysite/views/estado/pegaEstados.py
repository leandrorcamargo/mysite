#!/usr/bin/python
#-*- coding: utf-8 -*-

from django.contrib.auth.decorators import login_required
from django.shortcuts import render

from mysite.forms import IndexForm
from mysite.views.anuncio import anuncioList
from mysite.views import *
from django.views.decorators.csrf import csrf_protect
from mysite.models import Estado
from django.http import JsonResponse
import json
from django.core import serializers

from mysite import settings

def pegaEstados (request):
  def append_estados(estados_data, estado):
    estados_data.append({ 
      'id': estado.id,
      'data': estado.nome,  
    })

  estados_data = []
  estados = list(Estado.objects.all())
  for estado in estados:
    append_estados(estados_data, estado) 

  return JsonResponse((estados_data), status = 200, safe=False)