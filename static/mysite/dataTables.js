$(function(){
  if (typeof id_modal != "undefined"){

    $('#'+id_modal).on('show.bs.modal', function (event) {
      var button = $(event.relatedTarget) // Button that triggered the modal
      var name = button.data('name') // Extract info from data-* attributes
      var model = button.data('model')

      // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
      // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
      var modal = $(this)
      modal.find('.modal-body span').text( name )
      modal.find('.modal-footer a').attr('href', model)
    });
  }
});



var $table = $('#'+id_list),
    full_screen = false;
    
$().ready(function(){
    $table.bootstrapTable({
        toolbar: ".toolbar",

        showRefresh: true,
        search: true,
        showToggle: true,
        //showColumns: true,
        pagination: true,
        striped: true,
        pageSize: 8,
        pageList: [8,10,25,50,100],
        
        formatShowingRows: function(pageFrom, pageTo, totalRows){
            //do nothing here, we don't want to show the text "showing x of y from..." 
        },
        formatRecordsPerPage: function(pageNumber){
            return pageNumber + " rows visible";
        },
        icons: {
            refresh: 'fa fa-refresh',
            toggle: 'fa fa-th-list',
            columns: 'fa fa-columns',
            detailOpen: 'fa fa-plus-circle',
            detailClose: 'fa fa-minus-circle'
        }
    });
    
                
    
    $(window).resize(function () {
        $table.bootstrapTable('resetView');
    });  
    
});